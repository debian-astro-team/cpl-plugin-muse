<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:str="http://exslt.org/strings"
                extension-element-prefixes="str"
                version="1.0">
  <xsl:include href="str.replace.template.xsl"/>

  <xsl:output method="text"/>
  <xsl:param name="plugin"/>

  <xsl:template match="/">
  <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="pluginlist">
    <xsl:choose>
      <xsl:when test="$plugin">
        <xsl:message>
          Using plugin name <xsl:value-of select="$plugin"/>
        </xsl:message>
        <xsl:apply-templates select="plugin[@name = $plugin]">
          <xsl:with-param name="instrument" select="@instrument"/>
        </xsl:apply-templates>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="plugin">
          <xsl:with-param name="instrument" select="@instrument"/>
        </xsl:apply-templates>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="fileformats">
    <xsl:document href="muse_data_format_z.c" method="text">
      <xsl:call-template name="copyright"/>
      <xsl:text>
#include "muse_cplwrappers.h"
#include "muse_data_format_z.h"

/*----------------------------------------------------------------------------*/
/**
   @defgroup muse_data_format_z   External FITS table formats

   The MUSE pipeline uses and produces a number of FITS tables in
   different formats, which are described in this section.
 */
/**@{*/
      </xsl:text>
      <xsl:apply-templates select="format/data/table" mode="code"/>
      <xsl:text>
/**@}*/
      </xsl:text>
    </xsl:document>
    <xsl:document href="muse_data_format_z.h" method="text">
      <xsl:call-template name="copyright"/>
      <xsl:text>
#ifndef MUSE_TABLEFORMATS_Z_H
#define MUSE_TABLEFORMATS_Z_H

#include "muse_cplwrappers.h"

</xsl:text>
      <xsl:apply-templates select="format/data/table" mode="header"/>
      <xsl:text>
#endif /* MUSE_TABLEFORMATS_Z_H */
</xsl:text>
    </xsl:document>
  </xsl:template>

  <xsl:template match="table" mode="header">
    <xsl:if test="../../@c-code = 'false'">
      <xsl:text>
#ifdef XML_TABLEDEF_</xsl:text>
      <xsl:value-of select="../../@name"/>
    <xsl:text> /* currently not used */
</xsl:text>
    </xsl:if>
    <xsl:for-each select="column/alias">
      <xsl:text>#define </xsl:text>
      <xsl:value-of select="."/>
      <xsl:text> "</xsl:text>
      <xsl:value-of select="../name"/>
      <xsl:text>"
</xsl:text>
    </xsl:for-each>
    <xsl:text>extern const muse_cpltable_def muse_</xsl:text>
    <xsl:value-of select="translate(../../@name, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
                          'abcdefghijklmnopqrstuvwxyz')"/>
    <xsl:if test="../@extname">
      <xsl:text>_</xsl:text>
      <xsl:value-of select="translate(../@extname, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
                            'abcdefghijklmnopqrstuvwxyz')"/>
    </xsl:if>
    <xsl:text>_def[];
</xsl:text>
    <xsl:if test="../../@c-code = 'false'">
      <xsl:text>#endif
</xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template match="table" mode="code">
    <xsl:if test="../../@c-code = 'false'">
      <xsl:text>
#ifdef XML_TABLEDEF_</xsl:text>
      <xsl:value-of select="../../@name"/>
    <xsl:text> /* currently not used */
</xsl:text>
    </xsl:if>
    <xsl:text>
/*----------------------------------------------------------------------------*/
/**</xsl:text>
    <xsl:value-of select="../../description"/>
    <xsl:text>
      Columns:
    </xsl:text>
    <xsl:apply-templates select="column" mode="dox"/>
    <xsl:text>
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
</xsl:text>
<xsl:text>const muse_cpltable_def muse_</xsl:text>
    <xsl:value-of select="translate(../../@name, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
                          'abcdefghijklmnopqrstuvwxyz')"/>
    <xsl:if test="../@extname">
      <xsl:text>_</xsl:text>
      <xsl:value-of select="translate(../@extname, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
                            'abcdefghijklmnopqrstuvwxyz')"/>
    </xsl:if>
    <xsl:text>_def[] = {</xsl:text>
    <xsl:apply-templates select="column" mode="code"/>
    <xsl:text>
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};

</xsl:text>
    <xsl:if test="../../@c-code = 'false'">
      <xsl:text>#endif /* XML_TABLEDEF_</xsl:text>
      <xsl:value-of select="../../@name"/>
      <xsl:text> */
</xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template match="column" mode="code">
    <xsl:text>
  { </xsl:text>
    <xsl:choose>
      <xsl:when test="alias">
	<xsl:value-of select="alias[1]"/>
      </xsl:when>
      <xsl:otherwise>
	<xsl:text>"</xsl:text>
	<xsl:value-of select="name"/>
	<xsl:text>"</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>, </xsl:text>
    <xsl:call-template name="cpltype">
      <xsl:with-param name="type" select="@type"/>
    </xsl:call-template>
    <xsl:if test="@array">
      <xsl:text> | CPL_TYPE_POINTER</xsl:text>
    </xsl:if>
    <xsl:text>, </xsl:text>
    <xsl:choose>
      <xsl:when test="unit">
	<xsl:text>"</xsl:text>
	<xsl:value-of select="unit"/>
	<xsl:text>"</xsl:text>
      </xsl:when>
      <xsl:otherwise>
	<xsl:text>NULL</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>, </xsl:text>
    <xsl:choose>
      <xsl:when test="format">
        <xsl:text>"</xsl:text>
        <xsl:value-of select="format"/>
        <xsl:text>"</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>"</xsl:text>
        <xsl:call-template name="cplformatcode">
          <xsl:with-param name="type" select="@type"/>
        </xsl:call-template>
        <xsl:text>"</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>,
    "</xsl:text>
    <xsl:call-template name="str:replace">
    <xsl:with-param name="string" select="normalize-space(description)"/>
    <xsl:with-param name="search">"</xsl:with-param>
    <xsl:with-param name="replace">\"</xsl:with-param>
    </xsl:call-template>
    <xsl:text>", </xsl:text>
    <xsl:choose>
      <xsl:when test="@required='false'">
        <xsl:text>CPL_FALSE</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>CPL_TRUE</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>},</xsl:text>
  </xsl:template>

  <xsl:template match="column" mode="dox">
    <xsl:text>
      - '&lt;tt&gt;</xsl:text>
    <xsl:value-of select="name"/>
    <xsl:text>&lt;/tt&gt;': </xsl:text>
    <xsl:value-of select="description"/>
    <xsl:text> [</xsl:text>
    <xsl:value-of select="@type"/>
    <xsl:if test="@array">
      <xsl:text> array</xsl:text>
    </xsl:if>
    <xsl:text>]</xsl:text>
    
  </xsl:template>

  <xsl:template match="plugin">
    <xsl:param name="instrument"/>
    <xsl:variable name="cname" select="translate(@name, ' .-+*/()!','______')"/>
    <xsl:document href="{$cname}_z.c" method="text">
      <xsl:apply-templates select="." mode="code">
        <xsl:with-param name="cname" select="$cname"/>
        <xsl:with-param name="context" select="concat($instrument, '.', $cname)"/>
      </xsl:apply-templates>
    </xsl:document>
    <xsl:document href="{$cname}_z.h" method="text">
      <xsl:apply-templates select="." mode="header">
        <xsl:with-param name="cname" select="$cname"/>
      </xsl:apply-templates>
    </xsl:document>
  </xsl:template>

  <xsl:template match="plugin" mode="code">
    <xsl:param name="context"/>
    <xsl:param name="cname"/>
    <xsl:call-template name="copyright"/>
    <xsl:call-template name="include_c">
      <xsl:with-param name="cname" select="$cname"/>
    </xsl:call-template>
    <xsl:apply-templates select="plugininfo" mode="doxygenstart_c">
      <xsl:with-param name="cname" select="$cname"/>
    </xsl:apply-templates>
    <xsl:call-template name="helpstring">
      <xsl:with-param name="cname" select="$cname"/>
      <xsl:with-param name="value" select="normalize-space(plugininfo/description)"/>
    </xsl:call-template>
    <xsl:apply-templates select="frames" name="helpstring_esorex">
      <xsl:with-param name="cname" select="$cname"/>
      <xsl:with-param name="value" select="normalize-space(plugininfo/description)"/>
    </xsl:apply-templates>
    <xsl:apply-templates select="frames" mode="recipeconfig">
      <xsl:with-param name="cname" select="$cname"/>
    </xsl:apply-templates>
    <xsl:apply-templates select="frames" mode="fitsheader">
      <xsl:with-param name="cname" select="$cname"/>
    </xsl:apply-templates>
    <xsl:apply-templates select="frames" mode="level">
      <xsl:with-param name="cname" select="$cname"/>
    </xsl:apply-templates>
    <xsl:apply-templates select="frames" mode="mode">
      <xsl:with-param name="cname" select="$cname"/>
    </xsl:apply-templates>
    <xsl:apply-templates select="parameters" mode="create_plugin">
      <xsl:with-param name="cname" select="$cname"/>
      <xsl:with-param name="context" select="$context"/>
    </xsl:apply-templates>
    <xsl:apply-templates select="parameters" mode="params_fill">
      <xsl:with-param name="cname" select="$cname"/>
      <xsl:with-param name="context" select="$context"/>
    </xsl:apply-templates>
    <xsl:call-template name="exec_plugin">
      <xsl:with-param name="name" select="@name"/>
      <xsl:with-param name="cname" select="$cname"/>
      <xsl:with-param name="nifu" select="parameters/parameter[@name='nifu']"/>
      <xsl:with-param name="merge" select="parameters/parameter[@name='merge']"/>
    </xsl:call-template>
    <xsl:call-template name="destroy_plugin">
      <xsl:with-param name="cname" select="$cname"/>
    </xsl:call-template>
    <xsl:call-template name="init_plugin">
      <xsl:with-param name="name" select="@name"/>
      <xsl:with-param name="cname" select="$cname"/>
      <xsl:with-param name="synopsis" select="plugininfo/synopsis"/>
      <xsl:with-param name="author" select="plugininfo/author"/>
      <xsl:with-param name="email" select="plugininfo/email"/>
    </xsl:call-template>
    <xsl:call-template name="doxygenend"/>
  </xsl:template>

  <xsl:template match="parameters" mode="create_plugin">
    <xsl:param name="cname"/>
    <xsl:param name="context"/>
/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Setup the recipe options.
  @param   aPlugin   the plugin
  @return  0 if everything is ok, -1 if not called as part of a recipe.

  Define the command-line, configuration, and environment parameters for the
  recipe.
 */
/*----------------------------------------------------------------------------*/
static int
<xsl:value-of select="$cname"/>_create(cpl_plugin *aPlugin)
{
  /* Check that the plugin is part of a valid recipe */
  cpl_recipe *recipe;
  if (cpl_plugin_get_type(aPlugin) == CPL_PLUGIN_TYPE_RECIPE) {
    recipe = (cpl_recipe *)aPlugin;
  } else {
    return -1;
  }

  /* register the extended processing information (new FITS header creation, *
   * getting of the frame level for a certain tag)                           */
  muse_processinginfo_register(recipe,
                               <xsl:value-of select="$cname"/>_new_recipeconfig(),
                               <xsl:value-of select="$cname"/>_prepare_header,
                               <xsl:value-of select="$cname"/>_get_frame_level,
                               <xsl:value-of select="$cname"/>_get_frame_mode);

  /* XXX initialize timing in messages                                       *
   *     since at least esorex is too stupid to turn it on, we have to do it */
  if (muse_cplframework() == MUSE_CPLFRAMEWORK_ESOREX) {
    cpl_msg_set_time_on();
  }

  /* Create the parameter list in the cpl_recipe object */
  recipe->parameters = cpl_parameterlist_new();
  /* Fill the parameters list */
  <xsl:if test="parameter">cpl_parameter *p;</xsl:if>
  <xsl:apply-templates mode="create_plugin">
    <xsl:with-param name="context" select="$context"/>
    <xsl:with-param name="cname" select="$cname"/>
  </xsl:apply-templates>
  return 0;
} /* <xsl:value-of select="$cname"/>_create() */
</xsl:template>

  <xsl:template match="parameter" mode="create_plugin">
    <xsl:param name="context"/>
    <xsl:param name="cname"/>
    <xsl:variable name="full-name" select="concat($context, '.', @name)"/>
    <xsl:variable name="alias" select="translate(@name, ' .-+*/()!','------')"/>
  /* --<xsl:value-of select="$alias"/>: <xsl:value-of select="description"/> */
  <xsl:choose>
    <xsl:when test="enumeration">p = cpl_parameter_new_enum("<xsl:value-of select="$full-name"/>",
                             <xsl:call-template name="cpltype">
                               <xsl:with-param name="type" select="@type"/>
                             </xsl:call-template>,
                             "<xsl:call-template name="str:replace">
                               <xsl:with-param name="string" select="normalize-space(description)"/>
                               <xsl:with-param name="search">"</xsl:with-param>
                               <xsl:with-param name="replace">\"</xsl:with-param>
                             </xsl:call-template>",
                             "<xsl:value-of select="$context"/>",
                             (<xsl:call-template name="ctype">
                                <xsl:with-param name="type" select="@type"/>
                              </xsl:call-template>)<xsl:call-template name="cvalue">
                              <xsl:with-param name="value" select="default"/>
                              <xsl:with-param name="type" select="@type"/>
                             </xsl:call-template>,
                             <xsl:value-of select="count(enumeration/value)"/><xsl:for-each select="enumeration/value">,
                             (<xsl:call-template name="ctype">
                             <xsl:with-param name="type" select="../../@type"/>
                              </xsl:call-template>)<xsl:call-template name="cvalue">
                               <xsl:with-param name="value" select="."/>
                               <xsl:with-param name="type" select="../../@type"/>
                             </xsl:call-template>
                             </xsl:for-each>);
</xsl:when>
    <xsl:when test="range">p = cpl_parameter_new_range("<xsl:value-of select="$full-name"/>",
                              <xsl:call-template name="cpltype">
                                <xsl:with-param name="type" select="@type"/>
                              </xsl:call-template>,
                             "<xsl:call-template name="str:replace">
                               <xsl:with-param name="string" select="normalize-space(description)"/>
                               <xsl:with-param name="search">"</xsl:with-param>
                               <xsl:with-param name="replace">\"</xsl:with-param>
                             </xsl:call-template>",
                              "<xsl:value-of select="$context"/>",
                              (<xsl:call-template name="ctype">
                                 <xsl:with-param name="type" select="@type"/>
                               </xsl:call-template>)<xsl:call-template name="cvalue">
                                <xsl:with-param name="value" select="default"/>
                                <xsl:with-param name="type" select="@type"/>
                              </xsl:call-template>,
                              (<xsl:call-template name="ctype">
                                 <xsl:with-param name="type" select="@type"/>
                               </xsl:call-template>)<xsl:call-template name="cvalue">
                                <xsl:with-param name="value" select="range/@min"/>
                                <xsl:with-param name="type" select="@type"/>
                              </xsl:call-template>,
                              (<xsl:call-template name="ctype">
                                 <xsl:with-param name="type" select="@type"/>
                               </xsl:call-template>)<xsl:call-template name="cvalue">
                                <xsl:with-param name="value" select="range/@max"/>
                                <xsl:with-param name="type" select="@type"/>
                              </xsl:call-template>);
</xsl:when>
    <xsl:otherwise>p = cpl_parameter_new_value("<xsl:value-of select="$full-name"/>",
                              <xsl:call-template name="cpltype">
                                <xsl:with-param name="type" select="@type"/>
                              </xsl:call-template>,
                             "<xsl:call-template name="str:replace">
                               <xsl:with-param name="string" select="normalize-space(description)"/>
                               <xsl:with-param name="search">"</xsl:with-param>
                               <xsl:with-param name="replace">\"</xsl:with-param>
                             </xsl:call-template>",
                              "<xsl:value-of select="$context"/>",
                              (<xsl:call-template name="ctype">
                                 <xsl:with-param name="type" select="@type"/>
                               </xsl:call-template>)<xsl:call-template name="cvalue">
                                <xsl:with-param name="value" select="default"/>
                                <xsl:with-param name="type" select="@type"/>
                              </xsl:call-template>);
</xsl:otherwise>
  </xsl:choose>  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "<xsl:value-of select="$alias"/>");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "<xsl:value-of select="$alias"/>");
<xsl:if test="@expertlevel='true'">  if (!getenv("MUSE_EXPERT_USER")) {
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_CLI);
  }
</xsl:if>
  <!--cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_ENV, "<xsl:value-of select="translate($cname, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>_PARAM_<xsl:value-of select="translate($alias, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>");-->
  cpl_parameterlist_append(recipe->parameters, p);</xsl:template>

  <xsl:template match="parameters" mode="params_fill">
    <xsl:param name="cname"/>
    <xsl:param name="context"/>
    <xsl:text>
/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Fill the recipe parameters into the parameter structure
  @param   aParams       the recipe-internal structure to fill
  @param   aParameters   the cpl_parameterlist with the parameters
  @return  0 if everything is ok, -1 if something went wrong.

  This is a convienience function that centrally fills all parameters into the
  according fields of the recipe internal structure.
 */
/*----------------------------------------------------------------------------*/
static int
</xsl:text>
    <xsl:value-of select="$cname"/>
    <xsl:text>_params_fill(</xsl:text>
    <xsl:value-of select="$cname"/>
    <xsl:text>_params_t *aParams, cpl_parameterlist *aParameters)
{
  cpl_ensure_code(aParams, CPL_ERROR_NULL_INPUT);
  cpl_ensure_code(aParameters, CPL_ERROR_NULL_INPUT);
</xsl:text>
  <xsl:if test="parameter">
    <xsl:text>  cpl_parameter *p;</xsl:text>
  </xsl:if>
  <xsl:apply-templates mode="params_fill">
    <xsl:with-param name="context" select="$context"/>
  </xsl:apply-templates>
  <xsl:text>
  return 0;
} /* </xsl:text><xsl:value-of select="$cname"/><xsl:text>_params_fill() */
</xsl:text>
</xsl:template>

  <xsl:template match="parameter" mode="params_fill">
    <xsl:param name="context"/>
    <xsl:variable name="full-name" select="concat($context, '.', @name)"/>
    <xsl:variable name="alias" select="translate(@name, ' ._+*/()!','------')"/>
    <xsl:variable name="cname" select="translate(@name, ' .-+*/()!','______')"/>
    <xsl:text>
  p = cpl_parameterlist_find(aParameters, "</xsl:text>
    <xsl:value-of select="$full-name"/>
    <xsl:text>");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);</xsl:text>
    <xsl:choose>
      <xsl:when test="@type = 'string' and enumeration">
        <xsl:text>
  aParams-></xsl:text>
        <xsl:value-of select="$cname"/>
        <xsl:text>_s = cpl_parameter_get_</xsl:text>
        <xsl:call-template name="cpltypename">
          <xsl:with-param name="type" select="@type"/>
        </xsl:call-template>
        <xsl:text>(p);
  aParams-></xsl:text>
        <xsl:value-of select="$cname"/>
        <xsl:text> =
</xsl:text>
        <xsl:for-each select="enumeration/value">
          <xsl:text>    (!strcasecmp(aParams-></xsl:text>
          <xsl:value-of select="$cname"/>
          <xsl:text>_s, "</xsl:text>
          <xsl:value-of select='.'/>
          <xsl:text>")) ? </xsl:text>
          <xsl:apply-templates mode="paramvaluename" select="."/>
          <xsl:text> :
</xsl:text>
        </xsl:for-each>
        <xsl:text>      </xsl:text>
        <xsl:apply-templates mode="paraminvalidname" select="."/>
        <xsl:text>;
  cpl_ensure_code(aParams-></xsl:text>
        <xsl:value-of select="$cname"/>
        <xsl:text> != </xsl:text>
        <xsl:apply-templates mode="paraminvalidname" select="."/>
        <xsl:text>,
                  CPL_ERROR_ILLEGAL_INPUT);</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>
  aParams-></xsl:text>
        <xsl:value-of select="$cname"/>
        <xsl:text> = cpl_parameter_get_</xsl:text>
        <xsl:call-template name="cpltypename">
          <xsl:with-param name="type" select="@type"/>
        </xsl:call-template>
        <xsl:text>(p);</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="frames" mode="recipeconfig">
    <xsl:param name="cname"/>
/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Create the recipe config for this plugin.

  @remark The recipe config is used to check for the tags as well as to create
          the documentation of this plugin.
 */
/*----------------------------------------------------------------------------*/
static cpl_recipeconfig *
<xsl:value-of select="$cname"/>_new_recipeconfig(void)
{
  cpl_recipeconfig *recipeconfig = cpl_recipeconfig_new();<xsl:apply-templates mode="recipeconfig"/>
  return recipeconfig;
} /* <xsl:value-of select="$cname"/>_new_recipeconfig() */
</xsl:template>

  <xsl:template match="frameset" mode="recipeconfig">
  <xsl:for-each select="frame[@group='raw']">
  <xsl:variable name="rawtag"><xsl:value-of select="@tag"/></xsl:variable>
  cpl_recipeconfig_set_tag(recipeconfig, "<xsl:value-of select="$rawtag"/>", <xsl:choose>
      <xsl:when test="@min"><xsl:value-of select="@min"/></xsl:when>
      <xsl:otherwise>-1</xsl:otherwise>
    </xsl:choose>, <xsl:choose>
      <xsl:when test="@max"><xsl:value-of select="@max"/></xsl:when>
      <xsl:otherwise>-1</xsl:otherwise>
    </xsl:choose>);<xsl:for-each select="../frame[@group='calib']">
  cpl_recipeconfig_set_input(recipeconfig, "<xsl:value-of select="$rawtag"/>", "<xsl:value-of select="@tag"/>", <xsl:choose>
        <xsl:when test="@min"><xsl:value-of select="@min"/></xsl:when>
        <xsl:otherwise>-1</xsl:otherwise>
      </xsl:choose>, <xsl:choose>
        <xsl:when test="@max"><xsl:value-of select="@max"/></xsl:when>
        <xsl:otherwise>-1</xsl:otherwise>
      </xsl:choose>);</xsl:for-each>
    <xsl:for-each select="../frame[@group='product'][@base=$rawtag]">
  cpl_recipeconfig_set_output(recipeconfig, "<xsl:value-of select="$rawtag"/>", "<xsl:value-of select="@tag"/>");</xsl:for-each>
    <xsl:for-each select="../frame[@group='product'][not(@base)]">
  cpl_recipeconfig_set_output(recipeconfig, "<xsl:value-of select="$rawtag"/>", "<xsl:value-of select="@tag"/>");</xsl:for-each>
  </xsl:for-each>
  </xsl:template>

  <xsl:template match="frames" mode="fitsheader">
    <xsl:param name="cname"/>
/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Return a new header that shall be filled on output.
  @param   aFrametag   tag of the output frame
  @param   aHeader     the prepared FITS header
  @return  CPL_ERROR_NONE on success another cpl_error_code on error

  @remark This function is also used to generate the recipe documentation.
 */
/*----------------------------------------------------------------------------*/
static cpl_error_code
<xsl:value-of select="$cname"/>_prepare_header(const char *aFrametag, cpl_propertylist *aHeader)
{
  cpl_ensure_code(aFrametag, CPL_ERROR_NULL_INPUT);
  cpl_ensure_code(aHeader, CPL_ERROR_NULL_INPUT);
  <xsl:apply-templates select="frameset/frame[@group='product']" mode="fitsheader"/>{
    cpl_msg_warning(__func__, "Frame tag %s is not defined", aFrametag);
    return CPL_ERROR_ILLEGAL_INPUT;
  }
  return CPL_ERROR_NONE;
} /* <xsl:value-of select="$cname"/>_prepare_header() */
</xsl:template>

  <xsl:template match="frame[@group='product']" mode="fitsheader">if (!strcmp(aFrametag, "<xsl:value-of select="@tag"/>")) {<xsl:apply-templates select="fitsheader/property"/>
  } else </xsl:template>

  <xsl:template match="property">
  <xsl:if test="value">
    cpl_propertylist_update_<xsl:call-template name="cpltypename">
  <xsl:with-param name="type" select="@type"/>
</xsl:call-template>(aHeader, "<xsl:value-of select="name"/>", <xsl:value-of select="value"/>);</xsl:if>
    muse_processing_prepare_property(aHeader, "<xsl:call-template name='replace-param-id-regexp'>
      <xsl:with-param name='string' select='name'/>
      <xsl:with-param name='plist' select='../parameter'/>
  </xsl:call-template>",
                                     <xsl:call-template name="cpltype">
                                       <xsl:with-param name="type" select="@type"/>
                                     </xsl:call-template>,
                                     "<xsl:if test="unit">[<xsl:value-of select="normalize-space(unit)"/>] </xsl:if><xsl:call-template name="str:replace">
				     <xsl:with-param name="string" select="normalize-space(description)"/>
				     <xsl:with-param name="search">"</xsl:with-param>
				     <xsl:with-param name="replace">\"</xsl:with-param>
				   </xsl:call-template>");</xsl:template>

  <xsl:template name='replace-param-id-regexp'>
    <xsl:param name='string'/>
    <xsl:param name='plist'/>
      <xsl:choose>
        <xsl:when test='$plist'>
          <xsl:call-template name="str:replace">
            <xsl:with-param name="string">
              <xsl:call-template name='replace-param-id-regexp'>
                <xsl:with-param name="string" select='$string'/>
                <xsl:with-param name="plist" select='$plist[position() > 1]'/>
              </xsl:call-template>
            </xsl:with-param>
            <xsl:with-param name="search"><xsl:value-of select="$plist[1]/@id"/></xsl:with-param>
            <xsl:with-param name="replace"><xsl:value-of select="$plist[1]/@regexp"/></xsl:with-param>
        </xsl:call-template></xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$string"/>
        </xsl:otherwise>
      </xsl:choose>
  </xsl:template>

  <xsl:template match="frames" mode="level">
    <xsl:param name="cname"/>
/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Return the level of an output frame.
  @param   aFrametag   tag of the output frame
  @return  the cpl_frame_level or CPL_FRAME_LEVEL_NONE on error

  @remark This function is also used to generate the recipe documentation.
 */
/*----------------------------------------------------------------------------*/
static cpl_frame_level
<xsl:value-of select="$cname"/>_get_frame_level(const char *aFrametag)
{
  if (!aFrametag) {
    return CPL_FRAME_LEVEL_NONE;
  }<xsl:apply-templates select="frameset/frame[@group='product']" mode="level"/>
  return CPL_FRAME_LEVEL_NONE;
} /* <xsl:value-of select="$cname"/>_get_frame_level() */
</xsl:template>

  <xsl:template match="frame[@group='product']" mode="level">
  if (!strcmp(aFrametag, "<xsl:value-of select="@tag"/>")) {
    return <xsl:call-template name="cplframelevel">
      <xsl:with-param name="level" select="@level"/>
    </xsl:call-template>;
  }</xsl:template>

  <xsl:template name="cplframelevel">
    <xsl:param name="level"/>
    <xsl:choose>
      <xsl:when test="$level = 'temporary'">CPL_FRAME_LEVEL_TEMPORARY</xsl:when>
      <xsl:when test="$level = 'intermediate'">CPL_FRAME_LEVEL_INTERMEDIATE</xsl:when>
      <xsl:when test="$level = 'final'">CPL_FRAME_LEVEL_FINAL</xsl:when>
      <xsl:otherwise>CPL_FRAME_LEVEL_NONE</xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="frames" mode="mode">
    <xsl:param name="cname"/>
/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Return the mode of an output frame.
  @param   aFrametag   tag of the output frame
  @return  the muse_frame_mode

  @remark This function is also used to generate the recipe documentation.
 */
/*----------------------------------------------------------------------------*/
static muse_frame_mode
<xsl:value-of select="$cname"/>_get_frame_mode(const char *aFrametag)
{
  if (!aFrametag) {
    return MUSE_FRAME_MODE_ALL;
  }<xsl:apply-templates select="frameset/frame[@group='product']" mode="mode"/>
  return MUSE_FRAME_MODE_ALL;
} /* <xsl:value-of select="$cname"/>_get_frame_mode() */
</xsl:template>

  <xsl:template match="frame[@group='product']" mode="mode">
  if (!strcmp(aFrametag, "<xsl:value-of select="@tag"/>")) {
  <xsl:choose>
    <xsl:when test="@mode='master'">  return MUSE_FRAME_MODE_MASTER;</xsl:when>
    <xsl:when test="@mode='dateobs'">  return MUSE_FRAME_MODE_DATEOBS;</xsl:when>
    <xsl:when test="@mode='subset'">  return MUSE_FRAME_MODE_SUBSET;</xsl:when>
    <xsl:when test="@mode='sequence'">  return MUSE_FRAME_MODE_SEQUENCE;</xsl:when>
    <xsl:otherwise>  return MUSE_FRAME_MODE_ALL;</xsl:otherwise>
  </xsl:choose>
  }</xsl:template>

  <xsl:template name="ctype">
    <xsl:param name="type"/>
    <xsl:choose>
      <xsl:when test="$type = 'int'">int</xsl:when>
      <xsl:when test="$type = 'string'">const char *</xsl:when>
      <xsl:when test="$type = 'datetime'">const char *</xsl:when>
      <xsl:when test="$type = 'double'">double</xsl:when>
      <xsl:when test="$type = 'float'">float</xsl:when>
      <xsl:when test="$type = 'boolean'">int</xsl:when>
      <xsl:otherwise>void</xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="cpltype">
    <xsl:param name="type"/>
    <xsl:choose>
      <xsl:when test="$type = 'int'">CPL_TYPE_INT</xsl:when>
      <xsl:when test="$type = 'string'">CPL_TYPE_STRING</xsl:when>
      <xsl:when test="$type = 'datetime'">CPL_TYPE_STRING</xsl:when>
      <xsl:when test="$type = 'double'">CPL_TYPE_DOUBLE</xsl:when>
      <xsl:when test="$type = 'float'">CPL_TYPE_FLOAT</xsl:when>
      <xsl:when test="$type = 'boolean'">CPL_TYPE_BOOL</xsl:when>
      <xsl:otherwise>CPL_TYPE_UNKNOWN: <xsl:value-of select="$type"/></xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="cplformatcode">
    <xsl:param name="type"/>
    <xsl:choose>
      <xsl:when test="$type = 'int'">%7d</xsl:when>
      <xsl:when test="$type = 'string'">%s</xsl:when>
      <xsl:when test="$type = 'datetime'">%s</xsl:when>
      <xsl:when test="$type = 'double'">%1.5e</xsl:when>
      <xsl:when test="$type = 'float'">%1.5e</xsl:when>
      <xsl:when test="$type = 'boolean'">%7d</xsl:when>
      <xsl:otherwise>%s</xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="cvalue">
    <xsl:param name="value"/>
    <xsl:param name="type"/>
    <xsl:choose>
      <xsl:when test="$type = 'string'">"<xsl:call-template name="str:replace">
        <xsl:with-param name="string" select="$value"/>
        <xsl:with-param name="search">"</xsl:with-param>
        <xsl:with-param name="replace">\"</xsl:with-param>
      </xsl:call-template>"</xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$value = 'true'">TRUE</xsl:when>
          <xsl:when test="$value = 'false'">FALSE</xsl:when>
          <xsl:otherwise><xsl:value-of select="$value"/></xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="nullcvalue">
    <xsl:param name="type"/>
    <xsl:choose>
      <xsl:when test="$type = 'int'">INT_MAX</xsl:when>
      <xsl:when test="$type = 'string'">NULL</xsl:when>
      <xsl:when test="$type = 'datetime'">NULL</xsl:when>
      <xsl:when test="$type = 'double'">NAN</xsl:when>
      <xsl:when test="$type = 'float'">NAN</xsl:when>
      <xsl:when test="$type = 'boolean'">INT_MAX</xsl:when>
      <xsl:otherwise>NULL</xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="cpltypename">
    <xsl:param name="type"/>
    <xsl:choose>
      <xsl:when test="$type = 'int'">int</xsl:when>
      <xsl:when test="$type = 'string'">string</xsl:when>
      <xsl:when test="$type = 'datetime'">string</xsl:when>
      <xsl:when test="$type = 'double'">double</xsl:when>
      <xsl:when test="$type = 'float'">float</xsl:when>
      <xsl:when test="$type = 'boolean'">bool</xsl:when>
      <xsl:otherwise><xsl:value-of select="$type"/></xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="value" mode="paramvaluename">
    <xsl:value-of select="translate(../../../../@name, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
    <xsl:text>_PARAM_</xsl:text>
    <xsl:value-of select="translate(../../@name, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
    <xsl:text>_</xsl:text>
    <xsl:value-of select="translate(., 'abcdefghijklmnopqrstuvwxyz .-&quot;','ABCDEFGHIJKLMNOPQRSTUVWXYZ___')"/>
  </xsl:template>

  <xsl:template match="parameter" mode="paraminvalidname">
    <xsl:value-of select="translate(../../@name, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
    <xsl:text>_PARAM_</xsl:text>
    <xsl:value-of select="translate(@name, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
    <xsl:text>_INVALID_VALUE</xsl:text>
  </xsl:template>

  <xsl:template match="plugin" mode="header">
    <xsl:param name="cname"/>
    <xsl:call-template name="copyright"/>
#ifndef <xsl:value-of select="translate($cname, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>_Z_H
#define <xsl:value-of select="translate($cname, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>_Z_H
<![CDATA[
/*----------------------------------------------------------------------------*
 *                              Includes                                      *
 *----------------------------------------------------------------------------*/
#include <muse.h>
#include <muse_instrument.h>
]]>
<xsl:apply-templates select="plugininfo" mode="doxygenstart_h">
    <xsl:with-param name="cname" select="$cname"/>
  </xsl:apply-templates>
  <xsl:apply-templates select="parameters" mode="paramstruct">
    <xsl:with-param name="cname" select="$cname"/>
  </xsl:apply-templates>
  <xsl:call-template name="doxygenend"/>
  <xsl:call-template name="compute_plugin_prototype">
    <xsl:with-param name="cname" select="$cname"/>
  </xsl:call-template>
#endif /* <xsl:value-of select="translate($cname, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>_Z_H */
</xsl:template>

  <xsl:template match="parameters" mode="paramstruct">
    <xsl:param name="cname"/>
    <xsl:text>

/*----------------------------------------------------------------------------*/
/**
  @brief   Structure to hold the parameters of the </xsl:text><xsl:value-of select="$cname"/><xsl:text> recipe.

  This structure contains the parameters for the recipe that may be set on the
  command line, in the configuration, or through the environment.
 */
/*----------------------------------------------------------------------------*/
typedef struct </xsl:text>
    <xsl:value-of select="$cname"/>
    <xsl:text>_params_s {</xsl:text>
    <xsl:apply-templates select="parameter" mode="paramstruct"/>
    <xsl:text>
  char __dummy__; /* quieten compiler warning about possibly empty struct */
} </xsl:text>
    <xsl:value-of select="$cname"/>
    <xsl:text>_params_t;

</xsl:text>
    <xsl:for-each select="parameter">
      <xsl:if test="@type = 'string' and enumeration">
        <xsl:for-each select="enumeration/value">
          <xsl:text>#define </xsl:text>
          <xsl:apply-templates mode="paramvaluename" select="."/>
          <xsl:text> </xsl:text>
          <xsl:value-of select='position()'/>
        <xsl:text>
</xsl:text>
        </xsl:for-each>
        <xsl:text>#define </xsl:text>
        <xsl:apply-templates mode="paraminvalidname" select="."/>
        <xsl:text> -1
</xsl:text>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="parameter" mode="paramstruct">
    <xsl:variable name="cname" select="translate(@name, ' .-+*/()!','______')"/>
    <xsl:text>
  /** @brief   </xsl:text>
    <xsl:value-of select="description"/>
    <xsl:text> */
</xsl:text>
    <xsl:choose>
      <xsl:when test="@type = 'string' and enumeration">
        <xsl:text>  int </xsl:text>
        <xsl:value-of select="$cname"/>
    <xsl:text>;
  /** @brief   </xsl:text>
    <xsl:value-of select="description"/>
    <xsl:text> (as string) */
  </xsl:text>
        <xsl:call-template name="ctype">
          <xsl:with-param name="type" select="@type"/>
        </xsl:call-template>
        <xsl:value-of select="$cname"/>
        <xsl:text>_s;
</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>  </xsl:text>
        <xsl:call-template name="ctype">
          <xsl:with-param name="type" select="@type"/>
        </xsl:call-template>
        <xsl:text> </xsl:text>
        <xsl:value-of select="$cname"/>
        <xsl:text>;
</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="compute_plugin_prototype">
    <xsl:param name="cname"/>

/*----------------------------------------------------------------------------*
 *                           Function prototypes                              *
 *----------------------------------------------------------------------------*/
int <xsl:value-of select="$cname"/>_compute(muse_processing *, <xsl:value-of select="$cname"/>_params_t *);
</xsl:template>

 <xsl:template name="copyright"><![CDATA[/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/* 
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* This file was automatically generated */
]]></xsl:template>

  <xsl:template name="include_c">
    <xsl:param name="cname"/><![CDATA[
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*----------------------------------------------------------------------------*
 *                              Includes                                      *
 *----------------------------------------------------------------------------*/
#include <string.h> /* strcmp(), strstr() */
#include <strings.h> /* strcasecmp() */
#include <cpl.h>

#include "]]><xsl:value-of select="$cname"/>_z.h" /* in turn includes muse.h */
</xsl:template>

  <xsl:template match="plugininfo" mode="doxygenstart_c">
    <xsl:param name="cname"/>
/*----------------------------------------------------------------------------*/
/**
  @defgroup recipe_<xsl:value-of select="$cname"/><xsl:text>         Recipe </xsl:text><xsl:value-of select="$cname"/><xsl:text>: </xsl:text><xsl:value-of select="synopsis"/><xsl:text>
  @author </xsl:text><xsl:value-of select="author"/><xsl:text>
  </xsl:text><xsl:value-of select="description"/>
 */
/*----------------------------------------------------------------------------*/
/**@{*/</xsl:template>

  <xsl:template name="doxygenend">
/**@}*/</xsl:template>

  <xsl:template match="plugininfo" mode="doxygenstart_h">
    <xsl:param name="cname"/>/*----------------------------------------------------------------------------*
 *                          Special variable types                            *
 *----------------------------------------------------------------------------*/

/** @addtogroup recipe_<xsl:value-of select="$cname"/> */
/**@{*/</xsl:template>

  <xsl:template name="helpstring">
    <xsl:param name="cname"/>
    <xsl:param name="value"/>

/*----------------------------------------------------------------------------*
 *                         Static variables                                   *
 *----------------------------------------------------------------------------*/
static const char *<xsl:value-of select="$cname"/>
  <xsl:text>_help =
  "</xsl:text>
  <xsl:call-template name="str:replace">
    <xsl:with-param name="string" select="normalize-space($value)"/>
    <xsl:with-param name="search">"</xsl:with-param>
    <xsl:with-param name="replace">\"</xsl:with-param>
  </xsl:call-template>
  <xsl:text>";
</xsl:text>
</xsl:template>

  <xsl:template match="frames" name="helpstring_esorex">
    <xsl:param name="cname"/>
    <xsl:param name="value"/>
    <xsl:text>
static const char *</xsl:text>
    <xsl:value-of select="$cname"/>
    <xsl:text>_help_esorex =
</xsl:text>
    <xsl:apply-templates select="frameset" mode="helpstring_esorex"/>
    <xsl:text>;
</xsl:text>
  </xsl:template>

  <xsl:template match="frameset" mode="helpstring_esorex">
    <xsl:text>  "\n\nInput frames</xsl:text>
    <xsl:if test="frame[@group='raw'][1]/@tag != ''">
      <xsl:text> for raw frame tag \"</xsl:text>
      <xsl:value-of select="frame[@group='raw'][1]/@tag"/>
      <xsl:text>\"</xsl:text>
    </xsl:if>
    <xsl:text>:\n"
  "\n Frame tag            Type Req #Fr Description"
  "\n -------------------- ---- --- --- ------------"</xsl:text>
    <xsl:apply-templates select="frame[@group='raw']" mode="helpstring_esorex"/>
    <xsl:apply-templates select="frame[@group='calib']" mode="helpstring_esorex"/>
    <xsl:text>
  "\n\nProduct frames for raw frame tag \"</xsl:text>
    <xsl:value-of select="frame[@group='raw'][1]/@tag"/>
    <xsl:text>\":\n"
  "\n Frame tag            Level    Description"
  "\n -------------------- -------- ------------"</xsl:text>
    <xsl:apply-templates select="frame[@group='product']" mode="helpstring_esorex"/>
  </xsl:template>

  <xsl:template match="frame[@group='raw' or @group='calib']" mode="helpstring_esorex">
    <xsl:text>
  "\n </xsl:text>
    <xsl:choose>
      <xsl:when test="@tag != ''">
        <xsl:value-of select="@tag"/>
        <xsl:value-of select="substring('                    ',string-length(@tag), 20)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>any                  </xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="@group"/>
    <xsl:value-of select="substring('     ',string-length(@group))"/>
    <xsl:choose>
      <xsl:when test="@min and @min > 0">Y</xsl:when>
      <xsl:otherwise>.</xsl:otherwise>
    </xsl:choose>
    <xsl:text>  </xsl:text>
    <xsl:choose>
      <xsl:when test="@min and @min > 1 and (not(@max) or @max > @min)">
        <xsl:text>&gt;=</xsl:text>
        <xsl:value-of select="@min"/>
      </xsl:when>
      <xsl:when test="@max">
        <xsl:choose>
          <xsl:when test="@max > 1 and (not(@min) or @max > @min)">&lt;=</xsl:when>
          <xsl:otherwise><xsl:text>  </xsl:text></xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="@max"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>   </xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text> </xsl:text>
    <xsl:call-template name="str:replace">
      <xsl:with-param name="string" select="normalize-space(description)"/>
      <xsl:with-param name="search">"</xsl:with-param>
      <xsl:with-param name="replace">\"</xsl:with-param>
    </xsl:call-template>
    <xsl:text>"</xsl:text>
  </xsl:template>

  <xsl:template match="frame[@group='product']" mode="helpstring_esorex">
    <xsl:text>
  "\n </xsl:text>
    <xsl:value-of select="@tag"/>
    <xsl:value-of select="substring('                    ',string-length(@tag))"/>
    <xsl:value-of select="substring(@level, 1, 8)"/>
    <xsl:value-of select="substring('        ',string-length(substring(@level, 1, 8)))"/>
    <xsl:call-template name="str:replace">
      <xsl:with-param name="string" select="normalize-space(description)"/>
      <xsl:with-param name="search">"</xsl:with-param>
      <xsl:with-param name="replace">\"</xsl:with-param>
    </xsl:call-template>
    <xsl:text>"</xsl:text>
  </xsl:template>

  <xsl:template name="exec_plugin">
    <xsl:param name="name"/>
    <xsl:param name="cname"/>
    <xsl:param name="nifu"/>
    <xsl:param name="merge"/>
/*----------------------------------------------------------------------------*/
/**
  @private
  @brief    Execute the plugin instance given by the interface
  @param    aPlugin   the plugin
  @return   0 if everything is ok, -1 if not called as part of a recipe
 */
/*----------------------------------------------------------------------------*/
static int
<xsl:value-of select="$cname"/>_exec(cpl_plugin *aPlugin)
{
  if (cpl_plugin_get_type(aPlugin) != CPL_PLUGIN_TYPE_RECIPE) {
    return -1;
  }
  muse_processing_recipeinfo(aPlugin);
  cpl_recipe *recipe = (cpl_recipe *)aPlugin;
  cpl_msg_set_threadid_on();

  cpl_frameset *usedframes = cpl_frameset_new(),
               *outframes = cpl_frameset_new();
  <xsl:value-of select="$cname"/>_params_t params;
  <xsl:value-of select="$cname"/>_params_fill(&amp;params, recipe->parameters);

  cpl_errorstate prestate = cpl_errorstate_get();
<xsl:choose>
  <xsl:when test="$nifu and not(contains($cname, 'qi_mask'))">
  if (params.nifu &lt; -1 || params.nifu &gt; kMuseNumIFUs) {
    cpl_msg_error(__func__, "Please specify a valid IFU number (between 1 and "
                  "%d), 0 (to process all IFUs consecutively), or -1 (to "
                  "process all IFUs in parallel) using --nifu.", kMuseNumIFUs);
    return -1;
  } /* if invalid params.nifu */

  cpl_boolean donotmerge = CPL_FALSE; /* depending on nifu we may not merge */
  int rc = 0;
  if (params.nifu > 0) {
    muse_processing *proc = muse_processing_new("<xsl:value-of select="$name"/>",
                                                recipe);
    rc = <xsl:value-of select="$cname"/>_compute(proc, &amp;params);
    cpl_frameset_join(usedframes, proc->usedframes);
    cpl_frameset_join(outframes, proc->outframes);
    muse_processing_delete(proc);
    donotmerge = CPL_TRUE; /* after processing one IFU, merging cannot work */
  } else if (params.nifu &lt; 0) { /* parallel processing */
    int *rcs = cpl_calloc(kMuseNumIFUs, sizeof(int));
    int nifu;
    // FIXME: Removed default(none) clause here, to make the code work with
    //        gcc9 while keeping older versions of gcc working too without
    //        resorting to conditional compilation. Having default(none) here
    //        causes gcc9 to abort the build because of __func__ not being
    //        specified in a data sharing clause. Adding a data sharing clause
    //        makes older gcc versions choke.
    #pragma omp parallel for                                                   \
            shared(outframes, params, rcs, recipe, usedframes)
    for (nifu = 1; nifu &lt;= kMuseNumIFUs; nifu++) {
      muse_processing *proc = muse_processing_new("<xsl:value-of select="$name"/>",
                                                  recipe);
      <xsl:value-of select="$cname"/>_params_t *pars = cpl_malloc(sizeof(<xsl:value-of select="$cname"/>_params_t));
      memcpy(pars, &amp;params, sizeof(<xsl:value-of select="$cname"/>_params_t));
      pars->nifu = nifu;
      int *rci = rcs + (nifu - 1);
      *rci = <xsl:value-of select="$cname"/>_compute(proc, pars);
      if (rci &amp;&amp; (int)cpl_error_get_code() == MUSE_ERROR_CHIP_NOT_LIVE) {
        *rci = 0;
      }
      cpl_free(pars);
      #pragma omp critical(muse_processing_used_frames)
      cpl_frameset_join(usedframes, proc->usedframes);
      #pragma omp critical(muse_processing_output_frames)
      cpl_frameset_join(outframes, proc->outframes);
      muse_processing_delete(proc);
    } /* for nifu */
    /* non-parallel loop to propagate the "worst" return code;       *
     * since we only ever return -1, any non-zero code is propagated */
    for (nifu = 1; nifu &lt;= kMuseNumIFUs; nifu++) {
      if (rcs[nifu-1] != 0) {
        rc = rcs[nifu-1];
      } /* if */
    } /* for nifu */
    cpl_free(rcs);
  } else { /* serial processing */
    for (params.nifu = 1; params.nifu &lt;= kMuseNumIFUs &amp;&amp; !rc; params.nifu++) {
      muse_processing *proc = muse_processing_new("<xsl:value-of select="$name"/>",
                                                  recipe);
      rc = <xsl:value-of select="$cname"/>_compute(proc, &amp;params);
      if (rc &amp;&amp; (int)cpl_error_get_code() == MUSE_ERROR_CHIP_NOT_LIVE) {
        rc = 0;
      }
      cpl_frameset_join(usedframes, proc->usedframes);
      cpl_frameset_join(outframes, proc->outframes);
      muse_processing_delete(proc);
    } /* for nifu */
  } /* else */
  UNUSED_ARGUMENT(donotmerge); /* maybe this is not going to be used below */
</xsl:when>
  <xsl:otherwise>
  muse_processing *proc = muse_processing_new("<xsl:value-of select="$name"/>",
                                              recipe);
  int rc = <xsl:value-of select="$cname"/>_compute(proc, &amp;params);
  cpl_frameset_join(usedframes, proc->usedframes);
  cpl_frameset_join(outframes, proc->outframes);
  muse_processing_delete(proc);
  </xsl:otherwise>
</xsl:choose>
  if (!cpl_errorstate_is_equal(prestate)) {
    /* dump all errors from this recipe in chronological order */
    cpl_errorstate_dump(prestate, CPL_FALSE, muse_cplerrorstate_dump_some);
    /* reset message level to not get the same errors displayed again by esorex */
    cpl_msg_set_level(CPL_MSG_INFO);
  }
  /* clean up duplicates in framesets of used and output frames */
  muse_cplframeset_erase_duplicate(usedframes);
  muse_cplframeset_erase_duplicate(outframes);
<xsl:choose><xsl:when test="$merge">
  /* merge output products from the up to 24 IFUs */
  if (params.merge &amp;&amp; !donotmerge) {
    muse_utils_frameset_merge_frames(outframes, CPL_TRUE);
  }
</xsl:when></xsl:choose>
  /* to get esorex to see our classification (frame groups etc.), *
   * replace the original frameset with the list of used frames   *
   * before appending product output frames                       */
  /* keep the same pointer, so just erase all frames, not delete the frameset */
  muse_cplframeset_erase_all(recipe->frames);
  cpl_frameset_join(recipe->frames, usedframes);
  cpl_frameset_join(recipe->frames, outframes);
  cpl_frameset_delete(usedframes);
  cpl_frameset_delete(outframes);
  return rc;
} /* <xsl:value-of select="$cname"/>_exec() */
</xsl:template>

  <xsl:template name="destroy_plugin">
    <xsl:param name="cname"/>
/*----------------------------------------------------------------------------*/
/**
  @private
  @brief    Destroy what has been created by the 'create' function
  @param    aPlugin   the plugin
  @return   0 if everything is ok, -1 if not called as part of a recipe
 */
/*----------------------------------------------------------------------------*/
static int
<xsl:value-of select="$cname"/>_destroy(cpl_plugin *aPlugin)
{
  /* Get the recipe from the plugin */
  cpl_recipe *recipe;
  if (cpl_plugin_get_type(aPlugin) == CPL_PLUGIN_TYPE_RECIPE) {
    recipe = (cpl_recipe *)aPlugin;
  } else {
    return -1;
  }

  /* Clean up */
  cpl_parameterlist_delete(recipe->parameters);
  muse_processinginfo_delete(recipe);
  return 0;
} /* <xsl:value-of select="$cname"/>_destroy() */
</xsl:template>
  <xsl:template name="init_plugin">
    <xsl:param name="cname"/>
    <xsl:param name="name"/>
    <xsl:param name="synopsis"/>
    <xsl:param name="email"/>
    <xsl:param name="author"/>
/*----------------------------------------------------------------------------*/
/**
  @private
  @brief    Add this recipe to the list of available plugins.
  @param    aList   the plugin list
  @return   0 if everything is ok, -1 otherwise (but this cannot happen)

  Create the recipe instance and make it available to the application using the
  interface. This function is exported.
 */
/*----------------------------------------------------------------------------*/
int
cpl_plugin_get_info(cpl_pluginlist *aList)
{
  cpl_recipe *recipe = cpl_calloc(1, sizeof *recipe);
  cpl_plugin *plugin = &amp;recipe->interface;

  char *helptext;
  if (muse_cplframework() == MUSE_CPLFRAMEWORK_ESOREX) {
    helptext = cpl_sprintf("%s%s", <xsl:value-of select="$cname"/>_help,
                           <xsl:value-of select="$cname"/>_help_esorex);
  } else {
    helptext = cpl_sprintf("%s", <xsl:value-of select="$cname"/>_help);
  }

  /* Initialize the CPL plugin stuff for this module */
  cpl_plugin_init(plugin, CPL_PLUGIN_API, MUSE_BINARY_VERSION,
                  CPL_PLUGIN_TYPE_RECIPE,
                  "<xsl:value-of select="$name"/>",
                  "<xsl:call-template name="str:replace">
		    <xsl:with-param name="string" select="normalize-space($synopsis)"/>
		    <xsl:with-param name="search">"</xsl:with-param>
		    <xsl:with-param name="replace">\"</xsl:with-param>
		  </xsl:call-template>",
                  helptext,
                  "<xsl:value-of select="$author"/>",
                  "https://support.eso.org",
                  muse_get_license(),
                  <xsl:value-of select="$cname"/>_create,
                  <xsl:value-of select="$cname"/>_exec,
                  <xsl:value-of select="$cname"/>_destroy);
  cpl_pluginlist_append(aList, plugin);
  cpl_free(helptext);

  return 0;
} /* cpl_plugin_get_info() */
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

  <xsl:output method="text"/>

  <xsl:template match="/">
    <xsl:apply-templates select="*"/>
  </xsl:template>


  <xsl:template match="plugins">
    <xsl:text>#******************************************************************************
# E.S.O. - VLT project
# $</xsl:text><xsl:text>Id</xsl:text><xsl:text>$
# $</xsl:text><xsl:text>Author</xsl:text><xsl:text>$
# $</xsl:text><xsl:text>Date</xsl:text><xsl:text>$
# $</xsl:text><xsl:text>Revision</xsl:text><xsl:text>$
#
#******************************************************************************
Dictionary Name:   ESO-DFS-DIC.MUSE_QC
Scope:             QC
Source:            ESO DFS/SDD
Revision:          1.0
Date:              2013-08-12
Status:            submitted
Description:       MUSE Quality Control dictionary.
    </xsl:text>
    <xsl:apply-templates select="plugin" mode="load"/>
  </xsl:template>

  <xsl:template match="plugin" mode="load">
    <xsl:apply-templates select="document(concat('../recipes/', @name, '.xml'))/pluginlist"/>
  </xsl:template>

  <xsl:template match="pluginlist">
    <xsl:apply-templates select="plugin"/>
  </xsl:template>

  <xsl:template match="plugin">
    <xsl:if test="frames/frameset/frame[@group='product']/fitsheader/property">
<xsl:text>
#******************************************************************************
# Recipe: </xsl:text><xsl:value-of select="@name"/><xsl:text>
# </xsl:text><xsl:value-of select="plugininfo/synopsis"/><xsl:text>
</xsl:text>
    <xsl:apply-templates select="frames"/>
  </xsl:if>
  </xsl:template>

  <xsl:template match="frames">
    <xsl:apply-templates select="frameset"/>
  </xsl:template>

  <xsl:template match="frameset">
    <xsl:if test="frame[@group='product']/fitsheader/property">
    <xsl:text>
# Input frame: </xsl:text>
    <xsl:value-of select="frame[@group='raw']/@tag"/>
    <xsl:apply-templates select="frame[@group='product']"/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="frame[@group='product']">
    <xsl:if test="fitsheader/property">
<xsl:text>
# Product frame: </xsl:text><xsl:value-of select="@tag"/>
<xsl:text> (</xsl:text><xsl:value-of select="@level"/>
<xsl:text>)
</xsl:text>
<xsl:apply-templates select="fitsheader"/>
  </xsl:if>
  </xsl:template>

  <xsl:template match="fitsheader">
<!--    <xsl:apply-templates select="parameter"/> -->
    <xsl:apply-templates select="property"/>
  </xsl:template>

  <xsl:template match="parameter">
    <xsl:text># </xsl:text><xsl:value-of select="@id"/><xsl:text>: </xsl:text>
    <xsl:value-of select="@name"/><xsl:text> parameter (</xsl:text><xsl:value-of select="@min"/><xsl:text>...</xsl:text><xsl:value-of select="@max"/><xsl:text>)
</xsl:text>
  </xsl:template>

  <xsl:template match="property">
    <xsl:text>
Parameter Name: </xsl:text><xsl:value-of select="translate(substring(name, 5), 'ijklmn', 'iiiiii')"/>
    <xsl:text>
Class:          header|qc-log
Context:        process
Type:           </xsl:text><xsl:value-of select="@type"/><xsl:text>
Value Format:   </xsl:text><xsl:choose>
<xsl:when test="@type = 'float'"><xsl:text>%e</xsl:text></xsl:when>
<xsl:when test="@type = 'double'"><xsl:text>%e</xsl:text></xsl:when>
<xsl:when test="@type = 'int'"><xsl:text>%i</xsl:text></xsl:when>
<xsl:when test="@type = 'string'"><xsl:text>%s</xsl:text></xsl:when>
</xsl:choose>
<xsl:text>
Unit:           </xsl:text><xsl:value-of select="unit"/><xsl:text>
Comment field:  </xsl:text>
<xsl:if test="unit"><xsl:text>[</xsl:text><xsl:value-of select="unit"/><xsl:text>] </xsl:text></xsl:if><xsl:value-of select="description"/><xsl:text>
Description:    </xsl:text><xsl:value-of select="description"/><xsl:text>
</xsl:text>
  </xsl:template>
</xsl:stylesheet>

Source: cpl-plugin-muse
Maintainer: Debian Astro Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libcpl-dev,
               libgsl-dev,
               pkg-config,
               python3,
               python3-astropy,
               python3-cpl
Build-Depends-Indep: dh-sequence-sphinxdoc,
                     python3-sphinx
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/debian-astro-team/cpl-plugin-muse
Vcs-Git: https://salsa.debian.org/debian-astro-team/cpl-plugin-muse.git
Homepage: https://www.eso.org/sci/software/pipelines/muse
Rules-Requires-Root: no

Package: cpl-plugin-muse
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends}
Recommends: esorex|python3-cpl
Suggests: cpl-plugin-muse-calib,
          cpl-plugin-muse-doc
Description: ESO data reduction pipeline for the MUSE instrument
 This is the data reduction pipeline for the Muse instrument of the
 Very Large Telescope (VLT) from the European Southern Observatory (ESO).
 .
 MUSE, the Multi-Unit Spectroscopic Explorer, is an Integral Field
 Spectrograph located at the Nasmyth B focus of Yepun, the VLT UT4
 telescope. It has a modular structure composed of 24 identical IFU modules
 that together sample, in Wide Field Mode (WFM), a near-contiguous 1 squared
 arcmin field of view. Spectrally the instrument samples almost the full
 optical domain with a mean resolution of 3000. Spatially, the instrument
 samples the sky with 0.2 arcseconds spatial pixels in the currently offered
 Wide Field Mode with natural seeing (WFM-noAO).

Package: cpl-plugin-muse-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Description: ESO data reduction pipeline documentation for MUSE
 This package contains the HTML documentation and manpages for the data
 reduction pipeline for the Muse instrument of the Very Large Telescope
 (VLT) from the European Southern Observatory (ESO).

Package: cpl-plugin-muse-calib
Architecture: all
Multi-Arch: foreign
Section: contrib/science
Depends: ${misc:Depends},
         cpl-plugin-muse,
         wget
Description: ESO data reduction pipeline calibration data downloader for MUSE
 This package downloads calibration and other static data of the
 data reduction pipeline for the Muse instrument of the
 Very Large Telescope (VLT) from the European Southern Observatory (ESO).
 .
 MUSE, the Multi-Unit Spectroscopic Explorer, is an Integral Field
 Spectrograph located at the Nasmyth B focus of Yepun, the VLT UT4
 telescope. It has a modular structure composed of 24 identical IFU modules
 that together sample, in Wide Field Mode (WFM), a near-contiguous 1 squared
 arcmin field of view. Spectrally the instrument samples almost the full
 optical domain with a mean resolution of 3000. Spatially, the instrument
 samples the sky with 0.2 arcseconds spatial pixels in the currently offered
 Wide Field Mode with natural seeing (WFM-noAO).

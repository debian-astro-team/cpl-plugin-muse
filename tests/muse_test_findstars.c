/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/*
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <muse.h>

/*----------------------------------------------------------------------------*
 *                             Debugging/feature Macros                       *
 *----------------------------------------------------------------------------*/
#define DEBUG_MUSE_TESTFINDSTARS 0  /* if 1 enable debugging output */

/* If BASEFILENAME is undefined, try the local working directory *
 * so that the filename construction below is correct            */
#ifndef BASEFILENAME
#  define BASEFILENAME "."
#endif

/*----------------------------------------------------------------------------*/
/**
  @brief    Test program to check that all the functions in muse_findstars.c
            work correctly.

  This program explicitly tests
    muse_find_stars
 */
/*----------------------------------------------------------------------------*/
int main(int argc, char **argv)
{
  UNUSED_ARGUMENTS(argc, argv);
  cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_DEBUG);

  const cpl_size next[4] = {0, 1, 1, 1};
  const char *inames[4] = {"sgsImage.fits",  "fovImage1.fits",
                           "fovImage2.fits", "fovImage3.fits"};

#if DEBUG_MUSE_TESTFINDSTARS > 0
  const char *tnames[4] = {"sgsStars.fits",  "fovStars1.fits",
                           "fovStars2.fits", "fovStars3.fits"};
#endif

  /* Use default sharpness and roundness criteria for all tests */
  const double *roundlimits = NULL;
  const double *sharplimits = NULL;

  const double hmin[4] = {100., 15., 15., 25.};
  const double fwhm[4] = {15., 5., 5., 5.};

  /* Expected number of detections for each test case */
  const cpl_size ndetect[4] = {6, 28, 30, 72};

  cpl_size itest;
  for (itest = 0; (size_t)itest < (sizeof inames / sizeof inames[0]); ++itest) {
    char *fname = cpl_sprintf("%s/%s", BASEFILENAME, inames[itest]);
    cpl_image *raw_image = cpl_image_load(fname, CPL_TYPE_DOUBLE, 0 ,
                                          next[itest]);
    cpl_free(fname);
    cpl_test(raw_image != NULL);

    cpl_errorstate ps = cpl_errorstate_get();
    cpl_table *stars = muse_find_stars(raw_image, hmin[itest], fwhm[itest],
                                       roundlimits, sharplimits);
    cpl_test(cpl_errorstate_is_equal(ps));
    cpl_test(stars != NULL);
    cpl_test(cpl_table_get_nrow(stars) == ndetect[itest]);

#if DEBUG_MUSE_TESTFINDSTARS > 0
    cpl_msg_debug(cpl_func, "Source detection pass %" CPL_SIZE_FORMAT
                  " on image %s", itest + 1, inames[itest]);
    cpl_table_dump(stars, 0, cpl_table_get_nrow(stars), stderr);
    cpl_table_save(stars, 0, 0, tnames[itest], CPL_IO_CREATE);
#endif

    cpl_table_delete(stars);
    cpl_image_delete(raw_image);
  }
  return cpl_test_end(0);
}

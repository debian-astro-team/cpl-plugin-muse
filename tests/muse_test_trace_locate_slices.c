/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/*
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2007-2014 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <muse.h>

/*----------------------------------------------------------------------------*/
/**
  @brief    short test program to check that muse_trace_locate_slices() does
            what it should
 */
/*----------------------------------------------------------------------------*/
int main(int argc, char **argv)
{
  cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_DEBUG);

  cpl_image *image = NULL;
  if (argc == 2) {
    /* assume that the file passed on the command line is an input image */
    cpl_msg_info(__func__, "command line argument detected, will try to load %s",
                 argv[1]);
    image = cpl_image_load(argv[1], CPL_TYPE_FLOAT, 0, 0);
  } else {
    image = cpl_image_load(BASEFILENAME"_in.fits", CPL_TYPE_FLOAT, 0, 0);
  }
  if (!image) {
    return cpl_test_end(2);
  } else {
    cpl_msg_info(__func__, "image %"CPL_SIZE_FORMAT"x%"CPL_SIZE_FORMAT,
                 cpl_image_get_size_x(image), cpl_image_get_size_y(image));
  }
  cpl_vector *vector = cpl_vector_new_from_image_row(image, 1);
  double cputime = cpl_test_get_cputime();
  /* use default values from muse_masterflat */
  cpl_vector *vresult = muse_trace_locate_slices(vector, 48, 0.7, 10);
  double diff = cpl_test_get_cputime() - cputime;
  if (vector && vresult) {
    cpl_msg_info(__func__, "muse_trace_locate_slices succeeded, took %gs",
                 diff);
    cpl_test(cpl_vector_get_size(vresult) == 48);
    /* centers should increase monotonically, with a step of at least 75 pix */
    int i;
    for (i = 1; i < cpl_vector_get_size(vresult); i++) {
      cpl_test(cpl_vector_get(vresult, i) - cpl_vector_get(vresult, i-1) > 75.);
    }
    cpl_vector_delete(vresult);
  }
  cpl_image_delete(image);
  cpl_vector_delete(vector);

  return cpl_test_end(0);
}

/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/* 
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* This file was automatically generated */

#ifndef MUSE_TEST_PROCESSING_Z_H
#define MUSE_TEST_PROCESSING_Z_H

/*----------------------------------------------------------------------------*
 *                              Includes                                      *
 *----------------------------------------------------------------------------*/
#include <muse.h>
#include <muse_instrument.h>

/*----------------------------------------------------------------------------*
 *                          Special variable types                            *
 *----------------------------------------------------------------------------*/

/** @addtogroup recipe_muse_test_processing */
/**@{*/

/*----------------------------------------------------------------------------*/
/**
  @brief   Structure to hold the parameters of the muse_test_processing recipe.

  This structure contains the parameters for the recipe that may be set on the
  command line, in the configuration, or through the environment.
 */
/*----------------------------------------------------------------------------*/
typedef struct muse_test_processing_params_s {
  /** @brief   IFU number to handle, and an integer */
  int nifu;

  /** @brief   A double value */
  double adouble;

  /** @brief   A boolean value */
  int aboolean;

  /** @brief   A string value */
  const char * astring;

  /** @brief   An enumeration value */
  int anenum;
  /** @brief   An enumeration value (as string) */
  const char *anenum_s;

  char __dummy__; /* quieten compiler warning about possibly empty struct */
} muse_test_processing_params_t;

#define MUSE_TEST_PROCESSING_PARAM_ANENUM_VALUE1 1
#define MUSE_TEST_PROCESSING_PARAM_ANENUM_VALUE2 2
#define MUSE_TEST_PROCESSING_PARAM_ANENUM_VALUE3 3
#define MUSE_TEST_PROCESSING_PARAM_ANENUM_INVALID_VALUE -1

/**@}*/

/*----------------------------------------------------------------------------*
 *                           Function prototypes                              *
 *----------------------------------------------------------------------------*/
int muse_test_processing_compute(muse_processing *, muse_test_processing_params_t *);

#endif /* MUSE_TEST_PROCESSING_Z_H */

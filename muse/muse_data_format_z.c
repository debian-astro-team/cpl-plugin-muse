/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/* 
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* This file was automatically generated */

#include "muse_cplwrappers.h"
#include "muse_data_format_z.h"

/*----------------------------------------------------------------------------*/
/**
   @defgroup muse_data_format_z   External FITS table formats

   The MUSE pipeline uses and produces a number of FITS tables in
   different formats, which are described in this section.
 */
/**@{*/
      
/*----------------------------------------------------------------------------*/
/**
      Euro3D format. See Format Definition Document, Kissler-Patig et
      al., Issue 1.2, May 2003, for a description.

      Contrary to the examples in the Euro3D specs we use floats instead of
      doubles for the entries in the group table. This is because the E3D
      tool is otherwise not able to read the values correctly.

      This data format may be written alternatively to the common
      DATACUBE format, if the parameter "format" is set to "Euro3D" or
      "xEuro3D".
    
      Columns:
    
      - '<tt>SPEC_ID</tt>': Spectrum identifier [int]
      - '<tt>SELECTED</tt>': Selection flag [int]
      - '<tt>NSPAX</tt>': Number of instrument spaxels composing the spectrum [int]
      - '<tt>SPEC_LEN</tt>': Useful number of spectral elements [int]
      - '<tt>SPEC_STA</tt>': Starting wavelength of spectrum [int]
      - '<tt>XPOS</tt>': Horizontal position [double]
      - '<tt>YPOS</tt>': Vertical position [double]
      - '<tt>GROUP_N</tt>': Group number [int]
      - '<tt>SPAX_ID</tt>': Spaxel identifier [string]
      - '<tt>DATA_SPE</tt>': Data spectrum [float array]
      - '<tt>QUAL_SPE</tt>': Data quality spectrum [int array]
      - '<tt>STAT_SPE</tt>': Associated statistical error spectrum [float array]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_euro3dcube_e3d_data_def[] = {
  { "SPEC_ID", CPL_TYPE_INT, NULL, "%5d",
    "Spectrum identifier", CPL_TRUE},
  { "SELECTED", CPL_TYPE_INT, NULL, "%1d",
    "Selection flag", CPL_TRUE},
  { "NSPAX", CPL_TYPE_INT, NULL, "%d",
    "Number of instrument spaxels composing the spectrum", CPL_TRUE},
  { "SPEC_LEN", CPL_TYPE_INT, "pixel", "%ld",
    "Useful number of spectral elements", CPL_TRUE},
  { "SPEC_STA", CPL_TYPE_INT, "pixel", "%7d",
    "Starting wavelength of spectrum", CPL_TRUE},
  { "XPOS", CPL_TYPE_DOUBLE, "pix", "%f",
    "Horizontal position", CPL_TRUE},
  { "YPOS", CPL_TYPE_DOUBLE, "pix", "%f",
    "Vertical position", CPL_TRUE},
  { "GROUP_N", CPL_TYPE_INT, NULL, "%d",
    "Group number", CPL_TRUE},
  { "SPAX_ID", CPL_TYPE_STRING, NULL, "%s",
    "Spaxel identifier", CPL_TRUE},
  { "DATA_SPE", CPL_TYPE_FLOAT | CPL_TYPE_POINTER, NULL, "%1.5e",
    "Data spectrum", CPL_TRUE},
  { "QUAL_SPE", CPL_TYPE_INT | CPL_TYPE_POINTER, NULL, "%010x",
    "Data quality spectrum", CPL_TRUE},
  { "STAT_SPE", CPL_TYPE_FLOAT | CPL_TYPE_POINTER, NULL, "%1.5e",
    "Associated statistical error spectrum", CPL_TRUE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};


/*----------------------------------------------------------------------------*/
/**
      Euro3D format. See Format Definition Document, Kissler-Patig et
      al., Issue 1.2, May 2003, for a description.

      Contrary to the examples in the Euro3D specs we use floats instead of
      doubles for the entries in the group table. This is because the E3D
      tool is otherwise not able to read the values correctly.

      This data format may be written alternatively to the common
      DATACUBE format, if the parameter "format" is set to "Euro3D" or
      "xEuro3D".
    
      Columns:
    
      - '<tt>GROUP_N</tt>': Group number [int]
      - '<tt>G_SHAPE</tt>': Spaxel shape keyword [string]
      - '<tt>G_SIZE1</tt>': Horizontal size per spaxel [float]
      - '<tt>G_ANGLE</tt>': Angle of spaxel on the sky [float]
      - '<tt>G_SIZE2</tt>': Vertical size per spaxel [float]
      - '<tt>G_POSWAV</tt>': Wavelength for which the WCS is valid [float]
      - '<tt>G_AIRMAS</tt>': Airmass [float]
      - '<tt>G_PARANG</tt>': Parallactic angle [float]
      - '<tt>G_PRESSU</tt>': Pressure [float]
      - '<tt>G_TEMPER</tt>': Temperature [float]
      - '<tt>G_HUMID</tt>': Humidity [float]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_euro3dcube_e3d_grp_def[] = {
  { "GROUP_N", CPL_TYPE_INT, NULL, "%1d",
    "Group number", CPL_TRUE},
  { "G_SHAPE", CPL_TYPE_STRING, NULL, "%s",
    "Spaxel shape keyword", CPL_TRUE},
  { "G_SIZE1", CPL_TYPE_FLOAT, "arcsec", "%1.5e",
    "Horizontal size per spaxel", CPL_TRUE},
  { "G_ANGLE", CPL_TYPE_FLOAT, "deg", "%1.5e",
    "Angle of spaxel on the sky", CPL_TRUE},
  { "G_SIZE2", CPL_TYPE_FLOAT, "arcsec", "%1.5e",
    "Vertical size per spaxel", CPL_TRUE},
  { "G_POSWAV", CPL_TYPE_FLOAT, "Angstrom", "%1.5e",
    "Wavelength for which the WCS is valid", CPL_TRUE},
  { "G_AIRMAS", CPL_TYPE_FLOAT, NULL, "%1.5e",
    "Airmass", CPL_TRUE},
  { "G_PARANG", CPL_TYPE_FLOAT, "deg", "%1.5e",
    "Parallactic angle", CPL_TRUE},
  { "G_PRESSU", CPL_TYPE_FLOAT, "hPa", "%1.5e",
    "Pressure", CPL_TRUE},
  { "G_TEMPER", CPL_TYPE_FLOAT, "K", "%1.5e",
    "Temperature", CPL_TRUE},
  { "G_HUMID", CPL_TYPE_FLOAT, NULL, "%1.5e",
    "Humidity", CPL_TRUE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};


/*----------------------------------------------------------------------------*/
/**
      Spectral data from a datacube
    
      Columns:
    
      - '<tt>lambda</tt>': Wavelength [double]
      - '<tt>data</tt>': Data values [double]
      - '<tt>stat</tt>': Data variance [double]
      - '<tt>dq</tt>': Quality flag of data values [int]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_dataspectrum_def[] = {
  { "lambda", CPL_TYPE_DOUBLE, "Angstrom", "%7.2f",
    "Wavelength", CPL_TRUE},
  { "data", CPL_TYPE_DOUBLE, "count", "%1.5e",
    "Data values", CPL_TRUE},
  { "stat", CPL_TYPE_DOUBLE, "count", "%1.5e",
    "Data variance", CPL_TRUE},
  { "dq", CPL_TYPE_INT, NULL, "%#x",
    "Quality flag of data values", CPL_TRUE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};


#ifdef XML_TABLEDEF_TRACE_TABLE /* currently not used */

/*----------------------------------------------------------------------------*/
/**
      This file gives the trace solution for each slice in the form of
      a polynomial. It is a FITS table with 48 rows, one for each
      slice.
    
      Columns:
    
      - '<tt>SliceNo</tt>': Slice number [int]
      - '<tt>Width</tt>': Average slice width [float]
      - '<tt>tc0_ij</tt>': polynomial coefficients for the central trace solution [double]
      - '<tt>MSE0</tt>': mean squared error of fit (central solution) [double]
      - '<tt>tc1_ij</tt>': polynomial coefficients for the left-edge trace solution [double]
      - '<tt>MSE1</tt>': mean squared error of fit (left-edge solution) [double]
      - '<tt>tc2_ij</tt>': polynomial coefficients for the right-edge trace solution [double]
      - '<tt>MSE2</tt>': mean squared error of fit (right-edge solution) [double]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_trace_table_def[] = {
  { "SliceNo", CPL_TYPE_INT, NULL, "%7d",
    "Slice number", CPL_TRUE},
  { "Width", CPL_TYPE_FLOAT, NULL, "%1.5e",
    "Average slice width", CPL_TRUE},
  { "tc0_ij", CPL_TYPE_DOUBLE, NULL, "%1.5e",
    "polynomial coefficients for the central trace solution", CPL_TRUE},
  { "MSE0", CPL_TYPE_DOUBLE, NULL, "%1.5e",
    "mean squared error of fit (central solution)", CPL_TRUE},
  { "tc1_ij", CPL_TYPE_DOUBLE, NULL, "%1.5e",
    "polynomial coefficients for the left-edge trace solution", CPL_TRUE},
  { "MSE1", CPL_TYPE_DOUBLE, NULL, "%1.5e",
    "mean squared error of fit (left-edge solution)", CPL_TRUE},
  { "tc2_ij", CPL_TYPE_DOUBLE, NULL, "%1.5e",
    "polynomial coefficients for the right-edge trace solution", CPL_TRUE},
  { "MSE2", CPL_TYPE_DOUBLE, NULL, "%1.5e",
    "mean squared error of fit (right-edge solution)", CPL_TRUE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};

#endif /* XML_TABLEDEF_TRACE_TABLE */

#ifdef XML_TABLEDEF_TRACE_SAMPLES /* currently not used */

/*----------------------------------------------------------------------------*/
/**
      This is an optional FITS table, output on request by the
      muse_flat recipe. It can be used to verify the quality of the
      tracing, i.e. find out how accurate the pipeline was able to
      determine the location and boundary of the slices on the CCD.
    
      Columns:
    
      - '<tt>slice</tt>': Slice number [int]
      - '<tt>y</tt>': y position on the CCD [float]
      - '<tt>mid</tt>': Midpoint of the slice at this y position [float]
      - '<tt>left</tt>': Left edge of the slice at this y position [float]
      - '<tt>right</tt>': Right edge of the slice at this y position [float]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_trace_samples_def[] = {
  { "slice", CPL_TYPE_INT, NULL, "%7d",
    "Slice number", CPL_TRUE},
  { "y", CPL_TYPE_FLOAT, "pix", "%1.5e",
    "y position on the CCD", CPL_TRUE},
  { "mid", CPL_TYPE_FLOAT, "pix", "%1.5e",
    "Midpoint of the slice at this y position", CPL_TRUE},
  { "left", CPL_TYPE_FLOAT, "pix", "%1.5e",
    "Left edge of the slice at this y position", CPL_TRUE},
  { "right", CPL_TYPE_FLOAT, "pix", "%1.5e",
    "Right edge of the slice at this y position", CPL_TRUE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};

#endif /* XML_TABLEDEF_TRACE_SAMPLES */

#ifdef XML_TABLEDEF_WAVECAL_TABLE /* currently not used */

/*----------------------------------------------------------------------------*/
/**
      This file gives the dispersion solution for each slice in one
      IFU. It is a FITS table with 48 rows, one for each slice.
    
      Columns:
    
      - '<tt>SliceNo</tt>': Slice number [int]
      - '<tt>wlcIJ</tt>': Polynomial coefficients for the wavelength solution [double]
      - '<tt>MSE</tt>': Mean squared error of fit [double]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_wavecal_table_def[] = {
  { "SliceNo", CPL_TYPE_INT, NULL, "%7d",
    "Slice number", CPL_TRUE},
  { "wlcIJ", CPL_TYPE_DOUBLE, NULL, "%1.5e",
    "Polynomial coefficients for the wavelength solution", CPL_TRUE},
  { "MSE", CPL_TYPE_DOUBLE, NULL, "%1.5e",
    "Mean squared error of fit", CPL_TRUE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};

#endif /* XML_TABLEDEF_WAVECAL_TABLE */

#ifdef XML_TABLEDEF_WAVECAL_RESIDUALS /* currently not used */

/*----------------------------------------------------------------------------*/
/**
      This is an optional FITS table, output on request by the
      muse_wavecal recipe. It can be used to verify the quality of the
      wavelength solution.
    
      Columns:
    
      - '<tt>slice</tt>': Slice number [int]
      - '<tt>iteration</tt>': Iteration [int]
      - '<tt>x</tt>': x position on the CCD [int]
      - '<tt>y</tt>': y position on the CCD [int]
      - '<tt>lambda</tt>': Wavelength [float]
      - '<tt>residual</tt>': Residual at this point [double]
      - '<tt>rejlimit</tt>': Rejection limit for this iteration [double]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_wavecal_residuals_def[] = {
  { "slice", CPL_TYPE_INT, NULL, "%7d",
    "Slice number", CPL_TRUE},
  { "iteration", CPL_TYPE_INT, NULL, "%7d",
    "Iteration", CPL_TRUE},
  { "x", CPL_TYPE_INT, "pix", "%7d",
    "x position on the CCD", CPL_TRUE},
  { "y", CPL_TYPE_INT, "pix", "%7d",
    "y position on the CCD", CPL_TRUE},
  { "lambda", CPL_TYPE_FLOAT, "Angstrom", "%1.5e",
    "Wavelength", CPL_TRUE},
  { "residual", CPL_TYPE_DOUBLE, "Angstrom", "%1.5e",
    "Residual at this point", CPL_TRUE},
  { "rejlimit", CPL_TYPE_DOUBLE, "Angstrom", "%1.5e",
    "Rejection limit for this iteration", CPL_TRUE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};

#endif /* XML_TABLEDEF_WAVECAL_RESIDUALS */

/*----------------------------------------------------------------------------*/
/**
      This file contains the line spread function for all slices of one IFU.

      It may come in two formats:
      one contains a datacube with a 2D image description of the LSF per slice;
      the other is a table with parameters of a Gauss-Hermite parametrization.

      The pipeline automatically detects the format and continues processing
      accordingly.
    
      Columns:
    
      - '<tt>ifu</tt>': IFU number [int]
      - '<tt>slice</tt>': Slice number within the IFU [int]
      - '<tt>sensitivity</tt>': Detector sensitivity, relative to the reference [double array]
      - '<tt>offset</tt>': Wavelength calibration offset [double]
      - '<tt>refraction</tt>': Relative refraction index [double]
      - '<tt>slit_width</tt>': Slit width [double]
      - '<tt>bin_width</tt>': Bin width [double]
      - '<tt>lsf_width</tt>': LSF gauss-hermitean width [double array]
      - '<tt>hermit3</tt>': 3th order hermitean coefficient [double array]
      - '<tt>hermit4</tt>': 4th order hermitean coefficient [double array]
      - '<tt>hermit5</tt>': 5th order hermitean coefficient [double array]
      - '<tt>hermit6</tt>': 6th order hermitean coefficient [double array]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_lsf_profile_def[] = {
  { "ifu", CPL_TYPE_INT, NULL, "%7d",
    "IFU number", CPL_TRUE},
  { "slice", CPL_TYPE_INT, NULL, "%7d",
    "Slice number within the IFU", CPL_TRUE},
  { "sensitivity", CPL_TYPE_DOUBLE | CPL_TYPE_POINTER, NULL, "%1.5e",
    "Detector sensitivity, relative to the reference", CPL_TRUE},
  { "offset", CPL_TYPE_DOUBLE, NULL, "%1.5e",
    "Wavelength calibration offset", CPL_TRUE},
  { "refraction", CPL_TYPE_DOUBLE, NULL, "%1.5e",
    "Relative refraction index", CPL_TRUE},
  { "slit_width", CPL_TYPE_DOUBLE, NULL, "%1.5e",
    "Slit width", CPL_TRUE},
  { "bin_width", CPL_TYPE_DOUBLE, NULL, "%1.5e",
    "Bin width", CPL_TRUE},
  { "lsf_width", CPL_TYPE_DOUBLE | CPL_TYPE_POINTER, NULL, "%1.5e",
    "LSF gauss-hermitean width", CPL_TRUE},
  { "hermit3", CPL_TYPE_DOUBLE | CPL_TYPE_POINTER, NULL, "%1.5e",
    "3th order hermitean coefficient", CPL_TRUE},
  { "hermit4", CPL_TYPE_DOUBLE | CPL_TYPE_POINTER, NULL, "%1.5e",
    "4th order hermitean coefficient", CPL_TRUE},
  { "hermit5", CPL_TYPE_DOUBLE | CPL_TYPE_POINTER, NULL, "%1.5e",
    "5th order hermitean coefficient", CPL_TRUE},
  { "hermit6", CPL_TYPE_DOUBLE | CPL_TYPE_POINTER, NULL, "%1.5e",
    "6th order hermitean coefficient", CPL_TRUE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};


#ifdef XML_TABLEDEF_GEOMETRY_TABLE /* currently not used */

/*----------------------------------------------------------------------------*/
/**
      This file provides the relative location of each slice in the
      MUSE field of view. It contains one table of 24x48 = 1152 rows,
      one for each slice.

      Other columns (e.g. columns containing errors estimates of the slice
      properties, xerr, yerr, ...) may be present in this table but are ignored
      by the MUSE pipeline.
    
      Columns:
    
      - '<tt>SubField</tt>': sub-field (IFU / channel) number [int]
      - '<tt>SliceCCD</tt>': Slice number on the CCD, counted from left to right [int]
      - '<tt>SliceSky</tt>': Slice number on the sky [int]
      - '<tt>x</tt>': x position within field of view [double]
      - '<tt>y</tt>': y position within field of view [double]
      - '<tt>angle</tt>': Rotation angle of slice [double]
      - '<tt>width</tt>': Width of slice within field of view [double]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_geometry_table_def[] = {
  { "SubField", CPL_TYPE_INT, NULL, "%02d",
    "sub-field (IFU / channel) number", CPL_TRUE},
  { "SliceCCD", CPL_TYPE_INT, NULL, "%02d",
    "Slice number on the CCD, counted from left to right", CPL_TRUE},
  { "SliceSky", CPL_TYPE_INT, NULL, "%02d",
    "Slice number on the sky", CPL_TRUE},
  { "x", CPL_TYPE_DOUBLE, "pix", "%9.4f",
    "x position within field of view", CPL_TRUE},
  { "y", CPL_TYPE_DOUBLE, "pix", "%9.4f",
    "y position within field of view", CPL_TRUE},
  { "angle", CPL_TYPE_DOUBLE, "deg", "%6.3f",
    "Rotation angle of slice", CPL_TRUE},
  { "width", CPL_TYPE_DOUBLE, "pix", "%.2f",
    "Width of slice within field of view", CPL_TRUE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};

#endif /* XML_TABLEDEF_GEOMETRY_TABLE */

#ifdef XML_TABLEDEF_SPOTS_TABLE /* currently not used */

/*----------------------------------------------------------------------------*/
/**
      This file lists all detections and properties of all spots (the image of a
      pinhole at one arc line) during geometrical calibration.

      It is thought to be used for debugging of the geometrical calibration.
    
      Columns:
    
      - '<tt>filename</tt>': (Raw) filename from which this measurement originates [string]
      - '<tt>image</tt>': Number of the image in the series [int]
      - '<tt>POSENC2</tt>': X position of the mask in encoder steps [int]
      - '<tt>POSPOS2</tt>': X position of the mask [double]
      - '<tt>POSENC3</tt>': Y position of the mask in encoder steps [int]
      - '<tt>POSPOS3</tt>': Y position of the mask [double]
      - '<tt>POSENC4</tt>': Z position of the mask in encoder steps [int]
      - '<tt>POSPOS4</tt>': Z position of the mask [double]
      - '<tt>VPOS</tt>': Real vertical position of the mask [double]
      - '<tt>ScaleFOV</tt>': Focus scale in VLT focal plane (from the FITS header) [double]
      - '<tt>SubField</tt>': Sub-field number [int]
      - '<tt>SliceCCD</tt>': Slice number as counted on the CCD [int]
      - '<tt>lambda</tt>': Wavelength [double]
      - '<tt>SpotNo</tt>': Number of this spot within the slice (1 is left, 2 is the central one, 3 is right within the slice) [int]
      - '<tt>xc</tt>': x center of this spot on the CCD [double]
      - '<tt>yc</tt>': y center of this spot on the CCD [double]
      - '<tt>xfwhm</tt>': FWHM in x-direction on the CCD [double]
      - '<tt>yfwhm</tt>': FWHM in y-direction on the CCD [double]
      - '<tt>flux</tt>': Flux of the spot as integrated on the CCD image [double]
      - '<tt>bg</tt>': Background level around the spot [double]
      - '<tt>dxcen</tt>': distance to center of slice at vertical position yc (positive: right of center) [double]
      - '<tt>twidth</tt>': trace width of the slice at the vertical CCD position of the spot [double]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_spots_table_def[] = {
  { "filename", CPL_TYPE_STRING, NULL, "%s",
    "(Raw) filename from which this measurement originates", CPL_TRUE},
  { "image", CPL_TYPE_INT, NULL, "%03d",
    "Number of the image in the series", CPL_TRUE},
  { "POSENC2", CPL_TYPE_INT, NULL, "%7d",
    "X position of the mask in encoder steps", CPL_TRUE},
  { "POSPOS2", CPL_TYPE_DOUBLE, "mm", "%.3f",
    "X position of the mask", CPL_TRUE},
  { "POSENC3", CPL_TYPE_INT, NULL, "%7d",
    "Y position of the mask in encoder steps", CPL_TRUE},
  { "POSPOS3", CPL_TYPE_DOUBLE, "mm", "%.3f",
    "Y position of the mask", CPL_TRUE},
  { "POSENC4", CPL_TYPE_INT, NULL, "%7d",
    "Z position of the mask in encoder steps", CPL_TRUE},
  { "POSPOS4", CPL_TYPE_DOUBLE, "mm", "%.3f",
    "Z position of the mask", CPL_TRUE},
  { "VPOS", CPL_TYPE_DOUBLE, "mm", "%.3f",
    "Real vertical position of the mask", CPL_TRUE},
  { "ScaleFOV", CPL_TYPE_DOUBLE, "arcsec/mm", "%.3f",
    "Focus scale in VLT focal plane (from the FITS header)", CPL_TRUE},
  { "SubField", CPL_TYPE_INT, NULL, "%02d",
    "Sub-field number", CPL_TRUE},
  { "SliceCCD", CPL_TYPE_INT, NULL, "%02d",
    "Slice number as counted on the CCD", CPL_TRUE},
  { "lambda", CPL_TYPE_DOUBLE, "Angstrom", "%.3f",
    "Wavelength", CPL_TRUE},
  { "SpotNo", CPL_TYPE_INT, NULL, "%04d",
    "Number of this spot within the slice (1 is left, 2 is the central one, 3 is right within the slice)", CPL_TRUE},
  { "xc", CPL_TYPE_DOUBLE, "pix", "%.3f",
    "x center of this spot on the CCD", CPL_TRUE},
  { "yc", CPL_TYPE_DOUBLE, "pix", "%.3f",
    "y center of this spot on the CCD", CPL_TRUE},
  { "xfwhm", CPL_TYPE_DOUBLE, "pix", "%.2f",
    "FWHM in x-direction on the CCD", CPL_TRUE},
  { "yfwhm", CPL_TYPE_DOUBLE, "pix", "%.2f",
    "FWHM in y-direction on the CCD", CPL_TRUE},
  { "flux", CPL_TYPE_DOUBLE, NULL, "%.1f",
    "Flux of the spot as integrated on the CCD image", CPL_TRUE},
  { "bg", CPL_TYPE_DOUBLE, NULL, "%f",
    "Background level around the spot", CPL_TRUE},
  { "dxcen", CPL_TYPE_DOUBLE, "pix", "%f",
    "distance to center of slice at vertical position yc (positive: right of center)", CPL_TRUE},
  { "twidth", CPL_TYPE_DOUBLE, "pix", "%f",
    "trace width of the slice at the vertical CCD position of the spot", CPL_TRUE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};

#endif /* XML_TABLEDEF_SPOTS_TABLE */

/*----------------------------------------------------------------------------*/
/**
      This FITS table contains the results of the slice auto-calibration used
      for deep fields. In particular, it contains the correction factors for
      each wavelength range, slice, and IFU.
      The entries ESO.DRS.MUSE.LAMBDAi.MIN and ESO.DRS.MUSE.LAMBDAi.MAX in the
      FITS header of the table give the wavelength ranges used during auto-
      calibration for each bin.
    
      Columns:
    
      - '<tt>ifu</tt>': IFU number [int]
      - '<tt>sli</tt>': Slice number [int]
      - '<tt>quad</tt>': Wavelength range bin number [int]
      - '<tt>npts</tt>': Number of points used to compute the correction factor [int]
      - '<tt>corr</tt>': Correction factor for the slice in this wavelength range [double]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_autocal_results_def[] = {
  { "ifu", CPL_TYPE_INT, NULL, "%2d",
    "IFU number", CPL_TRUE},
  { "sli", CPL_TYPE_INT, NULL, "%2d",
    "Slice number", CPL_TRUE},
  { "quad", CPL_TYPE_INT, NULL, "%2d",
    "Wavelength range bin number", CPL_TRUE},
  { "npts", CPL_TYPE_INT, NULL, "%5d",
    "Number of points used to compute the correction factor", CPL_TRUE},
  { "corr", CPL_TYPE_DOUBLE, NULL, "%6.4f",
    "Correction factor for the slice in this wavelength range", CPL_TRUE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};


/*----------------------------------------------------------------------------*/
/**
      This type of file contains one or more binary tables with the
      relative fluxes on the sky emission lines. If both tables are
      present, they are merged, so that lines should not appear in
      both tables.
    
      Columns:
    
      - '<tt>name</tt>': Line name [string]
      - '<tt>group</tt>': Line group id [int]
      - '<tt>lambda</tt>': Air wavelength [double]
      - '<tt>flux</tt>': Line flux [double]
      - '<tt>dq</tt>': Quality of the entry (>0: dont use) [int]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_sky_lines_lines_def[] = {
  { "name", CPL_TYPE_STRING, NULL, "%s",
    "Line name", CPL_TRUE},
  { "group", CPL_TYPE_INT, NULL, "%2d",
    "Line group id", CPL_TRUE},
  { "lambda", CPL_TYPE_DOUBLE, "Angstrom", "%7.2f",
    "Air wavelength", CPL_TRUE},
  { "flux", CPL_TYPE_DOUBLE, "10**(-20)*erg/(s cm^2 arcsec^2)", "%1.5e",
    "Line flux", CPL_TRUE},
  { "dq", CPL_TYPE_INT, NULL, "%#x",
    "Quality of the entry (>0: dont use)", CPL_TRUE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};


/*----------------------------------------------------------------------------*/
/**
      This type of file contains one or more binary tables with the
      relative fluxes on the sky emission lines. If both tables are
      present, they are merged, so that lines should not appear in
      both tables.
    
      Columns:
    
      - '<tt>name</tt>': Transition name; like "OH 8-3 P1e(22.5) 2" [string]
      - '<tt>lambda</tt>': Air wavelength [double]
      - '<tt>v_u</tt>': Upper transition level [int]
      - '<tt>v_l</tt>': Lower transition level [int]
      - '<tt>nu</tt>': Vibrational momentum [int]
      - '<tt>E_u</tt>': Upper energy [double]
      - '<tt>J_u</tt>': Upper momentum [double]
      - '<tt>A</tt>': Transition probability [double]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_sky_lines_oh_transitions_def[] = {
  { "name", CPL_TYPE_STRING, NULL, "%s",
    "Transition name; like \"OH 8-3 P1e(22.5) 2\"", CPL_TRUE},
  { "lambda", CPL_TYPE_DOUBLE, "Angstrom", "%7.2f",
    "Air wavelength", CPL_TRUE},
  { "v_u", CPL_TYPE_INT, NULL, "%7d",
    "Upper transition level", CPL_TRUE},
  { "v_l", CPL_TYPE_INT, NULL, "%7d",
    "Lower transition level", CPL_TRUE},
  { "nu", CPL_TYPE_INT, NULL, "%7d",
    "Vibrational momentum", CPL_TRUE},
  { "E_u", CPL_TYPE_DOUBLE, "J", "%1.5e",
    "Upper energy", CPL_TRUE},
  { "J_u", CPL_TYPE_DOUBLE, NULL, "%1.5e",
    "Upper momentum", CPL_TRUE},
  { "A", CPL_TYPE_DOUBLE, NULL, "%1.5e",
    "Transition probability", CPL_TRUE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};


/*----------------------------------------------------------------------------*/
/**
      This type of file contains a binary table with the relative
      fluxes on the raman emission lines.
    
      Columns:
    
      - '<tt>name</tt>': Line name [string]
      - '<tt>group</tt>': Line group id [int]
      - '<tt>lambda</tt>': Air wavelength [double]
      - '<tt>flux</tt>': Line flux [double]
      - '<tt>dq</tt>': Quality of the entry (>0: dont use) [int]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_raman_lines_lines_def[] = {
  { "name", CPL_TYPE_STRING, NULL, "%s",
    "Line name", CPL_TRUE},
  { "group", CPL_TYPE_INT, NULL, "%2d",
    "Line group id", CPL_TRUE},
  { "lambda", CPL_TYPE_DOUBLE, "Angstrom", "%7.2f",
    "Air wavelength", CPL_TRUE},
  { "flux", CPL_TYPE_DOUBLE, "10**(-20)*erg/(s cm^2 arcsec^2)", "%1.5e",
    "Line flux", CPL_TRUE},
  { "dq", CPL_TYPE_INT, NULL, "%#x",
    "Quality of the entry (>0: dont use)", CPL_TRUE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};


#ifdef XML_TABLEDEF_FLUX_TABLE /* currently not used */

/*----------------------------------------------------------------------------*/
/**
      This is a simple binary FITS table with the dependency of the flux on
      wavelength.
    
      Columns:
    
      - '<tt>lambda</tt>': Wavelength [double]
      - '<tt>flux</tt>': Flux [double]
      - '<tt>fluxerr</tt>': Error of the flux [double]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_flux_table_def[] = {
  { "lambda", CPL_TYPE_DOUBLE, "Angstrom", "%7.2f",
    "Wavelength", CPL_TRUE},
  { "flux", CPL_TYPE_DOUBLE, "erg/(s cm^2 arcsec^2)", "%1.5e",
    "Flux", CPL_TRUE},
  { "fluxerr", CPL_TYPE_DOUBLE, "erg/(s cm^2 arcsec^2)", "%1.5e",
    "Error of the flux", CPL_FALSE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};

#endif /* XML_TABLEDEF_FLUX_TABLE */

#ifdef XML_TABLEDEF_STD_FLUX_TABLE /* currently not used */

/*----------------------------------------------------------------------------*/
/**
      This is a binary FITS table with the dependency of the flux on
      wavelength, and an optional column containing the error of the flux.

      The wavelengths should cover at least the MUSE wavelength range.

      The pipeline expects several such tables in multiple binary table
      extensions of a single FITS file. It then loads the one nearest to the
      observed sky position, using the RA and DEC keywords present in each FITS
      extension.
    
      Columns:
    
      - '<tt>lambda</tt>': Wavelength [double]
      - '<tt>flux</tt>': The standard star flux [double]
      - '<tt>fluxerr</tt>': Error of the standard star flux, optional [double]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_std_flux_table_def[] = {
  { "lambda", CPL_TYPE_DOUBLE, "Angstrom", "%7.2f",
    "Wavelength", CPL_TRUE},
  { "flux", CPL_TYPE_DOUBLE, "erg/s/cm**2/Angstrom", "%1.5e",
    "The standard star flux", CPL_TRUE},
  { "fluxerr", CPL_TYPE_DOUBLE, "erg/s/cm**2/Angstrom", "%1.5e",
    "Error of the standard star flux, optional", CPL_FALSE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};

#endif /* XML_TABLEDEF_STD_FLUX_TABLE */

#ifdef XML_TABLEDEF_STD_RESPONSE /* currently not used */

/*----------------------------------------------------------------------------*/
/**
      MUSE flux response table.

      In addition to the three main columns, this table may contain additional
      entries related to the throughput computed from the response curve
      ("throughput") and estimates of the response and its error that were not
      smoothed ("response_unsmoothed" and "resperr_unsmoothed").
    
      Columns:
    
      - '<tt>lambda</tt>': wavelength [double]
      - '<tt>response</tt>': instrument response derived from standard star [double]
      - '<tt>resperr</tt>': instrument response error derived from standard star [double]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_std_response_def[] = {
  { "lambda", CPL_TYPE_DOUBLE, "Angstrom", "%7.2f",
    "wavelength", CPL_TRUE},
  { "response", CPL_TYPE_DOUBLE, "2.5*log10((count/s/Angstrom)/(erg/s/cm**2/Angstrom))", "%.4e",
    "instrument response derived from standard star", CPL_TRUE},
  { "resperr", CPL_TYPE_DOUBLE, "2.5*log10((count/s/Angstrom)/(erg/s/cm**2/Angstrom))", "%.4e",
    "instrument response error derived from standard star", CPL_TRUE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};

#endif /* XML_TABLEDEF_STD_RESPONSE */

#ifdef XML_TABLEDEF_STD_TELLURIC /* currently not used */

/*----------------------------------------------------------------------------*/
/**
      MUSE telluric correction table.
    
      Columns:
    
      - '<tt>lambda</tt>': wavelength [double]
      - '<tt>ftelluric</tt>': the telluric correction factor, normalized to an airmass of 1 [double]
      - '<tt>ftellerr</tt>': the error of the telluric correction factor [double]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_std_telluric_def[] = {
  { "lambda", CPL_TYPE_DOUBLE, "Angstrom", "%7.2f",
    "wavelength", CPL_TRUE},
  { "ftelluric", CPL_TYPE_DOUBLE, NULL, "%.5f",
    "the telluric correction factor, normalized to an airmass of 1", CPL_TRUE},
  { "ftellerr", CPL_TYPE_DOUBLE, NULL, "%.5f",
    "the error of the telluric correction factor", CPL_TRUE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};

#endif /* XML_TABLEDEF_STD_TELLURIC */

#ifdef XML_TABLEDEF_EXTINCT_TABLE /* currently not used */

/*----------------------------------------------------------------------------*/
/**
      This is a simple binary FITS table with the dependency of the extinction on
      wavelength.

      The wavelengths should cover at least the MUSE wavelength range. The
      atmospheric extinction values should be applicable for Paranal, ideally
      for the night of observations.
    
      Columns:
    
      - '<tt>lambda</tt>': Wavelength [double]
      - '<tt>extinction</tt>': Extinction [double]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_extinct_table_def[] = {
  { "lambda", CPL_TYPE_DOUBLE, "Angstrom", "%1.5e",
    "Wavelength", CPL_TRUE},
  { "extinction", CPL_TYPE_DOUBLE, "mag/airmass", "%1.5e",
    "Extinction", CPL_TRUE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};

#endif /* XML_TABLEDEF_EXTINCT_TABLE */

#ifdef XML_TABLEDEF_TELLURIC_REGIONS /* currently not used */

/*----------------------------------------------------------------------------*/
/**
      This FITS table defines wavelength ranges of telluric absorption lines.
      It can be used to override the internal telluric bands used in the
      muse_standard recipe.
    
      Columns:
    
      - '<tt>lmin</tt>': Lower limit of the telluric region [double]
      - '<tt>lmax</tt>': Upper limit of the telluric region [double]
      - '<tt>bgmin</tt>': Lower limit of the background region [double]
      - '<tt>bgmax</tt>': Upper limit of the background region [double]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_telluric_regions_def[] = {
  { "lmin", CPL_TYPE_DOUBLE, "Angstrom", "%8.3f",
    "Lower limit of the telluric region", CPL_TRUE},
  { "lmax", CPL_TYPE_DOUBLE, "Angstrom", "%8.3f",
    "Upper limit of the telluric region", CPL_TRUE},
  { "bgmin", CPL_TYPE_DOUBLE, "Angstrom", "%8.3f",
    "Lower limit of the background region", CPL_TRUE},
  { "bgmax", CPL_TYPE_DOUBLE, "Angstrom", "%8.3f",
    "Upper limit of the background region", CPL_TRUE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};

#endif /* XML_TABLEDEF_TELLURIC_REGIONS */

/*----------------------------------------------------------------------------*/
/**
      This is a list of arc lines to be used for wavelength
      calibration. It is a FITS table, with one row for each line,
      which contains central wavelength of the line in question and a
      relative strength of the line, if known. The line fluxes may be
      used in the data reduction software as a first guess to the
      expected flux, the actual fluxes will be determined using line
      fitting. Additionally, to identify the lines and associate them
      with an arc lamp, a column ion (with element and ionization
      status) and a quality flag are needed. Optionally, a comment
      column might be useful.
    
      Columns:
    
      - '<tt>lambda</tt>': Wavelength [float]
      - '<tt>flux</tt>': Relative flux [float]
      - '<tt>ion</tt>': Ion from which the line originates [string]
      - '<tt>quality</tt>': Quality flag (0: undetected line, 1: line used
          for pattern matching, 2: line that is part of a multiplet, 3:
          good line, fully used, 5: bright and isolated line, use as
          FWHM reference [int]
      - '<tt>comment</tt>': Optional comment [string]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_line_catalog_def[] = {
  { MUSE_LINE_CATALOG_LAMBDA, CPL_TYPE_FLOAT, "Angstrom", "%1.5e",
    "Wavelength", CPL_TRUE},
  { MUSE_LINE_CATALOG_FLUX, CPL_TYPE_FLOAT, NULL, "%1.5e",
    "Relative flux", CPL_TRUE},
  { MUSE_LINE_CATALOG_ION, CPL_TYPE_STRING, NULL, "%s",
    "Ion from which the line originates", CPL_TRUE},
  { MUSE_LINE_CATALOG_QUALITY, CPL_TYPE_INT, NULL, "%7d",
    "Quality flag (0: undetected line, 1: line used for pattern matching, 2: line that is part of a multiplet, 3: good line, fully used, 5: bright and isolated line, use as FWHM reference", CPL_TRUE},
  { "comment", CPL_TYPE_STRING, NULL, "%s",
    "Optional comment", CPL_FALSE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};


/*----------------------------------------------------------------------------*/
/**
      This is a FITS table, typically with 24 extensions. It is used in the
      low-level recipes working on raw data. Each extension lists known bad
      pixels of one CCD.
    
      Columns:
    
      - '<tt>xpos</tt>': X position of a bad pixel (on untrimmed raw data) [int]
      - '<tt>ypos</tt>': Y position of a bad pixel (on untrimmed raw data) [int]
      - '<tt>status</tt>': 32bit bad pixel mask as defined by Euro3D [int]
      - '<tt>value</tt>': Extra value, e.g. depth for traps [float]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_badpix_table_def[] = {
  { MUSE_BADPIX_X, CPL_TYPE_INT, "pix", "%4d",
    "X position of a bad pixel (on untrimmed raw data)", CPL_TRUE},
  { MUSE_BADPIX_Y, CPL_TYPE_INT, "pix", "%4d",
    "Y position of a bad pixel (on untrimmed raw data)", CPL_TRUE},
  { MUSE_BADPIX_DQ, CPL_TYPE_INT, NULL, "%#010x",
    "32bit bad pixel mask as defined by Euro3D", CPL_TRUE},
  { MUSE_BADPIX_VALUE, CPL_TYPE_FLOAT, "count", "%7.4f",
    "Extra value, e.g. depth for traps", CPL_TRUE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};


#ifdef XML_TABLEDEF_ASTROMETRY_REFERENCE /* currently not used */

/*----------------------------------------------------------------------------*/
/**
      This FITS file lists astrometric sources in fields to be observed with MUSE
      as astrometric calibrators. It is used by the muse_astrometry recipe.
      One such table exists per field; the tables contains a list of (point)
      sources. Each row contains information about one object in the field.

      The pipeline expects several such tables in multiple binary table
      extensions of a single FITS file. It then loads the one nearest to the
      observed sky position, using the RA and DEC keywords present in each FITS
      extension.
    
      Columns:
    
      - '<tt>SourceID</tt>': Source identification [string]
      - '<tt>RA</tt>': Right ascension [double]
      - '<tt>DEC</tt>': Declination [double]
      - '<tt>filter</tt>': Filter name used for column mag [string]
      - '<tt>mag</tt>': Object (Vega) magnitude [double]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_astrometry_reference_def[] = {
  { "SourceID", CPL_TYPE_STRING, NULL, "%s",
    "Source identification", CPL_TRUE},
  { "RA", CPL_TYPE_DOUBLE, "deg", "%1.5e",
    "Right ascension", CPL_TRUE},
  { "DEC", CPL_TYPE_DOUBLE, "deg", "%1.5e",
    "Declination", CPL_TRUE},
  { "filter", CPL_TYPE_STRING, NULL, "%s",
    "Filter name used for column mag", CPL_TRUE},
  { "mag", CPL_TYPE_DOUBLE, "mag", "%1.5e",
    "Object (Vega) magnitude", CPL_TRUE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};

#endif /* XML_TABLEDEF_ASTROMETRY_REFERENCE */

#ifdef XML_TABLEDEF_FILTER_LIST /* currently not used */

/*----------------------------------------------------------------------------*/
/**
      This FITS table contains all filter functions that can be used
      for image reconstruction. Each filter curve is contained within
      one sub-table.
    
      Columns:
    
      - '<tt>lambda</tt>': Wavelength [double]
      - '<tt>throughput</tt>': Filter throughput (in fractions of 1) [double]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_filter_list_def[] = {
  { "lambda", CPL_TYPE_DOUBLE, "Angstrom", "%1.5e",
    "Wavelength", CPL_TRUE},
  { "throughput", CPL_TYPE_DOUBLE, NULL, "%1.5e",
    "Filter throughput (in fractions of 1)", CPL_TRUE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};

#endif /* XML_TABLEDEF_FILTER_LIST */

/*----------------------------------------------------------------------------*/
/**
      Coordinate offsets suitable for being used with muse_exp_combine to
      properly align a set exposures to a reference position during the creation
      of a combined data cube.

      The offset corrections in the RA_OFFSET and DEC_OFFSET columns are the
      direct difference of the measured position to the reference position,
      without cos(DEC):

      RA_OFFSET = RA(measured) - RA(reference)

      DEC_OFFSET = DEC(measured) - DEC(reference)

      This table optionally also contains a FLUX_SCALE column that is then used
      to correct relative scaling of exposures in a sequence, e.g. to correct
      observations taken in non-photometric conditions.
      When created by muse_exp_align, the table does contain the FLUX_SCALE
      column. It is then filled with invalid values (NANs), so that the pipeline
      knows to ignore these values. The user can fill these values by hand with
      any FITS editor.
    
      Columns:
    
      - '<tt>DATE_OBS</tt>': Date and time at the start of the exposure [string]
      - '<tt>MJD_OBS</tt>': MJD at the start of the exposure [double]
      - '<tt>RA_OFFSET</tt>': Right ascension offset in degrees [double]
      - '<tt>DEC_OFFSET</tt>': Declination offset in degrees [double]
      - '<tt>FLUX_SCALE</tt>': (Relative) flux scaling of the given exposure [double]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_offset_list_def[] = {
  { MUSE_OFFSETS_DATEOBS, CPL_TYPE_STRING, NULL, "%s",
    "Date and time at the start of the exposure", CPL_TRUE},
  { MUSE_OFFSETS_MJDOBS, CPL_TYPE_DOUBLE, "s", "%.8f",
    "MJD at the start of the exposure", CPL_FALSE},
  { MUSE_OFFSETS_DRA, CPL_TYPE_DOUBLE, "deg", "%.6e",
    "Right ascension offset in degrees", CPL_TRUE},
  { MUSE_OFFSETS_DDEC, CPL_TYPE_DOUBLE, "deg", "%.6e",
    "Declination offset in degrees", CPL_TRUE},
  { MUSE_OFFSETS_FSCALE, CPL_TYPE_DOUBLE, NULL, "%.5f",
    "(Relative) flux scaling of the given exposure", CPL_FALSE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};


/*----------------------------------------------------------------------------*/
/**
      List of source positions detected on a MUSE field-of-view image.
    
      Columns:
    
      - '<tt>Id</tt>': Source identifier [int]
      - '<tt>X</tt>': X-position of the source in image coordinates [double]
      - '<tt>Y</tt>': Y-position of the source in image coordinates [double]
      - '<tt>Flux</tt>': Source flux [double]
      - '<tt>Sharpness</tt>': Source sharpness value [double]
      - '<tt>Roundness</tt>': Source roundness value [double]
      - '<tt>RA</tt>': Source right ascension in degrees [double]
      - '<tt>DEC</tt>': Source declination in degrees [double]
      - '<tt>RA_CORR</tt>': Source right ascension in degrees corrected for field offset [double]
      - '<tt>DEC_CORR</tt>': Source declination in degrees corrected for field offset [double]
      @hideinitializer 
  */
/*----------------------------------------------------------------------------*/
const muse_cpltable_def muse_source_list_def[] = {
  { MUSE_SRCLIST_ID, CPL_TYPE_INT, NULL, "%d",
    "Source identifier", CPL_TRUE},
  { MUSE_SRCLIST_X, CPL_TYPE_DOUBLE, "pix", "%.6e",
    "X-position of the source in image coordinates", CPL_TRUE},
  { MUSE_SRCLIST_Y, CPL_TYPE_DOUBLE, "pix", "%.6e",
    "Y-position of the source in image coordinates", CPL_TRUE},
  { MUSE_SRCLIST_FLUX, CPL_TYPE_DOUBLE, NULL, "%.6e",
    "Source flux", CPL_FALSE},
  { MUSE_SRCLIST_SHARPNESS, CPL_TYPE_DOUBLE, NULL, "%.8f",
    "Source sharpness value", CPL_FALSE},
  { MUSE_SRCLIST_ROUNDNESS, CPL_TYPE_DOUBLE, NULL, "%.8f",
    "Source roundness value", CPL_FALSE},
  { MUSE_SRCLIST_RA, CPL_TYPE_DOUBLE, "deg", "%.6e",
    "Source right ascension in degrees", CPL_TRUE},
  { MUSE_SRCLIST_DEC, CPL_TYPE_DOUBLE, "deg", "%.6e",
    "Source declination in degrees", CPL_TRUE},
  { MUSE_SRCLIST_RACORR, CPL_TYPE_DOUBLE, "deg", "%.6e",
    "Source right ascension in degrees corrected for field offset", CPL_FALSE},
  { MUSE_SRCLIST_DECCORR, CPL_TYPE_DOUBLE, "deg", "%.6e",
    "Source declination in degrees corrected for field offset", CPL_FALSE},
  { NULL, 0, NULL, NULL, NULL, CPL_FALSE }
};


/**@}*/
      
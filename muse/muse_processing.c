/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/*
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2016 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*----------------------------------------------------------------------------*
 *                              Includes                                      *
 *----------------------------------------------------------------------------*/
#include <stdio.h>
#include <float.h>
#include <math.h>
#include <string.h>
#include <cpl.h>

#include "muse_processing.h"
#include "muse_instrument.h"

#include "muse_datacube.h"
#include "muse_dfs.h"
#include "muse_idp.h"
#include "muse_pfits.h"
#include "muse_lsf.h"
#include "muse_utils.h"

/*----------------------------------------------------------------------------*
 *                             Debugging Macros                               *
 *         Set these to 1 or higher for (lots of) debugging output            *
 *----------------------------------------------------------------------------*/
#define DEBUG_EXPOSURES_SORT 0 /* debug muse_processing_sort_exposures() */

/*----------------------------------------------------------------------------*/
/**
 * @defgroup muse_processing   Processing framework
 *
 * The following functions are used to make the data processing in MUSE
 * easier and uniform.
 */
/*----------------------------------------------------------------------------*/

/**@{*/

/*----------------------------------------------------------------------------*
 *                              Functions                                     *
 *----------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/**
  @private
  @brief  Get the raw input tag from the input frames of a recipe.
  @param  aRecipe   Recipe
  @return the array of unique raw input tag(s), or an empty array on error.

  @remark The information about the input tag is retrieved from the
          recipeconfig.
 */
/*---------------------------------------------------------------------------*/
static cpl_array *
muse_processing_get_rawtags(cpl_recipe *aRecipe)
{
  /* start with zero length array, that is returned on error */
  cpl_array *rawtags = cpl_array_new(0, CPL_TYPE_STRING);
  /* we need a recipe config for this to work */
  cpl_recipeconfig *recipeconfig = muse_processing_get_recipeconfig(aRecipe);
  if (!recipeconfig) {
    cpl_msg_error(__func__, "No recipeconfig found!");
    return rawtags;
  }

  /* loop through all frames of the recipe (which at this point will be   *
   * just the input frames) and check if they are inputs, i.e. raw frames */
  cpl_size iframe, nframes = cpl_frameset_get_size(aRecipe->frames);
  for (iframe = 0; iframe < nframes; iframe++) {
    cpl_frame *frame = cpl_frameset_get_position(aRecipe->frames, iframe);
    const char *tag = cpl_frame_get_tag(frame); /* fiducial raw tag to check */
    /* see if it's already in our list */
    cpl_size itag, ntags = cpl_array_get_size(rawtags);
    for (itag = 0; itag < ntags; itag++) {
      if (!strcmp(cpl_array_get_string(rawtags, itag), tag)) {
        tag = NULL; /* set to NULL to make cpl_recipeconfig_get_inputs() fail */
        break;
      } /* if match */
    } /* for itag */
    cpl_errorstate state = cpl_errorstate_get();
    char **tags = cpl_recipeconfig_get_inputs(recipeconfig, tag);
    if (!tags) { /* not a raw tag! */
      cpl_errorstate_set(state); /* CPL will have set an error, swallow it */
      continue;
    } /* if */
    /* we have a raw tag! record this tag in the output array of raw tags */
    cpl_array_set_size(rawtags, ntags + 1);
    cpl_array_set_string(rawtags, ntags, tag);
    /* clean up the elements which we didn't actually need */
    int i;
    for (i = 0; tags[i]; i++) {
      cpl_free(tags[i]);
    }
    cpl_free(tags);
  } /* for iframe (all recipe frames) */
  if (!cpl_array_get_size(rawtags)) {
    cpl_msg_error(__func__, "No valid raw tag(s) found!");
  }
  return rawtags;
} /* muse_processing_get_rawtags() */

/*---------------------------------------------------------------------------*/
/**
  @brief Create a new processing structure.
  @param aName     name of the recipe
  @param aRecipe   the CPL recipe
  @return the processing structure
 */
/*---------------------------------------------------------------------------*/
muse_processing *
muse_processing_new(const char *aName, cpl_recipe *aRecipe)
{
  muse_processing *processing = cpl_malloc(sizeof(muse_processing));
  processing->name = aName;
  processing->recipe = aRecipe;
  processing->inframes = cpl_frameset_duplicate(aRecipe->frames);
  processing->usedframes = cpl_frameset_new();
  processing->outframes = cpl_frameset_new();
  #pragma omp critical(cpl_parameters)
  processing->parameters = muse_cplparameterlist_duplicate(aRecipe->parameters);
  processing->intags = muse_processing_get_rawtags(aRecipe);
  processing->counter = cpl_malloc(sizeof(muse_processing_framecounter));
  processing->counter[0].tag = NULL;
  return processing;
} /* muse_processing_new() */

/*---------------------------------------------------------------------------*/
/**
  @brief If  RADECSYS is found, translate to RADESYS. Remove HDRVER.
  @param hdr is the propertlist containing the header to be adjusted
 */
/*---------------------------------------------------------------------------*/
void 
muse_processing_radecsys_fix(cpl_propertylist *hdr)
{
  if(hdr){
    if(cpl_propertylist_has(hdr,"HDRVER")){
        cpl_propertylist_erase(hdr,"HDRVER");
    }
    if(cpl_propertylist_has(hdr,"RADECSYS")){
      char* val = NULL;
      char* com = NULL;
      val = cpl_strdup(cpl_propertylist_get_string(hdr, "RADECSYS"));
      com = cpl_strdup(cpl_propertylist_get_comment(hdr, "RADECSYS"));
      cpl_propertylist_update_string(hdr, "RADESYS", val);
      cpl_propertylist_set_comment(hdr, "RADESYS", com);
      cpl_propertylist_erase(hdr,"RADECSYS");
      
      cpl_free(val);
      cpl_free(com);
      cpl_error_reset();
    }
  }
}


/*---------------------------------------------------------------------------*/
/**
  @brief Free the muse_processing structure.
  @param aProcessing the structure to be freed
 */
/*---------------------------------------------------------------------------*/
void
muse_processing_delete(muse_processing *aProcessing)
{
  if (!aProcessing) {
    return;
  }
  cpl_array_delete(aProcessing->intags);
  cpl_frameset_delete(aProcessing->inframes);
  cpl_frameset_delete(aProcessing->usedframes);
  cpl_frameset_delete(aProcessing->outframes);
  cpl_parameterlist_delete(aProcessing->parameters);
  int i;
  for (i = 0; aProcessing->counter[i].tag != NULL; i++) {
    cpl_free((char *)aProcessing->counter[i].tag);
  }
  cpl_free(aProcessing->counter);
  cpl_free(aProcessing);
} /* muse_processing_delete() */

/*---------------------------------------------------------------------------*/
/**
  @brief Add a frame to the set of used frames.
  @param aProcessing   the processing structure
  @param aFrame        the new used frame
  @param aGroup        the frame group to assign to the frame
  @param aDuplicate    if not zero, duplicate the frame before appending it
                       into the used frameset

  Insert aFrame into the usedframes frameset of aProcessing if it is not already
  present in that frameset (present being checked using the filename and tag
  information of existing frames against aFrame). If aDuplicate is zero, the
  frame is destroyed here, assuming that it was duplicated from some other frame
  before being passed into this function. <b>In this case, any property (e.g.
  filename) queried from the frame can no longer be used afterwards!</b>
 */
/*---------------------------------------------------------------------------*/
void
muse_processing_append_used(muse_processing *aProcessing,
                            cpl_frame *aFrame, cpl_frame_group aGroup,
                            int aDuplicate)
{
  if (!aProcessing) {
    cpl_msg_error(__func__, "NULL processing struct!");
    return;
  }
#if 0
  if (aGroup == CPL_FRAME_GROUP_CALIB) {
    cpl_msg_debug(__func__, "using %s %s frame %s", "calibration",
                  cpl_frame_get_tag(aFrame), cpl_frame_get_filename(aFrame));
  } else if (aGroup == CPL_FRAME_GROUP_RAW) {
    cpl_msg_debug(__func__, "using %s %s frame %s", "raw",
                  cpl_frame_get_tag(aFrame), cpl_frame_get_filename(aFrame));
  }
#endif
  /* check, if this frame is already listed   *
   * (this has to be locked by other threads) */
  cpl_boolean doreturn = CPL_FALSE;
#pragma omp critical(muse_processing_used_frames)
  {
    const char *fn = cpl_frame_get_filename(aFrame),
        *tag = cpl_frame_get_tag(aFrame);
    cpl_size iframe, nframes = cpl_frameset_get_size(aProcessing->usedframes);
    for (iframe = 0;
        (iframe < nframes) && fn && tag; /* only check with valid info */
        iframe++) {
      cpl_frame *frame = cpl_frameset_get_position(aProcessing->usedframes, iframe);
      const char *fn2 = cpl_frame_get_filename(frame),
          *tag2 = cpl_frame_get_tag(frame);
      if (fn2 && !strncmp(fn, fn2, strlen(fn) + 1) &&
          tag2 && !strncmp(tag, tag2, strlen(tag) + 1)) {
        if (!aDuplicate) { /* destroy frame, assuming it was duplicated elsewhere */
          cpl_frame_delete(aFrame);
        }
        doreturn = CPL_TRUE; /* already listed, mark for early return */
        break;
      }
    } /* for iframe (all usedframes) */
  } /* end of pragma omp */

  if (doreturn) {
    return; /* frame is already in the usedframes set, so return */
  }
  /* need to store this frame, so set a group */
  cpl_frame_set_group(aFrame, aGroup);

  /* actual inserting the frame has to be guarded against other threads */
#pragma omp critical(muse_processing_used_frames)
  {
    if (aDuplicate) {
      cpl_frameset_insert(aProcessing->usedframes, cpl_frame_duplicate(aFrame));
    } else {
      cpl_frameset_insert(aProcessing->usedframes, aFrame);
    }
  } /* end of pragma omp */
} /* muse_processing_append_used() */

/*---------------------------------------------------------------------------*/
/**
  @private
  @brief Get the next frame counter.
  @param aProcessing   the processing structure
  @param aTag          the frame tag
  @param aIFU          the IFU/channel number
  @return next frame counter

  Get the next frame counter. This number will start at 1 for any
  pair of tag/ifu and will increase with each call of the function
  for that pair.
 */
/*---------------------------------------------------------------------------*/
static int
muse_processing_get_framecounter(muse_processing *aProcessing,
                                 const char *aTag, int aIFU)
{
  if (!aProcessing) {
    cpl_msg_error(__func__, "NULL processing struct!");
    return 0;
  }
  int i;
  for (i = 0; aProcessing->counter[i].tag != NULL; i++) {
    if (strcmp(aProcessing->counter[i].tag, aTag) == 0 &&
        aProcessing->counter[i].ifu == aIFU) {
      return ++aProcessing->counter[i].counter;
    }
  }
  aProcessing->counter = cpl_realloc(aProcessing->counter,
                                     (i+2)*sizeof(muse_processing_framecounter));
  aProcessing->counter[i].tag = cpl_strdup(aTag);
  aProcessing->counter[i].ifu = aIFU;
  aProcessing->counter[i].counter = 1;
  aProcessing->counter[i+1].tag = NULL;
  return 1;
}

/*---------------------------------------------------------------------------*/
/**
  @private
  @brief Create a new FITS header for saving a frame.
  @param aProcessing   the processing structure
  @param aHeader       the header to be modified
  @param aFrame        the frame to be saved
  @param aIndex        input frame index (>= 0) or a negative number for master
                       or date-selected files
  @param aDateObs      the string of observed date (FITS keyword DATE-OBS), only
                       used if aIndex is negative
  @param aSequence     if target frame is part of a sequence
 */
/*---------------------------------------------------------------------------*/
static void
muse_processing_setup_header(muse_processing *aProcessing,
                             cpl_propertylist *aHeader, cpl_frame *aFrame,
                             int aIndex, const char *aDateObs,
                             cpl_boolean aSequence)
{
  if (!aProcessing) {
    cpl_msg_error(__func__, "NULL processing struct!");
    return;
  }
  if (!aProcessing->inframes ||
      cpl_frameset_is_empty(aProcessing->inframes)) {
    cpl_msg_warning(__func__, "No raw input files, no DFS product header added");
    return;
  }
  /* save some headers which are overwritten by cpl_dfs, *
   * including OBJECT and ESO.DRS.MUSE                   */
  cpl_propertylist *hkeep = cpl_propertylist_new();
  cpl_propertylist_copy_property_regexp(hkeep, aHeader,
                                        MUSE_HEADERS_KEEP_REGEXP, 0);
  /* don't want any DFS-generated ESO.PRO keywords before calling cpl_dfs *
   * to create them, so delete all ESO.PRO stuff and COMMENTS             */
  cpl_propertylist_erase_regexp(aHeader, "^ESO PRO|^COMMENT", 0);
  /* remove temporary stuff that is not meant to be saved to disk */
  cpl_propertylist_erase_regexp(aHeader, MUSE_HDR_TMP_REGEXP, 0);

  /* Apply RADECSYS fix */
  muse_processing_radecsys_fix(aHeader);

  /* make sure that the raw frame(s) are first in the list of used frames */
  cpl_frameset *fset = muse_frameset_sort_raw_other(aProcessing->usedframes,
                                                    aIndex, aDateObs, aSequence);

#if CPL_VERSION_CODE < CPL_VERSION(7, 0, 0)
  /* workaround for the EQUINOX warning, replace with double-type keyword */
  if (cpl_propertylist_has(aHeader, "EQUINOX") &&
      cpl_propertylist_get_type(aHeader, "EQUINOX") < CPL_TYPE_FLOAT) {
    cpl_property *equinox =  cpl_propertylist_get_property(aHeader, "EQUINOX");
    double equvalue = cpl_property_get_long_long(equinox);
    const char *equcomment = cpl_property_get_comment(equinox);
    cpl_property_set_name(equinox, "EQUIBRK");
    cpl_propertylist_insert_after_double(aHeader, "EQUIBRK", "EQUINOX",
                                         equvalue);
    cpl_propertylist_set_comment(aHeader, "EQUINOX", equcomment);
    cpl_propertylist_erase(aHeader, "EQUIBRK");
  } /* if non-float EQUINOX */
#endif

  char *pkgstring = cpl_sprintf("%s/%s", PACKAGE, PACKAGE_VERSION);

#pragma omp critical(muse_processing_dfs_setup_header)
  if (cpl_dfs_setup_product_header(aHeader, aFrame, fset,
                                   aProcessing->parameters, aProcessing->name,
                                   pkgstring, MUSE_PRO_DID, NULL)
      != CPL_ERROR_NONE) {
    cpl_msg_error(__func__, "Could not add DFS product header: %s",
                  cpl_error_get_message());
  }
  cpl_free(pkgstring);
  cpl_frameset_delete(fset);

  /* reset overwritten headers (if they were there before) */
  int i, n = cpl_propertylist_get_size(hkeep);
  for (i = 0; i < n; i++) {
    const cpl_property *p = cpl_propertylist_get_const(hkeep, i);
    cpl_propertylist_erase(aHeader, cpl_property_get_name(p));
    cpl_propertylist_append_property(aHeader, p);
  } /* for i (all properties in hkeep) */
  cpl_propertylist_delete(hkeep);

  /* MUSE data is always taken with an IFU, so override PRO.TECH */
  cpl_propertylist_update_string(aHeader, "ESO PRO TECH", "IFU");
  /* add PRO.SCIENCE header for science recipes */
  if (strstr(aProcessing->name, "muse_sci") ||
      !strcmp(aProcessing->name, "muse_exp_combine")) {
    cpl_propertylist_update_bool(aHeader, "ESO PRO SCIENCE", 1);
  }
} /* muse_processing_setup_header() */

/*---------------------------------------------------------------------------*/
/**
  @brief Create a new frame for a result file.
  @param aProcessing the processing structure
  @param aIFU        the IFU/channel number
                     (use negative value to suppress -nn postfix)
  @param aHeader     the custom properties to be included in the header
  @param aTag        tag to apply for output frames, used as default prefix
  @param aType       the type of data to be stored
  @return the new frame

  Create a new frame for a result file. Set the correct type, group, level, tag.
  The filename consists of the prefix, possibly an underscore and a running
  number (for non-unique files), a minus and the IFU number, and the suffix
  ".fits".

  As prefix, the frame tag is used on default. One may create a different
  prefix by providing the "MUSE PRIVATE FILE PREFIX" property in the header.

  All "MUSE PRIVATE " properties are removed from the header.

  @error{set CPL_ERROR_NULL_INPUT\, return NULL, aProcessing is NULL}
 */
/*---------------------------------------------------------------------------*/
cpl_frame *
muse_processing_new_frame(muse_processing *aProcessing, int aIFU,
                          cpl_propertylist *aHeader, const char *aTag,
                          cpl_frame_type aType)
{
  cpl_ensure(aProcessing, CPL_ERROR_NULL_INPUT, NULL);
  /* Apply RADECSYS fix */
  muse_processing_radecsys_fix(aHeader);

  muse_processing_prepare_header(aProcessing->recipe, aTag, aHeader);
  const char *prefix = cpl_propertylist_has(aHeader, "MUSE PRIVATE FILE PREFIX")
                     ? cpl_propertylist_get_string(aHeader,
                                                   "MUSE PRIVATE FILE PREFIX")
                     : aTag;

  cpl_frame *frame = cpl_frame_new();
  /* some error from before might disturbe further error handling */
  cpl_errorstate prestate = cpl_errorstate_get();

  /* see if we need to take all input frames, a specific frame based on its  *
   * number in the frameset, and/or a specific frame based on observing date */
  int fmode = muse_processing_get_frame_mode(aProcessing->recipe, aTag),
      framecounter = 0;
  if (fmode != MUSE_FRAME_MODE_MASTER) {
    framecounter = muse_processing_get_framecounter(aProcessing, prefix, aIFU);
  }
  const char *dateobs = NULL;
  if (fmode == MUSE_FRAME_MODE_DATEOBS) {
    dateobs = muse_pfits_get_dateobs(aHeader);
  }
  cpl_boolean sequence = fmode == MUSE_FRAME_MODE_SEQUENCE;

  /* Create product frame */
  char filename[FILENAME_MAX];
  if (aIFU >= 0) {
    if (framecounter == 0) {
      snprintf(filename, FILENAME_MAX, "%s-%02d.fits", prefix, aIFU);
    } else {
      snprintf(filename, FILENAME_MAX, "%s_%04d-%02d.fits", prefix,
               framecounter, aIFU);
    }
  } else {
    if (framecounter == 0) {
      snprintf(filename, FILENAME_MAX, "%s.fits", prefix);
    } else {
      snprintf(filename, FILENAME_MAX, "%s_%04d.fits", prefix, framecounter);
    }
  }
  cpl_frame_set_filename(frame, filename);
  cpl_frame_set_tag(frame, aTag);
  cpl_frame_set_type(frame, aType);
  cpl_frame_set_group(frame, CPL_FRAME_GROUP_PRODUCT);
  cpl_frame_set_level(frame,
                      muse_processing_get_frame_level(aProcessing->recipe,
                                                      aTag));
  if (!cpl_errorstate_is_equal(prestate)) {
    cpl_msg_error(__func__, "Error while initialising the product frame: %s",
                  cpl_error_get_message());
    cpl_frame_delete(frame);
    return NULL;
  }
  cpl_propertylist_erase_regexp(aHeader, "MUSE PRIVATE.*", 0);
  /* select specific frame index to use */
  int idx = dateobs || fmode == MUSE_FRAME_MODE_SUBSET ? -1 : framecounter - 1;
  muse_processing_setup_header(aProcessing, aHeader, frame, idx, dateobs,
                               sequence);
  return frame;
} /* muse_processing_new_frame() */

/*---------------------------------------------------------------------------*/
/**
  @brief Save a computed MUSE image to disk.
  @param aProcessing the processing structure
  @param aIFU        the IFU/channel number
  @param aImage      the image to be saved
  @param aTag        tag to apply for output frames, used as prefix
  @return the CPL error code

  Save an image to disk. The filename consists on the prefix, a "-"
  sign, the IFU number of the CCD and the suffix ".fits". If the
  image saving was successful, add the output frame to the list of
  output frames in the processing structure.

  @error{CPL_ERROR_NULL_INPUT, an argument was NULL}
 */
/*---------------------------------------------------------------------------*/
int
muse_processing_save_image(muse_processing *aProcessing, int aIFU,
                           muse_image *aImage, const char *aTag)
{
  cpl_ensure_code(aProcessing && aImage && aTag, CPL_ERROR_NULL_INPUT);

  cpl_frame *frame = muse_processing_new_frame(aProcessing, aIFU,
                                               aImage->header, aTag,
                                               CPL_FRAME_TYPE_IMAGE);
  cpl_msg_info(__func__, "Saving image as \"%s\"",
               cpl_frame_get_filename(frame));
  int r = muse_image_save(aImage, cpl_frame_get_filename(frame));
  if (r == CPL_ERROR_NONE) {
#pragma omp critical(muse_processing_output_frames)
    cpl_frameset_insert(aProcessing->outframes, frame);
  } else {
    cpl_frame_delete(frame);
  }
  return r;
} /* muse_processing_save_image() */

/*---------------------------------------------------------------------------*/
/**
  @brief  Save a MUSE datacube to disk.
  @param  aProcessing   the processing structure
  @param  aIFU          the IFU/channel number
  @param  aCube         the cube to be saved
  @param  aTag          tag to apply for output frames, used as default prefix
  @param  aType         type of cube to save (MUSE_CUBE_TYPE_FITS,
                        MUSE_CUBE_TYPE_EURO3D, MUSE_CUBE_TYPE_SDP, or
                        MUSE_CUBE_TYPE_LSF)
  @return CPL_ERROR_NONE or the relevant cpl_error_code on error

  Save a datacube to disk. The filename consists on the prefix, a "-"
  sign, the IFU number of the CCD and the suffix ".fits". If saving went without
  error, add the output frame to the list of output frames in the processing
  structure.

  @error{CPL_ERROR_NULL_INPUT, an argument was NULL}
  @error{CPL_ERROR_ILLEGAL_INPUT, an unknown aType was given}
 */
/*---------------------------------------------------------------------------*/
cpl_error_code
muse_processing_save_cube(muse_processing *aProcessing, int aIFU,
                          void *aCube, const char *aTag, muse_cube_type aType)
{
  cpl_ensure_code(aProcessing && aCube && aTag, CPL_ERROR_NULL_INPUT);
  cpl_ensure_code((aType == MUSE_CUBE_TYPE_EURO3D) ||
                  (aType == MUSE_CUBE_TYPE_FITS) ||
                  (aType == MUSE_CUBE_TYPE_SDP) ||
                  (aType == MUSE_CUBE_TYPE_LSF), CPL_ERROR_ILLEGAL_INPUT);

  /* both cube structures contain the header component as  *
   * first element, so it's safe to cast it to one of them */
  cpl_frame *frame = muse_processing_new_frame(aProcessing, aIFU,
                                               ((muse_datacube *)aCube)->header,
                                               aTag, CPL_FRAME_TYPE_IMAGE);

  /* For science data cubes collect IDP properties from the raw data frames *
   * and the data cube header, and update the data cube product header so   *
   * that they are saved in the next step.                                  */
  if (aType == MUSE_CUBE_TYPE_SDP) {
    cpl_errorstate status = cpl_errorstate_get();
    muse_idp_properties *properties =
      muse_idp_properties_collect(aProcessing, (muse_datacube *)aCube, aTag);
    if (properties) {
      muse_idp_properties_update(((muse_datacube *)aCube)->header, properties);
    }
    muse_idp_properties_delete(properties);
    if (!cpl_errorstate_is_equal(status)) {
      cpl_frame_delete(frame);
      return cpl_error_get_code();
    }
  }

  cpl_msg_info(__func__, "Saving %s cube as \"%s\"",
               aType == MUSE_CUBE_TYPE_EURO3D ? "Euro3D" : "FITS",
               cpl_frame_get_filename(frame));
  cpl_error_code rc = CPL_ERROR_NONE;
  if (aType == MUSE_CUBE_TYPE_EURO3D) {
    rc = muse_euro3dcube_save((muse_euro3dcube *)aCube,
                              cpl_frame_get_filename(frame));
  } else if ((aType == MUSE_CUBE_TYPE_FITS) || (aType == MUSE_CUBE_TYPE_SDP)) {
    rc = muse_datacube_save((muse_datacube *)aCube,
                            cpl_frame_get_filename(frame));
  } else {
    rc = muse_lsf_cube_save((muse_lsf_cube *)aCube,
                            cpl_frame_get_filename(frame));
  }
  if (rc == CPL_ERROR_NONE) {
#pragma omp critical(muse_processing_output_frames)
    cpl_frameset_insert(aProcessing->outframes, frame);
  } else {
    cpl_frame_delete(frame);
  }
  return rc;
} /* muse_processing_save_cube() */

/*---------------------------------------------------------------------------*/
/**
  @brief  Load a CPL table according to its tag and IFU/channel number.
  @param  aProcessing   the processing structure, includes the input frames list
  @param  aTag          the tag
  @param  aIFU          the IFU/channel number
  @return the cpl_table * or NULL if an error occurred.

  This function tries to find an extension that is specific for the given IFU
  number, and loads that, if available. If not, it is assumed that either the
  table is a general one applicable for all IFUs or only contains data for this
  one IFU.

  In effect, it uses @ref muse_processing_load_table() and strips off the header
  component.

  @error{set CPL_ERROR_NULL_INPUT\, return NULL, aProcessing is NULL}
  @error{propagate error code\, return NULL, muse_processing_load_table() fails}
 */
/*---------------------------------------------------------------------------*/
cpl_table *
muse_processing_load_ctable(muse_processing *aProcessing, const char *aTag,
                            unsigned char aIFU)
{
  cpl_ensure(aProcessing, CPL_ERROR_NULL_INPUT, NULL);

  /* load the table, with header */
  muse_table *table = muse_processing_load_table(aProcessing, aTag, aIFU);
  if (!table) {
    return NULL;
  }
  /* strip the header part to return a "plain" CPL table */
  cpl_table *ctable = table->table;
  table->table = NULL;
  muse_table_delete(table);

  return ctable;
} /* muse_processing_load_ctable() */

/*---------------------------------------------------------------------------*/
/**
  @brief  Load a MUSE table according to its tag and IFU/channel number.
  @param  aProcessing   the processing structure, includes the input frames list
  @param  aTag          the tag
  @param  aIFU          the IFU/channel number
  @return the muse_table * or NULL if an error occurred.

  This function tries to find an extension that is specific for the given IFU
  number, and loads that, if available. If not, it is assumed that either the
  table is a general one applicable for all IFUs or only contains data for this
  one IFU.

  The header component of the returned MUSE table is always loaded from the
  primary FITS extension.

  @error{set CPL_ERROR_NULL_INPUT\, return NULL, aProcessing is NULL}
 */
/*---------------------------------------------------------------------------*/
muse_table *
muse_processing_load_table(muse_processing *aProcessing, const char *aTag,
                           unsigned char aIFU)
{
  cpl_ensure(aProcessing, CPL_ERROR_NULL_INPUT, NULL);

  /* find the (first) table that matches the given tag */
  cpl_frame *frame = muse_frameset_find_master(aProcessing->inframes, aTag,
                                               aIFU);
  if (!frame) {
    if (aIFU > 0) {
      cpl_msg_debug(__func__, "No table found for tag %s and IFU %hhu", aTag,
                    aIFU);
    } else {
      /* swallow the IFU part, if it's not a real IFU number */
      cpl_msg_debug(__func__, "No table found for tag %s", aTag);
    }
    return NULL;
  }

  /* try to load the extension table for the given IFU, then just return  */
  cpl_errorstate state = cpl_errorstate_get();
  muse_table *table = muse_table_load(cpl_frame_get_filename(frame), aIFU);
  if (!cpl_errorstate_is_equal(state)) {
    cpl_error_set(__func__, cpl_error_get_code());
    muse_table_delete(table);
    return NULL;
  }

  /* tables are calibrations of some sort, so append them as such into        *
   * the used frames for the FITS header of the output product, if they exist */
  muse_processing_append_used(aProcessing, frame, CPL_FRAME_GROUP_CALIB, 0);

  return table;
} /* muse_processing_load_table() */

/*---------------------------------------------------------------------------*/
/**
  @brief  Save a computed table to disk.
  @param  aProcessing   the processing structure
  @param  aIFU          the IFU/channel number
  @param  aTable        the table to be saved
  @param  aHeader       the custom properties to be included in the header
                        (for aType == MUSE_TABLE_TYPE_CPL)
  @param  aTag          tag to apply for output frames, used as prefix
  @param  aType         type of table to save (MUSE_TABLE_TYPE_CPL,
                        MUSE_TABLE_TYPE_MUSE, or MUSE_TABLE_TYPE_PIXTABLE)
  @return CPL_ERROR_NONE or the relevant cpl_error_code on error

  Save a table to disk. The filename consists on the prefix, a "-"
  sign, the IFU number of the CCD and the suffix ".fits". If the
  image saving was successful, add the output frame to the list of
  output frames in the processing structure.

  @error{CPL_ERROR_NULL_INPUT, a mandatory argument was NULL}
  @error{CPL_ERROR_ILLEGAL_INPUT, an unknown aType was given}
 */
/*---------------------------------------------------------------------------*/
cpl_error_code
muse_processing_save_table(muse_processing *aProcessing, int aIFU,
                           void *aTable, cpl_propertylist *aHeader,
                           const char *aTag, muse_table_type aType)
{
  cpl_ensure_code(aProcessing && aTable && aTag, CPL_ERROR_NULL_INPUT);
  cpl_ensure_code(aType <= MUSE_TABLE_TYPE_MUSE, CPL_ERROR_ILLEGAL_INPUT);

  /* for plain tables, the separate header is needed */
  cpl_propertylist *header;
  if (aType == MUSE_TABLE_TYPE_CPL) {
    cpl_ensure_code(aHeader, CPL_ERROR_NULL_INPUT);
    header = aHeader;
  } else if (aType == MUSE_TABLE_TYPE_MUSE) {
    header = ((muse_table *)aTable)->header;
  } else {
    header = ((muse_pixtable *)aTable)->header;
  }
  cpl_frame *frame = muse_processing_new_frame(aProcessing, aIFU, header,
                                               aTag, CPL_FRAME_TYPE_TABLE);
  cpl_msg_info(__func__, "Saving %stable as \"%s\"",
               aType == MUSE_TABLE_TYPE_PIXTABLE ? "pixel " : "",
               cpl_frame_get_filename(frame));
  cpl_error_code rc = CPL_ERROR_NONE;
  if (aType == MUSE_TABLE_TYPE_CPL) {
    cpl_size nrow = cpl_table_get_nrow((cpl_table *)aTable);
    rc = cpl_table_save((cpl_table *)aTable, aHeader, NULL,
                        cpl_frame_get_filename(frame), CPL_IO_CREATE);
    if (nrow < 1) {
      cpl_msg_warning(__func__, "Table saved as \"%s\" has no rows!", aTag);
    }
  } else if (aType == MUSE_TABLE_TYPE_MUSE) {
    rc = muse_table_save((muse_table *)aTable, cpl_frame_get_filename(frame));
  } else {
    rc = muse_pixtable_save((muse_pixtable *)aTable,
                            cpl_frame_get_filename(frame));
  }
  if (rc == CPL_ERROR_NONE) {
#pragma omp critical(muse_processing_output_frames)
    cpl_frameset_insert(aProcessing->outframes, frame);
  } else {
    cpl_msg_error(__func__, "Saving %stable failed: %s",
                  aType == MUSE_TABLE_TYPE_PIXTABLE ? "pixel " : "",
                  cpl_error_get_message());
    cpl_frame_delete(frame);
  }
  return rc;
} /* muse_processing_save_table() */

/*---------------------------------------------------------------------------*/
/**
  @brief Save a computed FITS image to disk.
  @param aProcessing the processing structure
  @param aIFU        the IFU/channel number
  @param aImage      the image to be saved
  @param aHeader     the custom properties to be included in the header
  @param aTag        tag to apply for output frames, used as prefix
  @return the CPL error code

  Save an image to disk. The filename consists of the prefix, a "-"
  sign, the IFU number of the CCD, and the suffix ".fits". If the
  image saving was successful, add the output frame to the list of
  output frames in the processing structure.

  @error{CPL_ERROR_NULL_INPUT, an argument was NULL}
 */
/*---------------------------------------------------------------------------*/
int
muse_processing_save_cimage(muse_processing *aProcessing, int aIFU,
                            cpl_image *aImage, cpl_propertylist *aHeader,
                            const char *aTag)
{
  cpl_ensure_code(aProcessing && aImage && aHeader && aTag, CPL_ERROR_NULL_INPUT);

  cpl_frame *frame = muse_processing_new_frame(aProcessing, aIFU,
                                               aHeader, aTag,
                                               CPL_FRAME_TYPE_IMAGE);
  cpl_msg_info(__func__, "Saving image as \"%s\"",
               cpl_frame_get_filename(frame));
  int r = cpl_image_save(aImage, cpl_frame_get_filename(frame),
                         CPL_TYPE_UNSPECIFIED, aHeader, CPL_IO_CREATE);
  if (r == CPL_ERROR_NONE) {
#pragma omp critical(muse_processing_output_frames)
    cpl_frameset_insert(aProcessing->outframes, frame);
  } else {
    cpl_msg_error(__func__, "Saving image failed: %s", cpl_error_get_message());
    cpl_frame_delete(frame);
  }
  return r;
}

/*---------------------------------------------------------------------------*/
/**
  @brief  load a header according to its tag
  @param  aProcessing   the processing structure, includes the input frames list
  @param  aTag          the tag
  @return the cpl_propertylist * or NULL if an error occurred.

  This function tries to find the frame in the input frameset and loads the
  propertylist from it.

  @error{set CPL_ERROR_NULL_INPUT\, return NULL, aProcessing is NULL}
 */
/*---------------------------------------------------------------------------*/
cpl_propertylist *
muse_processing_load_header(muse_processing *aProcessing, const char *aTag)
{
  cpl_ensure(aProcessing, CPL_ERROR_NULL_INPUT, NULL);

  cpl_frame *frame = muse_frameset_find_master(aProcessing->inframes, aTag, 0);
  if (!frame) {
    cpl_msg_debug(__func__, "No propertylist found for tag %s", aTag);
    return NULL;
  }

  const char *filename = cpl_frame_get_filename(frame);
  cpl_propertylist *pl = cpl_propertylist_load(filename, 0);
  if (!pl) {
    cpl_msg_info(__func__, "loading %s from file %s failed: %s", aTag,
                 filename, cpl_error_get_message());
    cpl_frame_delete(frame);
    return NULL;
  }
  cpl_msg_info(__func__, "loaded %s from file \"%s\"", aTag, filename);
  /* property lists are calibrations of some sort, so append them as such into *
   * the used frames for the FITS header of the output product, if they exist  */
  muse_processing_append_used(aProcessing, frame, CPL_FRAME_GROUP_CALIB, 0);

  return pl;
} /* muse_processing_load_header() */

/*---------------------------------------------------------------------------*/
/**
  @brief Save a FITS header to disk.
  @param aProcessing the processing structure
  @param aIFU        the IFU/channel number
  @param aHeader     the header to be saved
  @param aTag        tag to apply for output frames, used as prefix
  @return the CPL error code

  Save a bare header to disk. The filename consists of the prefix, a "-"
  sign, the IFU number of the CCD, and the suffix ".fits". If the
  header saving was successful, add the output frame to the list of
  output frames in the processing structure.

  @error{CPL_ERROR_NULL_INPUT, an argument was NULL}
 */
/*---------------------------------------------------------------------------*/
cpl_error_code
muse_processing_save_header(muse_processing *aProcessing, int aIFU,
                            cpl_propertylist *aHeader, const char *aTag)
{
  cpl_ensure_code(aProcessing && aHeader && aTag, CPL_ERROR_NULL_INPUT);

  cpl_frame *frame = muse_processing_new_frame(aProcessing, aIFU,
                                               aHeader, aTag,
                                               CPL_FRAME_TYPE_IMAGE);
  cpl_msg_info(__func__, "Saving header as \"%s\"",
               cpl_frame_get_filename(frame));
  cpl_error_code rc = cpl_propertylist_save(aHeader,
                                            cpl_frame_get_filename(frame),
                                            CPL_IO_CREATE);
  if (rc == CPL_ERROR_NONE) {
#pragma omp critical(muse_processing_output_frames)
    cpl_frameset_insert(aProcessing->outframes, frame);
  } else {
    cpl_msg_error(__func__, "Saving header failed: %s", cpl_error_get_message());
    cpl_frame_delete(frame);
  }
  return rc;
} /* muse_processing_save_header() */

/*---------------------------------------------------------------------------*/
/**
  @brief  Load a mask file and its FITS header
  @param  aProcessing   the processing structure, includes the input frames list
  @param  aTag          the tag
  @return a new <tt><b>muse_mask *</b></tt> or NULL on error
  @remark The new mask has to be deallocated using
          <tt><b>muse_mask_delete()</b></tt>.
  @remark Only FITS keywords from the primary FITS header will be loaded.

  @error{return NULL\, propagate CPL error code,
         muse_frameset_find() fails to find a frame of the given tag}
  @error{return NULL\, propagate CPL error code, muse_mask_load() returns NULL}
 */
/*---------------------------------------------------------------------------*/
muse_mask *
muse_processing_load_mask(muse_processing *aProcessing, const char *aTag)
{
  cpl_frameset *frames = muse_frameset_find(aProcessing->inframes,
                                            aTag, 0, CPL_FALSE);
  if (frames == NULL || cpl_frameset_get_size(frames) <= 0) {
    cpl_frameset_delete(frames);
    return NULL;
  }
  cpl_frame *frame = cpl_frameset_get_position(frames, 0);
  muse_mask *mask = muse_mask_load(cpl_frame_get_filename(frame));
  if (!mask) {
    cpl_msg_warning(__func__, "loading mask \"%s\" failed!",
                    cpl_frame_get_filename(frame));
    cpl_frameset_delete(frames);
    return NULL;
  }
  cpl_msg_info(__func__, "using mask \"%s\" (%"CPL_SIZE_FORMAT" pixels)",
               cpl_frame_get_filename(frame), cpl_mask_count(mask->mask));
  muse_processing_append_used(aProcessing, frame, CPL_FRAME_GROUP_CALIB, 1);
  cpl_frameset_delete(frames);
  return mask;
} /* muse_processing_load_mask() */

/*---------------------------------------------------------------------------*/
/**
  @brief Save a computed MUSE mask to disk.
  @param aProcessing the processing structure
  @param aIFU        the IFU/channel number
  @param aMask       the mask to be saved
  @param aTag        tag to apply for output frames, used as prefix
  @return the CPL error code

  Save an mask to disk. The filename consists on the prefix, a "-"
  sign, the IFU number of the CCD and the suffix ".fits". If the
  mask saving was successful, add the output frame to the list of
  output frames in the processing structure.

  @error{CPL_ERROR_NULL_INPUT, an argument was NULL}
 */
/*---------------------------------------------------------------------------*/
int
muse_processing_save_mask(muse_processing *aProcessing, int aIFU,
                          muse_mask *aMask, const char *aTag)
{
  cpl_ensure_code(aProcessing && aMask && aTag, CPL_ERROR_NULL_INPUT);

  cpl_frame *frame = muse_processing_new_frame(aProcessing, aIFU,
                                               aMask->header, aTag,
                                               CPL_FRAME_TYPE_IMAGE);
  cpl_msg_info(__func__, "Saving mask as \"%s\"",
               cpl_frame_get_filename(frame));
  int r = muse_mask_save(aMask, cpl_frame_get_filename(frame));
  if (r == CPL_ERROR_NONE) {
#pragma omp critical(muse_processing_output_frames)
    cpl_frameset_insert(aProcessing->outframes, frame);
  } else {
    cpl_frame_delete(frame);
  }
  return r;
} /* muse_processing_save_mask() */

/*---------------------------------------------------------------------------*/
/**
  @brief  Check that a tag is part of the input tags of a processing structure.
  @param  aProcessing   the processing structure
  @param  aTag          the tag to check for
  @param  aNChars       the number of characters to check
  @return CPL_TRUE when the tag was found, CPL_FALSE otherwise and on error.

  Check the intags array of aProcessing against aTag, using strncmp() and up
  to aNChars length.

  @error{return CPL_ERROR_NULL_INPUT, aProcessing and/or aTag are NULL}
 */
/*---------------------------------------------------------------------------*/
cpl_boolean
muse_processing_check_intags(muse_processing *aProcessing, const char *aTag,
                             int aNChars)
{
  cpl_ensure(aProcessing && aTag, CPL_ERROR_NULL_INPUT, CPL_FALSE);

  cpl_boolean hastag = CPL_FALSE;
  /* loop through all tags, looking for a matching one */
  cpl_size itag, ntags = cpl_array_get_size(aProcessing->intags);
  for (itag = 0; itag < ntags; itag++) {
    const char *tag = cpl_array_get_string(aProcessing->intags, itag);
    if (tag && !strncmp(tag, aTag, aNChars)) {
      hastag = CPL_TRUE;
      break; /* found one match, that's enough */
    } /* if found */
  } /* for itag */
  return hastag;
} /* muse_processing_check_intags() */

/*---------------------------------------------------------------------------*/
/**
  @brief  Check the input files for completeness.
  @param  aProcessing   the processing structure
  @param  aIFU          the IFU/channel number
  @return CPL_ERROR_NONE on success or a cpl_error_code on any error.

  Check the input frames for completeness. We check the min and max
  of all raw input files and the calibration files associated to the raw inputs.
  On error, a message will be printed and the relevant CPL error code will be
  returned.

  @error{return CPL_ERROR_NULL_INPUT, aProcessing is NULL}
  @error{return CPL_ERROR_ILLEGAL_INPUT, recipe config not found in aProcessing}
  @error{output error message\, return CPL_ERROR_DATA_NOT_FOUND,
         a least one input requirement is not fulfilled}
 */
/*---------------------------------------------------------------------------*/
cpl_error_code
muse_processing_check_input(muse_processing *aProcessing, unsigned char aIFU)
{
  if (!aProcessing) {
    cpl_msg_error(__func__, "NULL processing struct");
    return CPL_ERROR_NULL_INPUT;
  }
  cpl_recipeconfig *recipeconfig
    = muse_processing_get_recipeconfig(aProcessing->recipe);
  if (!recipeconfig) {
    cpl_msg_error(__func__, "No recipeconfig found!");
    return CPL_ERROR_ILLEGAL_INPUT;
  }

  cpl_boolean output = CPL_TRUE; /* start with outputting debug messages */
  cpl_size itag, ntags = cpl_array_get_size(aProcessing->intags);
  unsigned int errors = 0;
  for (itag = 0; itag < ntags; itag++) {
    const char *intag = cpl_array_get_string(aProcessing->intags, itag);
    cpl_frameset *frames = muse_frameset_find(aProcessing->inframes,
                                              intag, aIFU, CPL_FALSE);
    int nframes = cpl_frameset_count_tags(frames, intag),
        nmin = cpl_recipeconfig_get_min_count(recipeconfig, intag, intag),
        nmax = cpl_recipeconfig_get_max_count(recipeconfig, intag, intag);
    cpl_frameset_delete(frames);
    if (nmin >= 0 && nframes < nmin) {
      cpl_msg_error(__func__, "Required %d, found %d input frames with tag "
                    "\"%s\" with IFU %hhu", nmin, nframes, intag, aIFU);
      errors++;
    }
    if (nmax >= 0 && nframes > nmax) {
      cpl_msg_error(__func__, "At most %d allowed, found %d input frames with "
                    "tag \"%s\" with IFU %hhu", nmax, nframes, intag, aIFU);
      errors++;
    }
    char **tags = cpl_recipeconfig_get_inputs(recipeconfig, intag);
    if (!tags) {
      cpl_msg_error(__func__, "Input frames with tag \"%s\" cannot be used with"
                    " this recipe", intag);
      errors++;
      continue;
    }
    /* check all other tags related to this raw input file */
    int i;
    for (i = 0; tags[i]; i++) {
      frames = muse_frameset_find(aProcessing->inframes, tags[i], aIFU,
                                  CPL_FALSE);
      nframes = cpl_frameset_count_tags(frames, tags[i]);
      cpl_frameset_delete(frames);

      nmin = cpl_recipeconfig_get_min_count(recipeconfig, intag, tags[i]);
      nmax = cpl_recipeconfig_get_max_count(recipeconfig, intag, tags[i]);
      if (nmin >= 0 && nframes < nmin) {
        if (output) {
          cpl_msg_error(__func__, "Required %d, found %d frames with tag \"%s\" "
                        "with IFU %hhu", nmin, nframes, tags[i], aIFU);
        }
        errors++;
      }
      if (nframes == 0 && nmin <= 0) {
        if (output) {
          cpl_msg_debug(__func__, "Optional frame with tag \"%s\" not given",
                        tags[i]);
        }
      }
      if (nmax >= 0 && nframes > nmax) {
        if (output) {
          cpl_msg_error(__func__, "At most %d allowed, found %d frames with tag "
                        "\"%s\" with IFU %hhu", nmax, nframes, tags[i], aIFU);
        }
        errors++;
      }
      cpl_free(tags[i]);
    } /* for i (all tags) */
    cpl_free(tags);
    output = CPL_FALSE; /* printing the output for one raw file is enough */
  } /* for itag */

  if (errors) {
    cpl_msg_error(__func__, "Found %u error(s)", errors);
    return CPL_ERROR_DATA_NOT_FOUND;
  }
  return CPL_ERROR_NONE;
} /* muse_processing_check_input() */

/*----------------------------------------------------------------------------*/
/**
  @brief    Sort input frames (containing lists of pixel table filenames) into
            different exposures.
  @param    aProcessing   the processing structure, especially containing
                          input/output frames and the tag
  @return   the exposure table on success or NULL on error

  @remark   This function assumes that all relevant FITS keywords (DATE-OBS
            and EXTNAME with the CHANnn number) are contained in the primary
            header.

  This function uses the DATE-OBS header keyword to check which of the input
  objects belong to the same exposure. As they all have been split up from the
  same exposure in the beginning, DATE-OBS should be identical for related
  files.

  If one IFU of the same exposure is given multiple times in the input
  frameset, only the first one is used and a warning is printed for all others.

  @note This function also modifies the usedframes component of the aProcessing
        structure, and adds all newly found exposures to that frameset. However,
        it is possible to run this function more than once, since no duplicate
        frames are added to that frameset.

  @error{set CPL_ERROR_NULL_INPUT\, return NULL, invalid processing pointer}
  @error{set CPL_ERROR_DATA_NOT_FOUND\, return NULL, no exposures found}
 */
/*----------------------------------------------------------------------------*/
cpl_table *
muse_processing_sort_exposures(muse_processing *aProcessing)
{
  cpl_ensure(aProcessing, CPL_ERROR_NULL_INPUT, NULL);
  cpl_size nframes = cpl_frameset_get_size(aProcessing->inframes);
  cpl_ensure(nframes, CPL_ERROR_DATA_NOT_FOUND, NULL);

  /* now that we have at least one frame, we can assume that the frameset is   *
   * valid and we have at least one exposure, so we can create the empty table */
  cpl_table *exptable = cpl_table_new(0);
  /* create columns for DATE-OBS and all IFUs */
  cpl_table_new_column(exptable, "DATE-OBS", CPL_TYPE_STRING);
  char colname[3]; /* further used in this function, define outside loop */
  int i;
  for (i = 0; i <= kMuseNumIFUs; i++) {
    snprintf(colname, 3, "%02d", i);
    cpl_table_new_column(exptable, colname, CPL_TYPE_STRING);
  }

  /* loop over all input frames */
  cpl_size iframe;
  for (iframe = 0; iframe < nframes; iframe++) {
    cpl_frame *frame = cpl_frameset_get_position(aProcessing->inframes,
                                                 iframe);
    const char *tag = cpl_frame_get_tag(frame);
    if (muse_processing_check_intags(aProcessing, tag, strlen(tag))) {
      const char *filename = cpl_frame_get_filename(frame);

      /* load the primary header and get DATE-OBS and the IFU number */
      cpl_propertylist *header = cpl_propertylist_load(filename, 0);
      if (!header) {
        cpl_msg_warning(__func__, "\"%s\" could not be loaded, it will be "
                        "ignored!", filename);
        continue; /* skip on to the next frame */
      }
      const char *date = muse_pfits_get_dateobs(header);
      if (!date) {
        cpl_msg_warning(__func__, "\"%s\" does not contain the DATE-OBS "
                        "keyword, it will be ignored!", filename);
        cpl_propertylist_delete(header);
        continue; /* skip on to the next frame */
      }
      unsigned char ifu = muse_utils_get_ifu(header);
      if (!ifu) {
        cpl_msg_debug(__func__, "\"%s\" seems to contain merged data (no "
                      "EXTNAME=CHANnn)", filename);
      }
#if DEBUG_EXPOSURES_SORT
      cpl_msg_debug(__func__, "\"%s\": IFU %2hhu, DATE-OBS=\"%s\"", filename,
                    ifu, date);
#endif

      /* loop through all exposures (=rows) in the table and compare the date */
      int irow = -1;
      for (i = 0; i < cpl_table_get_nrow(exptable); i++) {
#if DEBUG_EXPOSURES_SORT
        cpl_msg_debug(__func__, "i=%d, DATE-OBS=\"%s\"", i,
                      cpl_table_get_string(exptable, "DATE-OBS", i));
#endif
        if (!strcmp(date, cpl_table_get_string(exptable, "DATE-OBS", i))) {
          irow = i;
        }
      }

      /* didn't find a matching entry, add a new exposure (=row) */
      if (irow < 0) {
        cpl_table_set_size(exptable, cpl_table_get_nrow(exptable)+1);
        irow = cpl_table_get_nrow(exptable) - 1;
        cpl_table_set_string(exptable, "DATE-OBS", irow, date);
      }

      /* check, if table cell for this exposure is still empty and, *
       * if so, add the filename                                    */
      snprintf(colname, 3, "%02hhu", ifu);
      if (cpl_table_is_valid(exptable, colname, irow)) {
        cpl_msg_warning(__func__, "we already have IFU %2hhu of exposure %d "
                        "(\"%s\")! Ignoring \"%s\"", ifu, irow+1,
                        cpl_table_get_string(exptable, colname, irow),
                        filename);
        cpl_propertylist_delete(header);
        continue; /* skip on to the next frame */
      }
      cpl_table_set_string(exptable, colname, irow, filename);
      /* now we can assume that this is valid and will be used; it is not a   *
       * RAW file, but it's the main input, so the RAW group will satisfy the *
       * DFS-related functions in CPL                                         */
      muse_processing_append_used(aProcessing, frame, CPL_FRAME_GROUP_RAW, 1);

      cpl_propertylist_delete(header);
    } /* if correct tag */
#if DEBUG_EXPOSURES_SORT
    else {
      cpl_msg_debug(__func__, "wrong tag \"%s\" for \"%s\"", tag,
                    cpl_frame_get_filename(frame));
    }
#endif
  } /* for iframe (all input frames) */

  if (cpl_table_get_nrow(exptable) <= 0) {
    /* no exposures found */
    cpl_table_delete(exptable);
    cpl_error_set(__func__, CPL_ERROR_DATA_NOT_FOUND);
    return NULL;
  }
  /* loop through all exposures and print the number of valid IFUs found */
  for (i = 0; i < cpl_table_get_nrow(exptable); i++) {
    unsigned int nmerged = 0;
    if (cpl_table_is_valid(exptable, "00", i)) {
      nmerged++;
    }
    unsigned char ifu, nvalid = 0;
    for (ifu = 1; ifu <= kMuseNumIFUs; ifu++) {
      snprintf(colname, 3, "%02hhu", ifu);
      if (cpl_table_is_valid(exptable, colname, i)) {
        nvalid++;
      }
    } /* for i (table columns) */
    cpl_msg_debug(__func__, "Data from exposure %2d is contained in %2hhu "
                  "IFU%s/%u merged file%s", i+1, nvalid, nvalid == 1 ? "" : "s",
                  nmerged, nmerged == 1 ? "" : "s");
  } /* for i (table rows) */

  /* sort table by increasing DATE-OBS, so that we process the exposures *
   * in chronological order                                              */
  cpl_propertylist *sorting = cpl_propertylist_new();
  cpl_propertylist_append_bool(sorting, "DATE-OBS",
                               CPL_FALSE); /* sort ascending */
  cpl_table_sort(exptable, sorting);
  cpl_propertylist_delete(sorting);

  return exptable;
} /* muse_processing_sort_exposures() */

/**@}*/

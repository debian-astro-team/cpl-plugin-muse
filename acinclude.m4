# MUSE_SET_PREFIX(PREFIX)
#---------------------------
AC_DEFUN([MUSE_SET_PREFIX],
[
    unset CDPATH
    # make $PIPE_HOME the default for the installation
    AC_PREFIX_DEFAULT($1)

    if test "x$prefix" = "xNONE"; then
        prefix=$ac_default_prefix
        ac_configure_args="$ac_configure_args --prefix $prefix"
    fi

    if test "x$exec_prefix" = "xNONE"; then
        exec_prefix=$prefix
    fi

])


# MUSE_SET_VERSION_INFO(VERSION, [CURRENT], [REVISION], [AGE])
#----------------------------------------------------------------
# Setup various version information, especially the libtool versioning
AC_DEFUN([MUSE_SET_VERSION_INFO],
[
    muse_version=`echo "$1" | sed -e 's/[[a-z,A-Z]].*$//'`

    muse_major_version=`echo "$muse_version" | \
        sed 's/\([[0-9]]*\).\(.*\)/\1/'`
    muse_minor_version=`echo "$muse_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\)\(.*\)/\2/'`
    muse_micro_version=`echo "$muse_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`

    if test -z "$muse_major_version"; then muse_major_version=0
    fi

    if test -z "$muse_minor_version"; then muse_minor_version=0
    fi

    if test -z "$muse_micro_version"; then muse_micro_version=0
    fi

    MUSE_VERSION="$muse_version"
    MUSE_MAJOR_VERSION=$muse_major_version
    MUSE_MINOR_VERSION=$muse_minor_version
    MUSE_MICRO_VERSION=$muse_micro_version

    if test -z "$4"; then MUSE_INTERFACE_AGE=0
    else MUSE_INTERFACE_AGE="$4"
    fi

    MUSE_BINARY_AGE=`expr 100 '*' $MUSE_MINOR_VERSION + $MUSE_MICRO_VERSION`
    MUSE_BINARY_VERSION=`expr 10000 '*' $MUSE_MAJOR_VERSION + \
                          $MUSE_BINARY_AGE`

    AC_SUBST(MUSE_VERSION)
    AC_SUBST(MUSE_MAJOR_VERSION)
    AC_SUBST(MUSE_MINOR_VERSION)
    AC_SUBST(MUSE_MICRO_VERSION)
    AC_SUBST(MUSE_INTERFACE_AGE)
    AC_SUBST(MUSE_BINARY_VERSION)
    AC_SUBST(MUSE_BINARY_AGE)

    AC_DEFINE_UNQUOTED(MUSE_MAJOR_VERSION, $MUSE_MAJOR_VERSION,
                       [MUSE major version number])
    AC_DEFINE_UNQUOTED(MUSE_MINOR_VERSION, $MUSE_MINOR_VERSION,
                       [MUSE minor version number])
    AC_DEFINE_UNQUOTED(MUSE_MICRO_VERSION, $MUSE_MICRO_VERSION,
                       [MUSE micro version number])
    AC_DEFINE_UNQUOTED(MUSE_INTERFACE_AGE, $MUSE_INTERFACE_AGE,
                       [MUSE interface age])
    AC_DEFINE_UNQUOTED(MUSE_BINARY_VERSION, $MUSE_BINARY_VERSION,
                       [MUSE binary version number])
    AC_DEFINE_UNQUOTED(MUSE_BINARY_AGE, $MUSE_BINARY_AGE,
                       [MUSE binary age])

    ESO_SET_LIBRARY_VERSION([$2], [$3], [$4])
])


# MUSE_SET_PATHS
#------------------
# Define auxiliary directories of the installed directory tree.
AC_DEFUN([MUSE_SET_PATHS],
[

    if test -z "$plugindir"; then
        plugindir='${libdir}/esopipes-plugins/${PACKAGE}-${VERSION}'
    fi
    AC_SUBST(plugindir)

    if test -z "$configdir"; then
       configdir='${datadir}/${PACKAGE}/config'
    fi
    AC_SUBST(configdir)

    if test -z "$pipelinelibdir"; then
        pipelinelibdir='${libdir}/${PACKAGE}-${VERSION}'
    fi
    AC_SUBST(pipelinelibdir)

    if test -z "$pipelinedocdir"; then
        pipelinedocdir='${datadir}/doc/esopipes/${PACKAGE}-${VERSION}'
    fi
    AC_SUBST(pipelinedocdir)

    if test -z "$wkfdir"; then
        wkfdir='${datadir}/esopipes/${PACKAGE}-${VERSION}/reflex'
    fi
    AC_SUBST(wkfdir)

    if test -z "$wkfxmldir"; then
        wkfxmldir='${datadir}/reflex/workflows/${PACKAGE}-${VERSION}'
    fi
    AC_SUBST(wkfxmldir)

    if test -z "$wkfdocdir"; then
        wkfdocdir='${pipelinedocdir}/reflex'
    fi
    AC_SUBST(wkfdocdir)

    wkfaddonsprefix="contrib_wkf"
    AC_SUBST(wkfaddonsprefix)

    if test -z "$apidocdir"; then
        apidocdir='${pipelinedocdir}/html'
    fi
    AC_SUBST(apidocdir)


    # Define a preprocesor symbol for the plugin search paths

    AC_DEFINE_UNQUOTED(MUSE_PLUGIN_DIR, "esopipes-plugins",
                       [Plugin directory tree prefix])

    eval plugin_dir="$plugindir"
    plugin_path=`eval echo $plugin_dir | \
                sed -e "s/\/${PACKAGE}-${VERSION}.*$//"`

    AC_DEFINE_UNQUOTED(MUSE_PLUGIN_PATH, "$plugin_path",
                       [Absolute path to the plugin directory tree])

])


# MUSE_CREATE_SYMBOLS
#-----------------------
# Define include and library related makefile symbols
AC_DEFUN([MUSE_CREATE_SYMBOLS],
[

    # Symbols for package include file and library search paths

    MUSE_INCLUDES='-I$(top_srcdir)/muse -I$(top_builddir)/muse'
    MUSE_LDFLAGS='-L$(top_builddir)/muse'

    all_includes='$(MUSE_INCLUDES) $(CPL_INCLUDES) $(EXTRA_INCLUDES)'
    all_ldflags='$(MUSE_LDFLAGS) $(CPL_LDFLAGS) $(EXTRA_LDFLAGS)'

    # Library aliases

    LIBMUSE='$(top_builddir)/muse/libmuse.la'

    # Substitute the defined symbols

    AC_SUBST(MUSE_INCLUDES)
    AC_SUBST(MUSE_LDFLAGS)

    AC_SUBST(LIBMUSE)

    # Check for CPL and user defined libraries
    AC_REQUIRE([CPL_CHECK_LIBS])
    AC_REQUIRE([ESO_CHECK_EXTRA_LIBS])

    all_includes='$(MUSE_INCLUDES) $(CPL_INCLUDES) $(EXTRA_INCLUDES)'
    all_ldflags='$(MUSE_LDFLAGS) $(CPL_LDFLAGS) $(EXTRA_LDFLAGS)'

    AC_SUBST(all_includes)
    AC_SUBST(all_ldflags)
])


# MUSE_CHECK_DOCTOOLS
#--------------------
# Check for the availability of documentation tools like doxygen,
# latex, and xsltproc.
AC_DEFUN([MUSE_CHECK_DOCTOOLS],
[
    AC_REQUIRE([ESO_CHECK_DOCTOOLS])

    AC_ARG_VAR([LATEX2HTML], [latex2html command])
    AC_PATH_PROG([LATEX2HTML], [latex2html])

    AC_ARG_VAR([XSLTPROC], [xsltproc command])
    AC_PATH_PROG([XSLTPROC], [xsltproc])

    if test -z "${LATEX2HTML}"; then
        LATEX2HTML=":"
    fi

    if test -z "${XSLTPROC}"; then
        XSLTPROC=":"
    fi
])

# MUSE_CHECK_OPENMP
#------------------
# Check for the availability of OpenMP, taking into account that with
# GCC 4.2.x OpenMP is unusable. This is because it cannot be used from shared
# libraries while the main program (in our case esorex) isn't linked
# against libgomp (see http://gcc.gnu.org/bugzilla/show_bug.cgi?id=28482).
# Now (with muse_autocalib) we also need OpenMP 3.1 or _OPENMP=201107,
# which was implemented first in gcc 4.7.0.
AC_DEFUN([MUSE_CHECK_OPENMP],
[
    AC_REQUIRE([AC_PROG_CC])
    AC_REQUIRE([AC_PROG_AWK])

    muse_have_openmp="yes"
    if test "$GCC" ; then
        AC_MSG_CHECKING([whether GCC has a working OpenMP implementation])
        muse_gcc_version=`$CC -v 2>&1 | $AWK '/^gcc version/ {print [$]3}'`
        case "$muse_gcc_version" in
        4.2.*)
            AC_MSG_RESULT([no, broken GCC (version $muse_gcc_version)])
            muse_have_openmp="no"
            ;;
        4.3.* | 4.4.* | 4.5.* | 4.6.*)
            AC_MSG_RESULT([partly (GCC $muse_gcc_version, no OpenMP 3.1)])
            ;;
        *)
            AC_MSG_RESULT([yes (GCC $muse_gcc_version)])
            ;;
        esac
    fi

    if test "x${muse_have_openmp}" = xyes; then
        CPL_OPENMP
        case $ac_cv_prog_c_openmp in
        "none needed" | unsupported)
            ;;
        *)
            # FIXME: Explicitly add the OpenMP runtime library, since libtool
            #        removes the compiler flag when linking!
            AC_SEARCH_LIBS([omp_get_num_threads], [gomp mtsk omp], [],
                           AC_MSG_ERROR([OpenMP runtime environment not found!]))

            CFLAGS="$CFLAGS $OPENMP_CFLAGS"
            LDFLAGS="$LDFLAGS $OPENMP_CFLAGS"
            ;;
        esac
    else
        AC_MSG_WARN([OpenMP will not be used])
    fi
])


# MUSE_CHECK_CPL_VERSION(VERSION)
#--------------------------------
# Check that the CPL version is at least the given one
AC_DEFUN([MUSE_CHECK_CPL_VERSION],
[
    cpl_version_major=`echo "$1" | cut -d"." -f1`
    cpl_version_minor=`echo "$1" | cut -d"." -f2`
    cpl_version_micro=`echo "$1" | cut -d"." -f3`
    if test x"${cpl_version_micro}" = x; then
      cpl_version_micro="0"
    fi

    # Check CPL version
    AC_MSG_CHECKING([for a CPL version >= $1])
    AC_LANG_PUSH(C)
    cpl_cflags_save="$CFLAGS"
    cpl_ldflags_save="$LDFLAGS"
    cpl_libs_save="$LIBS"
    CFLAGS="$CPL_INCLUDES"
    LDFLAGS="$CPL_LDFLAGS"
    LIBS="$LIBCPLCORE $LIBPTHREAD"
    AC_RUN_IFELSE([AC_LANG_PROGRAM(
                  [[
                  #include <stdio.h>
                  #include <cpl_version.h>
                  ]],
                  [
                  FILE* f = fopen("conftest.out", "w");
                  fprintf(f, "%s\n", cpl_version_get_version());
                  /* fprintf(f, "%d\n", cpl_version_get_major()); */
                  /* fprintf(f, "%d\n", cpl_version_get_minor()); */
                  /* fprintf(f, "%d\n", cpl_version_get_micro()); */
                  int rc = 1; /* fail by default */
                  if (cpl_version_get_major() > ${cpl_version_major}) {
                    rc = 0;
                  } else if (cpl_version_get_major() == ${cpl_version_major} &&
                             cpl_version_get_minor() > ${cpl_version_minor}) {
                    rc = 0;
                  } else if (cpl_version_get_major() == ${cpl_version_major} &&
                             cpl_version_get_minor() == ${cpl_version_minor} &&
                             cpl_version_get_micro() >= ${cpl_version_micro}) {
                    rc = 0;
                  }
                  fclose(f);
                  return rc;
                  ])],
                  [cpl_version="`cat conftest.out`"],
                  [cpl_version="no"])
    AC_MSG_RESULT([${cpl_version}])
    AC_LANG_POP(C)
    CFLAGS="$cpl_cflags_save"
    LDFLAGS="$cpl_ldflags_save"
    LIBS="$cpl_libs_save"

    if test x"$cpl_version" = xno; then
        AC_MSG_ERROR([CPL version is too old, need at least v$1 to run the pipeline!])
    fi
])


# MUSE_CHECK_CPL_FFTW(VERSION)
#--------------------------------
# Check that the CPL was compiled against FFTW
AC_DEFUN([MUSE_CHECK_CPL_FFTW],
[
    # Check CPL version
    AC_MSG_CHECKING([for a CPL version with FFTW support])
    AC_LANG_PUSH(C)
    cpl_cflags_save="$CFLAGS"
    cpl_ldflags_save="$LDFLAGS"
    cpl_libs_save="$LIBS"
    CFLAGS="$CPL_INCLUDES"
    LDFLAGS="$CPL_LDFLAGS"
    LIBS="$LIBCPLCORE $LIBPTHREAD"
    AC_RUN_IFELSE([AC_LANG_PROGRAM(
                  [[
                  #include <stdio.h>
                  #include <string.h>
                  #include <cpl.h>
                  ]],
                  [
                  FILE* f = fopen("conftest.out", "w");
                  const char *info = cpl_get_description(CPL_DESCRIPTION_DEFAULT);
                  int rc = 1; /* fail */
                  if (strstr(info, "FFTW unavailable")) {
                    fprintf(f, "no\n");
                  } else {
                    fprintf(f, "yes\n");
                    rc = 0; /* succeed */
                  }
                  fclose(f);
                  return rc;
                  ])],
                  [cpl_fftw_supported="`cat conftest.out`"],
                  [cpl_fftw_supported="no"])
    AC_MSG_RESULT([${cpl_fftw_supported}])
    AC_LANG_POP(C)
    CFLAGS="$cpl_cflags_save"
    LDFLAGS="$cpl_ldflags_save"
    LIBS="$cpl_libs_save"

    if test x"$cpl_fftw_supported" = xno; then
        AC_MSG_ERROR([CPL was not compiled against FFTW (required for the MUSE pipeline)!])
    fi
])


# MUSE_ENABLE_STRICT(strict=no)
#------------------------------
AC_DEFUN([MUSE_ENABLE_STRICT],
[

    ESO_ENABLE_STRICT([$1])

    if test x"$eso_cv_enable_strict" = xyes; then

        # add all the warning flags mentioned by Ralf
        ESO_PROG_CC_FLAG([Wall], [CFLAGS="$CFLAGS -Wall"])
        ESO_PROG_CC_FLAG([W], [CFLAGS="$CFLAGS -W"])
        ESO_PROG_CC_FLAG([Wcast-align], [CFLAGS="$CFLAGS -Wcast-align"])
        ESO_PROG_CC_FLAG([Wmissing-noreturn], [CFLAGS="$CFLAGS -Wmissing-noreturn"])
        ESO_PROG_CC_FLAG([Wpointer-arith], [CFLAGS="$CFLAGS -Wpointer-arith"])
        ESO_PROG_CC_FLAG([Winline], [CFLAGS="$CFLAGS -Winline"])
        ESO_PROG_CC_FLAG([Wshadow], [CFLAGS="$CFLAGS -Wshadow"])
        ESO_PROG_CC_FLAG([Wsign-compare], [CFLAGS="$CFLAGS -Wsign-compare"])
        ESO_PROG_CC_FLAG([Wundef], [CFLAGS="$CFLAGS -Wundef"])
        ESO_PROG_CC_FLAG([Wwrite-strings], [CFLAGS="$CFLAGS -Wwrite-strings"])
        ESO_PROG_CC_FLAG([Wmissing-field-initializers], [CFLAGS="$CFLAGS -Wmissing-field-initializers"])
        ESO_PROG_CC_FLAG([Wmissing-format-attribute], [CFLAGS="$CFLAGS -Wmissing-format-attribute"])
        # would be desirable but warns for every cpl_ensure()...
        #ESO_PROG_CC_FLAG([Wunreachable-code], [CFLAGS="$CFLAGS -Wunreachable-code"])
        ESO_PROG_CC_FLAG([Wno-overlength-strings], [CFLAGS="$CFLAGS -Wno-overlength-strings"])
        ESO_PROG_CC_FLAG([Wabsolute-value], [CFLAGS="$CFLAGS -Wabsolute-value"])
        ESO_PROG_CC_FLAG([Wmaybe-uninitialized], [CFLAGS="$CFLAGS -Wmaybe-uninitialized"])
        ESO_PROG_CC_FLAG([Wformat], [CFLAGS="$CFLAGS -Wformat"])
        ESO_PROG_CC_FLAG([Wunknown-pragmas], [CFLAGS="$CFLAGS -Wunknown-pragmas"])

    fi
])


# MUSE_CHECK_CAIRO
#-----------------
# Checks the availability of the cairo library and its header files.
AC_DEFUN([MUSE_CHECK_CAIRO],
[

    AC_REQUIRE([ESO_PROG_PKGCONFIG])

    muse_have_cairo=no
    CAIRO_FLAGS=""
    CAIRO_LIBS=""

    AC_MSG_CHECKING([for cairo])

    if test -n "${PKGCONFIG}"; then

        $PKGCONFIG --exists cairo

        if test x$? = x0; then
            CAIRO_FLAGS=`$PKGCONFIG --cflags cairo`
            CAIRO_LIBS=`$PKGCONFIG --libs cairo`
            muse_have_cairo=yes
        fi

    else

        # Fallback if pkg-config is not available. A single try with
        # the system wide installation.

        AC_LANG_PUSH(C)

        muse_cairo_cflags_save="$CFLAGS"
        muse_cairo_ldflags_save="$LDFLAGS"
        muse_cairo_libs_save="$LIBS"

        CFLAGS="-I/usr/include/cairo"
        LIBS="-lcairo"

        AC_LINK_IFELSE([AC_LANG_PROGRAM(
                       [[
                       #include <cairo.h>
                       #include <cairo-pdf.h>
                       ]],
                       [
                       cairo_surface_t *s = cairo_pdf_surface_create(0, 0., 0.);
                       ])],
                       [muse_have_cairo="yes"],
                       [muse_have_cairo="no"])

       AC_LANG_POP(C)

       CFLAGS="$muse_cairo_cflags_save"
       LDFLAGS="$muse_cairo_ldflags_save"
       LIBS="$muse_cairo_libs_save"

       if test x$muse_have_cairo = xyes; then
           CAIRO_FLAGS="-I/usr/include/cairo"
           CAIRO_LIBS="-lcairo"
       fi
    fi

    if test x$muse_have_cairo = xyes; then
        AC_MSG_RESULT([yes])
    else
        AC_MSG_RESULT([no])
    fi

    AC_CACHE_VAL([muse_cv_have_cairo], [muse_cv_have_cairo=$muse_have_cairo])

    AC_SUBST(CAIRO_FLAGS)
    AC_SUBST(CAIRO_LIBS)

])


# MUSE_ENABLE_TOOLS(tools=yes)
#-----------------------------
# Enables/disables building of optional tools component.
AC_DEFUN([MUSE_ENABLE_TOOLS],
[
    AC_ARG_ENABLE(tools,
                  AC_HELP_STRING([--disable-tools],
                                 [disables building of the MUSE tools component]),
                  [muse_enable_tools=$enable_tools],
                  [muse_enable_tools=$1])

    AC_CACHE_VAL([muse_cv_enable_tools],
                 [muse_cv_enable_tools=$muse_enable_tools])

    SUBDIR_TOOLS=""

    if test x$muse_cv_enable_tools = xyes && test -d $srcdir/tools; then
        SUBDIR_TOOLS="tools"
        AC_CONFIG_FILES(tools/Makefile)
        
    fi

    AC_SUBST(SUBDIR_TOOLS)

])


# MUSE_ENABLE_MOCKDATA(mockdata=yes)
#-----------------------------------
# Enables/disables building of optional mockdata component.
AC_DEFUN([MUSE_ENABLE_MOCKDATA],
[
    AC_ARG_ENABLE(mockdata,
                  AC_HELP_STRING([--disable-mockdata],
                                 [disables building of the MUSE mockdata component]),
                  [muse_enable_mockdata=$enable_mockdata],
                  [muse_enable_mockdata=$1])

    AC_CACHE_VAL([muse_cv_enable_mockdata],
                 [muse_cv_enable_mockdata=$muse_enable_mockdata])

    SUBDIR_MOCKDATA=""

    if test x$muse_cv_enable_mockdata = xyes && test -d $srcdir/mockdata; then
        SUBDIR_MOCKDATA="mockdata"
        AC_CONFIG_FILES(mockdata/Makefile)
    fi

    AC_SUBST(SUBDIR_MOCKDATA)

])


# MUSE_ENABLE_DOC(doc=yes)
#-------------------------
# Enables/disables building of optional doc component and word output.
AC_DEFUN([MUSE_ENABLE_DOC],
[
    AC_ARG_ENABLE(doc,
                  AC_HELP_STRING([--disable-doc],
                                 [disables building of the MUSE doc component]),
                  [muse_enable_doc=$enable_doc],
                  [muse_enable_doc=$1])

    AC_CACHE_VAL([muse_cv_enable_doc],
                 [muse_cv_enable_doc=$muse_enable_doc])

    SUBDIR_DOC=""
    WORD_TARGETS=""

    if test x$muse_cv_enable_doc = xyes && test -d $srcdir/doc; then
        SUBDIR_DOC="doc"
        WORD_TARGETS="xml_check xml_dataformats xml_functions xml_recipes_qi xml_recipes_calib xml_recipes_sci xml_recipes_split"
        AC_CONFIG_FILES(doc/Makefile)
    fi

    AC_SUBST(SUBDIR_DOC)
    AC_SUBST(WORD_TARGETS)

])


# MUSE_WITH_TESTDATA
#-------------------
# Setup the location where the MUSE unit test data can be found.
AC_DEFUN([MUSE_WITH_TESTDATA],
[
    AC_ARG_VAR([MUSE_TESTDATA], [MUSE test data location])
    
    AC_ARG_WITH(testdata,
                AC_HELP_STRING([--with-testdata],
                               [location of the MUSE test data files]),
                [muse_with_testdata=$with_testdata],
                [muse_with_testdata="$MUSE_TESTDATA"])

    AC_CACHE_VAL([muse_cv_with_testdata],
                 [muse_cv_with_testdata=$muse_with_testdata])

    if test -n "$muse_cv_with_testdata"; then
        testdatadir="$muse_cv_with_testdata"
    else
        testdatadir='$(top_srcdir)/testdata'
    fi

    AC_SUBST(testdatadir)

])

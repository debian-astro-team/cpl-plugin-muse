/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/*
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2014 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*---------------------------------------------------------------------------*
 *                             Includes                                      *
 *---------------------------------------------------------------------------*/
#include <muse.h>
#include "muse_scipost_subtract_sky_simple_z.h"

/*----------------------------------------------------------------------------*/
/**
  @brief    Separate sky subtraction main routine
  @param    aProcessing  the processing structure
  @param    aParams      the parameters list
  @return   CPL_ERROR_NONE if everything is ok, any other CPL error if something
            went wrong
 */
/*----------------------------------------------------------------------------*/
int
muse_scipost_subtract_sky_simple_compute(muse_processing *aProcessing,
                                         muse_scipost_subtract_sky_simple_params_t *aParams)
{
  cpl_table *spectrum = muse_processing_load_ctable(aProcessing, MUSE_TAG_SKY_SPECTRUM, 0);
  if (!spectrum) {
    cpl_msg_warning(__func__, "Could not load %s", MUSE_TAG_SKY_SPECTRUM);
  }
  /* simple subtraction: pretend that the spectrum is a     *
   * continuum and use that function for direct subtraction */
  cpl_table_name_column(spectrum, "data", "flux");

  cpl_errorstate prestate = cpl_errorstate_get();
  cpl_frameset *inframes = muse_frameset_find_tags(aProcessing->inframes,
                                                   aProcessing->intags, 0,
                                                   CPL_FALSE);
  cpl_error_code rc = CPL_ERROR_NONE;
  cpl_size iframe, nframes = cpl_frameset_get_size(inframes);
  for (iframe = 0; iframe < nframes; iframe++) {
    cpl_frame *frame = cpl_frameset_get_position(inframes, iframe);
    const char *fn = cpl_frame_get_filename(frame);
    muse_pixtable *pixtable =
      muse_pixtable_load_restricted_wavelength(fn,
                                               aParams->lambdamin,
                                               aParams->lambdamax);
    if (pixtable == NULL) {
      cpl_msg_error(__func__, "NULL pixel table for \"%s\"", fn);
      rc = CPL_ERROR_NULL_INPUT;
      break;
    }
    muse_processing_append_used(aProcessing, frame, CPL_FRAME_GROUP_RAW, 1);

    /* erase pre-existing QC parameters */
    cpl_propertylist_erase_regexp(pixtable->header, "ESO QC ", 0);

    if (muse_pixtable_is_skysub(pixtable) == CPL_TRUE) {
      cpl_msg_error(__func__, "Pixel table \"%s\" already sky subtracted", fn);
      muse_pixtable_delete(pixtable);
      rc = CPL_ERROR_ILLEGAL_INPUT;
      break;
    }

    rc = muse_sky_subtract_continuum(pixtable, spectrum);
    if (rc != CPL_ERROR_NONE) {
      cpl_msg_error(__func__, "while subtracting sky spectrum from \"%s\"", fn);
      muse_pixtable_delete(pixtable);
      break;
    }
    muse_processing_save_table(aProcessing, -1, pixtable, NULL,
                               MUSE_TAG_PIXTABLE_REDUCED,
                               MUSE_TABLE_TYPE_PIXTABLE);
    muse_pixtable_delete(pixtable);
  } /* for iframe (all input frames) */
  cpl_frameset_delete(inframes);
  cpl_table_delete(spectrum);

  return cpl_errorstate_is_equal(prestate) ? 0 : rc;
} /* muse_scipost_subtract_sky_simple_compute() */

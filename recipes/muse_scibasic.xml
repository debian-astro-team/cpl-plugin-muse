<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/xsl" href="recipedoc2html.xsl" ?>
<pluginlist instrument="muse">
  <plugin name="muse_scibasic" type="recipe">
    <plugininfo>
      <synopsis>Remove the instrumental signature from the data of each CCD and convert them from an image into a pixel table.</synopsis>
      <description>
        Processing handles each raw input image separately: it trims the raw
        data and records the overscan statistics, subtracts the bias (taking
        account of the overscan, if --overscan is not "none"), optionally
        detects cosmic rays (note that by default cosmic ray rejection is
        handled in the post processing recipes while the data is reformatted
        into a datacube, so that the default setting is cr="none" here),
        converts the images from adu to count, subtracts the dark, divides by
        the flat-field, and (optionally) propagates the integrated flux value
        from the twilight-sky cube.

        <!-- [For special cases, users can choose to combine all input files at the
        image level, so that the pixel table is only created once, for the
        combined data. This is not recommended for science data, where the
        combination should take place after correcting for atmospheric effects,
        before the creation of the final cube.] -->

        The reduced image is then saved (if --saveimage=true).

        The input calibrations geometry table, trace table, and wavelength
        calibration table are used to assign 3D coordinates to each CCD-based
        pixel, thereby creating a pixel table for each exposure.

        If --skylines contains one or more wavelengths for (bright and isolated)
        sky emission lines, these lines are used to correct the wavelength
        calibration using an offset.

        The data is then cut to a useful wavelength range (if --crop=true).

        If an ILLUM exposure was given as input, it is then used to correct the
        relative illumination between all slices of one IFU. For this, the data
        of each slice is multiplied by the normalized median flux of that slice
        in the ILLUM exposure.

        As last step, the data is divided by the normalized twilight cube (if
        given), using the 3D coordinate of each pixel in the pixel table to
        interpolate the twilight correction onto the data.

        This pre-reduced pixel table for each exposure is then saved to disk.
      </description>
      <author>Peter Weilbacher</author>
      <email>pweilbacher@aip.de</email>
    </plugininfo>
    <parameters>
      <parameter name="nifu" type="int">
        <description>IFU to handle. If set to 0, all IFUs are processed serially. If set to -1, all IFUs are processed in parallel.</description>
        <range min="-1" max="24"/>
        <default>0</default>
      </parameter>
      <parameter name="overscan" type="string">
        <description>If this is "none", stop when detecting discrepant overscan levels (see ovscsigma), for "offset" it assumes that the mean overscan level represents the real offset in the bias levels of the exposures involved, and adjusts the data accordingly; for "vpoly", a polynomial is fit to the vertical overscan and subtracted from the whole quadrant.</description>
        <default>vpoly</default><!-- no enum here because we want to allow carrying parameters! -->
      </parameter>
      <parameter name="ovscreject" type="string">
        <description>This influences how values are rejected when computing overscan statistics. Either no rejection at all ("none"), rejection using the DCR algorithm ("dcr"), or rejection using an iterative constant fit ("fit").</description>
        <default>dcr</default><!-- no enum here because we want to allow carrying parameters! -->
      </parameter>
      <parameter name="ovscsigma" type="double">
        <description>If the deviation of mean overscan levels between a raw input image and the reference image is higher than |ovscsigma x stdev|, stop the processing. If overscan="vpoly", this is used as sigma rejection level for the iterative polynomial fit (the level comparison is then done afterwards with |100 x stdev| to guard against incompatible settings). Has no effect for overscan="offset".</description>
        <default>30.</default>
      </parameter>
      <parameter name="ovscignore" type="int">
        <description>The number of pixels of the overscan adjacent to the data section of the CCD that are ignored when computing statistics or fits.</description>
        <default>3</default>
      </parameter>
     <parameter name="crop" type="boolean">
        <description>Automatically crop the output pixel tables in wavelength depending on the expected useful wavelength range of the active instrument mode (4750-9350 Angstrom for nominal mode and NFM, 4700-9350 Angstrom for nominal AO mode, and 4600-9350 Angstrom for the extended modes).</description>
        <default>true</default>
      </parameter>
      <parameter name="cr" type="string">
        <description>Type of cosmic ray cleaning to use (for quick-look data processing).</description>
        <enumeration>
          <value>none</value>
          <value>dcr</value>
        </enumeration>
        <default>none</default>
      </parameter>
      <parameter name="xbox" type="int">
        <description>Search box size in x. Only used if cr=dcr.</description>
        <default>15</default>
      </parameter>
      <parameter name="ybox" type="int">
        <description>Search box size in y. Only used if cr=dcr.</description>
        <default>40</default>
      </parameter>
      <parameter name="passes" type="int">
        <description>Maximum number of cleaning passes. Only used if cr=dcr.</description>
        <default>2</default>
      </parameter>
      <parameter name="thres" type="double">
        <description>Threshold for detection gap in factors of standard deviation. Only used if cr=dcr.</description>
        <default>5.8</default>
      </parameter>
      <parameter name="combine" type="string" expertlevel="true">
        <description>Type of combination to use. Note that in most cases, science exposures cannot easily be combined on the CCD level, so this should usually be kept as "none"! This does not pay attention about the type of input data, and will combine all raw inputs!</description>
        <enumeration>
          <value>none</value>
          <value>average</value>
          <value>median</value>
          <value>minmax</value>
          <value>sigclip</value>
        </enumeration>
        <default>none</default>
      </parameter>
      <parameter name="nlow" type="int" expertlevel="true">
        <description>Number of minimum pixels to reject with minmax</description>
        <default>1</default>
      </parameter>
      <parameter name="nhigh" type="int" expertlevel="true">
        <description>Number of maximum pixels to reject with minmax</description>
        <default>1</default>
      </parameter>
      <parameter name="nkeep" type="int" expertlevel="true">
        <description>Number of pixels to keep with minmax</description>
        <default>1</default>
      </parameter>
      <parameter name="lsigma" type="double" expertlevel="true">
        <description>Low sigma for pixel rejection with sigclip</description>
        <default>3</default>
      </parameter>
      <parameter name="hsigma" type="double" expertlevel="true">
        <description>High sigma for pixel rejection with sigclip</description>
        <default>3</default>
      </parameter>
      <parameter name="scale" type="boolean" expertlevel="true">
        <description>Scale the individual images to a common exposure time before combining them.</description>
        <default>true</default>
      </parameter>
      <parameter name="saveimage" type="boolean">
        <description>Save the pre-processed CCD-based image of each input exposure before it is transformed into a pixel table.</description>
        <default>true</default>
      </parameter>
      <parameter name="skylines" type="string">
        <description>List of wavelengths of sky emission lines (in Angstrom) to use as reference for wavelength offset correction using a Gaussian fit. It can contain multiple (isolated) lines, as comma-separated list, individual shifts are then combined into one weighted average offset. Set to "none" to deactivate.</description>
        <default>5577.339,6300.304</default>
      </parameter>
      <parameter name="skyhalfwidth" type="double">
        <description>Half-width of the extraction box (in Angstrom) around each sky emission line.</description>
        <default>5.</default>
      </parameter>
      <parameter name="skybinsize" type="double">
        <description>Size of the bins (in Angstrom per pixel) for the intermediate spectrum to do the Gaussian fit to each sky emission line.</description>
        <default>0.1</default>
      </parameter>
      <parameter name="skyreject" type="string">
        <description>Sigma clipping parameters for the intermediate spectrum to do the Gaussian fit to each sky emission line. Up to three comma-separated numbers can be given, which are interpreted as high sigma-clipping limit (float), low limit (float), and number of iterations (integer), respectively.</description>
        <default>15.,15.,1</default>
      </parameter>
      <parameter name="resample" type="boolean">
        <description>Resample the input science data into 2D spectral images using all supplied calibrations for a visual check. Note that the image produced will show small wiggles even when the input calibrations are good and were applied successfully!</description>
        <default>false</default>
      </parameter>
      <parameter name="dlambda" type="double">
        <description>Wavelength step (in Angstrom per pixel) to use for resampling.</description>
        <default>1.25</default>
      </parameter>
      <parameter name="merge" type="boolean">
        <description>Merge output products from different IFUs into a common file.</description>
        <default>false</default>
      </parameter>
    </parameters>
    <frames>
      <frameset processing="standard">
        <frame tag="OBJECT" group="raw">
          <description>Raw exposure of a science target</description>
        </frame>
        <frame tag="STD" group="raw">
          <description>Raw exposure of a standard star field</description>
        </frame>
        <frame tag="SKY" group="raw">
          <description>Raw exposure of an (almost) empty sky field</description>
        </frame>
        <frame tag="ASTROMETRY" group="raw">
          <description>Raw exposure of an astrometric field</description>
        </frame>
        <frame tag="ILLUM" group="raw" max="1">
          <description>Single optional raw (attached/illumination) flat-field exposure</description>
        </frame>
        <frame tag="MASTER_BIAS" group="calib" min="1" max="1">
          <description>Master bias</description>
          <select origin="generated">
            <restrict attribute="ESO_DET_READ_CURID"/>
          </select>
        </frame>
        <frame tag="MASTER_DARK" group="calib" max="1" default="false">
          <description>Master dark</description>
          <select origin="generated">
            <restrict attribute="ESO_DET_READ_CURID"/>
          </select>
        </frame>
        <frame tag="MASTER_FLAT" group="calib" min="1" max="1">
          <description>Master flat</description>
          <select origin="generated">
            <restrict attribute="ESO_INS_MODE"/>
            <restrict attribute="ESO_DET_READ_CURID"/>
          </select>
        </frame>
        <frame tag="TRACE_TABLE" group="calib" min="1" max="1">
          <description>Trace table</description>
          <select origin="generated">
            <restrict attribute="ESO_INS_MODE"/>
            <restrict attribute="ESO_DET_READ_CURID"/>
          </select>
        </frame>
        <frame tag="WAVECAL_TABLE" group="calib" min="1" max="1">
          <description>Wavelength calibration table</description>
          <select origin="generated">
            <restrict attribute="ESO_INS_MODE"/>
            <restrict attribute="ESO_DET_READ_CURID"/>
          </select>
        </frame>
        <frame tag="GEOMETRY_TABLE" group="calib" min="1" max="1">
          <description>Relative positions of the slices in the field of view</description>
          <select origin="generated"/>
        </frame>
        <frame tag="TWILIGHT_CUBE" group="calib">
          <description>Smoothed cube of twilight sky</description>
          <select origin="generated">
            <restrict attribute="ESO_INS_MODE"/>
          </select>
        </frame>
        <frame tag="BADPIX_TABLE" group="calib">
          <description>Known bad pixels</description>
          <select origin="extern"/>
        </frame>
        <frame tag="OBJECT_RED" group="product" base="OBJECT" mode="dateobs" level="intermediate" format="MUSE_IMAGE">
          <description>Pre-processed CCD-based images for OBJECT input (if --saveimage=true)</description>
          <fitsheader>
            <property type="int">
              <name>ESO QC SCIBASIC NSATURATED</name>
              <description>Number of saturated pixels in output data</description>
            </property>
          </fitsheader>
        </frame>
        <frame tag="OBJECT_RESAMPLED" group="product" base="OBJECT" mode="dateobs" level="intermediate" format="MUSE_IMAGE">
          <description>Resampled 2D image for OBJECT input (if --resample=true)</description>
        </frame>
        <frame tag="PIXTABLE_OBJECT" group="product" base="OBJECT" mode="dateobs" level="intermediate" format="PIXEL_TABLE">
          <description>Output pixel table for OBJECT input</description>
          <fitsheader>
            <property type="int">
              <name>ESO QC SCIBASIC NSATURATED</name>
              <description>Number of saturated pixels in output data</description>
            </property>
            <property type="float">
              <name>ESO QC SCIBASIC LAMBDA SHIFT</name>
              <unit>Angstrom</unit>
              <description>Shift in wavelength applied to the data using sky emission line(s)</description>
            </property>
          </fitsheader>
        </frame>
        <frame tag="STD_RED" group="product" base="STD" mode="dateobs" level="intermediate" format="MUSE_IMAGE">
          <description>Pre-processed CCD-based images for STD input (if --saveimage=true)</description>
          <fitsheader>
            <property type="int">
              <name>ESO QC SCIBASIC NSATURATED</name>
              <description>Number of saturated pixels in output data</description>
            </property>
          </fitsheader>
        </frame>
        <frame tag="STD_RESAMPLED" group="product" base="STD" mode="dateobs" level="intermediate" format="MUSE_IMAGE">
          <description>Resampled 2D image for STD input (if --resample=true)</description>
        </frame>
        <frame tag="PIXTABLE_STD" group="product" base="STD" mode="dateobs" level="intermediate" format="PIXEL_TABLE">
          <description>Output pixel table for STD input</description>
          <fitsheader>
            <property type="int">
              <name>ESO QC SCIBASIC NSATURATED</name>
              <description>Number of saturated pixels in output data</description>
            </property>
          </fitsheader>
        </frame>
        <frame tag="SKY_RED" group="product" base="SKY" mode="dateobs" level="intermediate" format="MUSE_IMAGE">
          <description>Pre-processed CCD-based images for SKY input (if --saveimage=true)</description>
          <fitsheader>
            <property type="int">
              <name>ESO QC SCIBASIC NSATURATED</name>
              <description>Number of saturated pixels in output data</description>
            </property>
          </fitsheader>
        </frame>
        <frame tag="SKY_RESAMPLED" group="product" base="SKY" mode="dateobs" level="intermediate" format="MUSE_IMAGE">
          <description>Resampled 2D image for SKY input (if --resample=true)</description>
        </frame>
        <frame tag="PIXTABLE_SKY" group="product" base="SKY" mode="dateobs" level="intermediate" format="PIXEL_TABLE">
          <description>Output pixel table for SKY input</description>
          <fitsheader>
            <property type="int">
              <name>ESO QC SCIBASIC NSATURATED</name>
              <description>Number of saturated pixels in output data</description>
            </property>
          </fitsheader>
        </frame>
        <frame tag="ASTROMETRY_RED" group="product" base="ASTROMETRY" mode="dateobs" level="intermediate" format="MUSE_IMAGE">
          <description>Pre-processed CCD-based images for ASTROMETRY input (if --saveimage=true)</description>
          <fitsheader>
            <property type="int">
              <name>ESO QC SCIBASIC NSATURATED</name>
              <description>Number of saturated pixels in output data</description>
            </property>
          </fitsheader>
        </frame>
        <frame tag="ASTROMETRY_RESAMPLED" group="product" base="ASTROMETRY" mode="dateobs" level="intermediate" format="MUSE_IMAGE">
          <description>Resampled 2D image for ASTROMETRY input (if --resample=true)</description>
        </frame>
        <frame tag="PIXTABLE_ASTROMETRY" group="product" base="ASTROMETRY" mode="dateobs" level="intermediate" format="PIXEL_TABLE">
          <description>Output pixel table for ASTROMETRY input</description>
          <fitsheader>
            <property type="int">
              <name>ESO QC SCIBASIC NSATURATED</name>
              <description>Number of saturated pixels in output data</description>
            </property>
          </fitsheader>
        </frame>
      </frameset>
      <frameset>
        <frame tag="REDUCED" group="raw" min="1">
          <description>Reduced CCD image</description>
        </frame>
        <frame tag="TRACE_TABLE" group="calib" min="1" max="1">
          <description>Trace table</description>
          <select origin="generated">
            <restrict attribute="ESO_INS_MODE"/>
            <restrict attribute="ESO_DET_READ_CURID"/>
          </select>
        </frame>
        <frame tag="WAVECAL_TABLE" group="calib" min="1" max="1">
          <description>Wavelength calibration table</description>
          <select origin="generated">
            <restrict attribute="ESO_INS_MODE"/>
            <restrict attribute="ESO_DET_READ_CURID"/>
          </select>
        </frame>
        <frame tag="GEOMETRY_TABLE" group="calib" min="1" max="1">
          <description>Relative positions of the slices in the field of view</description>
          <select origin="generated"/>
        </frame>
        <frame tag="REDUCED_RESAMPLED" group="product" base="REDUCED" mode="dateobs" level="intermediate" format="MUSE_IMAGE">
          <description>Resampled 2D image (if --resample=true)</description>
        </frame>
        <frame tag="PIXTABLE_REDUCED" group="product" base="REDUCED" mode="dateobs" level="intermediate" format="PIXEL_TABLE">
          <description>Output pixel table</description>
        </frame>
      </frameset>
    </frames>
  </plugin>
</pluginlist>

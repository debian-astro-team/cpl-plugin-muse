/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/*
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2014 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*---------------------------------------------------------------------------*
 *                             Includes                                      *
 *---------------------------------------------------------------------------*/
#include <muse.h>
#include "muse_scipost_apply_astrometry_z.h"

/*---------------------------------------------------------------------------*
 *                             Functions code                                *
 *---------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
  @brief    Separate astrometry main computing routine
  @param    aProcessing  the processing structure
  @param    aParams      the parameters list
  @return   0 if everything is ok, -1 something went wrong

  @see muse_scipost_process_exposure()
 */
/*----------------------------------------------------------------------------*/
int
muse_scipost_apply_astrometry_compute(muse_processing *aProcessing,
                                      muse_scipost_apply_astrometry_params_t *aParams)
{
  UNUSED_ARGUMENT(aParams); /* this recipe doesn't have parameters */

  cpl_propertylist *wcs = muse_processing_load_header(aProcessing,
                                                      MUSE_TAG_ASTROMETRY_WCS);

  cpl_frameset *inframes = muse_frameset_find_tags(aProcessing->inframes,
                                                      aProcessing->intags, 0,
                                                      CPL_FALSE);
  cpl_error_code rc = CPL_ERROR_NONE;
  cpl_size iframe, nframes = cpl_frameset_get_size(inframes);
  for (iframe = 0; iframe < nframes; iframe++) {
    cpl_frame *frame = cpl_frameset_get_position(inframes, iframe);
    const char *fn = cpl_frame_get_filename(frame);
    muse_pixtable *pixtable = muse_pixtable_load_restricted_wavelength(fn,
                                                                       aParams->lambdamin,
                                                                       aParams->lambdamax);
    if (pixtable == NULL) {
      cpl_msg_error(__func__, "NULL pixel table for %s", fn);
      rc = CPL_ERROR_NULL_INPUT;
      break;
    }
    /* erase pre-existing QC parameters */
    cpl_propertylist_erase_regexp(pixtable->header, "ESO QC ", 0);
    rc = muse_wcs_project_tan(pixtable, wcs);
    if (rc != CPL_ERROR_NONE) {
      cpl_msg_error(__func__, "while muse_wcs_project_tan(%s)",
                    cpl_frame_get_filename(frame));
      muse_pixtable_delete(pixtable);
      break;
    }
    muse_processing_save_table(aProcessing, 0, pixtable, NULL,
                               MUSE_TAG_PIXTABLE_REDUCED,
                               MUSE_TABLE_TYPE_PIXTABLE);
    muse_pixtable_delete(pixtable);
  } /* for iframe */
  cpl_frameset_delete(inframes);
  cpl_propertylist_delete(wcs);

  return rc;
} /* muse_scipost_apply_astrometry_compute() */

/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/* 
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* This file was automatically generated */

#ifndef MUSE_SCIPOST_Z_H
#define MUSE_SCIPOST_Z_H

/*----------------------------------------------------------------------------*
 *                              Includes                                      *
 *----------------------------------------------------------------------------*/
#include <muse.h>
#include <muse_instrument.h>

/*----------------------------------------------------------------------------*
 *                          Special variable types                            *
 *----------------------------------------------------------------------------*/

/** @addtogroup recipe_muse_scipost */
/**@{*/

/*----------------------------------------------------------------------------*/
/**
  @brief   Structure to hold the parameters of the muse_scipost recipe.

  This structure contains the parameters for the recipe that may be set on the
  command line, in the configuration, or through the environment.
 */
/*----------------------------------------------------------------------------*/
typedef struct muse_scipost_params_s {
  /** @brief   Select output product(s) to save. Can contain one or more of "cube", "autocal", "skymodel", "individual", "positioned", "combined", and "stacked". If several options are given, they have to be comma-separated.
          ("cube": output cube and associated images, if this is not given, no final resampling is done at all --
          "autocal": up to two additional output products related to the slice autocalibration --
          "raman": up to four additional output products about the Raman light distribution for AO observations --
          "skymodel": up to four additional output products about the effectively used sky that was subtracted with the "model" method --
          "individual": fully reduced pixel table for each individual exposure --
          "positioned": fully reduced and positioned pixel table for each individual exposure, the difference to "individual" is that here, the output pixel tables have coordinates in RA and DEC, and the optional offsets were applied; this is only useful, if both the relative exposure weighting and the final resampling are to be done externally --
          "combined": fully reduced and combined pixel table for the full set of exposures, the difference to "positioned" is that all pixel tables are combined into one, with an added weight column; this is useful, if only the final resampling step is to be done separately --
          "stacked": an additional output file in form of a 2D column-stacked image, i.e. x direction is pseudo-spatial, y direction is wavelength.)
         */
  const char * save;

  /** @brief   The resampling technique to use for the final output cube. */
  int resample;
  /** @brief   The resampling technique to use for the final output cube. (as string) */
  const char *resample_s;

  /** @brief   Horizontal step size for resampling (in arcsec or pixel). The following defaults are taken when this value is set to 0.0: 0.2'' for WFM, 0.025'' for NFM, 1.0 if data is in pixel units. */
  double dx;

  /** @brief   Vertical step size for resampling (in arcsec or pixel). The following defaults are taken when this value is set to 0.0: 0.2'' for WFM, 0.025'' for NFM, 1.0 if data is in pixel units. */
  double dy;

  /** @brief   Wavelength step size (in Angstrom). Natural instrument sampling is used, if this is 0.0 */
  double dlambda;

  /** @brief   Type of statistics used for detection of cosmic rays during final resampling. "iraf" uses the variance information, "mean" uses standard (mean/stdev) statistics, "median" uses median and the median median of the absolute median deviation. */
  int crtype;
  /** @brief   Type of statistics used for detection of cosmic rays during final resampling. "iraf" uses the variance information, "mean" uses standard (mean/stdev) statistics, "median" uses median and the median median of the absolute median deviation. (as string) */
  const char *crtype_s;

  /** @brief   Sigma rejection factor to use for cosmic ray rejection during final resampling. A zero or negative value switches cosmic ray rejection off. */
  double crsigma;

  /** @brief   Critical radius for the "renka" resampling method. */
  double rc;

  /** @brief   Pixel down-scaling factor for the "drizzle" resampling method. Up to three, comma-separated, floating-point values can be given. If only one value is given, it applies to all dimensions, two values are interpreted as spatial and spectral direction, respectively, while three are taken as horizontal, vertical, and spectral. */
  const char * pixfrac;

  /** @brief   Number of adjacent pixels to take into account during resampling in all three directions (loop distance); this affects all resampling methods except "nearest". */
  int ld;

  /** @brief   Type of output file format, "Cube" is a standard FITS cube with NAXIS=3 and multiple extensions (for data and variance). The extended "x" formats include the reconstructed image(s) in FITS image extensions within the same file. "sdpCube" does some extra calculations to create FITS keywords for the ESO Science Data Products. */
  int format;
  /** @brief   Type of output file format, "Cube" is a standard FITS cube with NAXIS=3 and multiple extensions (for data and variance). The extended "x" formats include the reconstructed image(s) in FITS image extensions within the same file. "sdpCube" does some extra calculations to create FITS keywords for the ESO Science Data Products. (as string) */
  const char *format_s;

  /** @brief   Type of weighting scheme to use when combining multiple exposures. "exptime" just uses the exposure time to weight the exposures, "fwhm" uses the best available seeing information from the headers as well, "none" preserves an existing weight column in the input pixel tables without changes. */
  int weight;
  /** @brief   Type of weighting scheme to use when combining multiple exposures. "exptime" just uses the exposure time to weight the exposures, "fwhm" uses the best available seeing information from the headers as well, "none" preserves an existing weight column in the input pixel tables without changes. (as string) */
  const char *weight_s;

  /** @brief   The filter name(s) to be used for the output field-of-view image. Each name has to correspond to an EXTNAME in an extension of the FILTER_LIST file. If an unsupported filter name is given, creation of the respective image is omitted. If multiple filter names are given, they have to be comma separated. */
  const char * filter;

  /** @brief   The type of autocalibration to use. "none" switches it off, "deepfield" uses the revised MPDAF method that can be used for the reduction of mostly empty "Deep Fields", "user" searches for a user-provided table with autocalibration factors. */
  int autocalib;
  /** @brief   The type of autocalibration to use. "none" switches it off, "deepfield" uses the revised MPDAF method that can be used for the reduction of mostly empty "Deep Fields", "user" searches for a user-provided table with autocalibration factors. (as string) */
  const char *autocalib_s;

  /** @brief   Wavelength range around Raman lines [Angstrom]. */
  double raman_width;

  /** @brief   The method used to subtract the sky background (spectrum).
          Option "model" should work in all kinds of science fields: it uses a global sky spectrum model with a local LSF.
          "model" uses fluxes indicated in the SKY_LINES file as starting estimates, but re-fits them on the global sky spectrum created from the science exposure.
          If SKY_CONTINUUM is given, it is directly subtracted, otherwise it is created from the sky region of the science exposure.
          Option "subtract-model" uses the input SKY_LINES and SKY_CONTINUUM, subtracting them directly without re-fitting the fluxes, but still makes use of the local LSF, hence LSF_PROFILE is required.
          The inputs LSF_PROFILE and SKY_LINES are necessary for these two model-based methods; SKY_CONTINUUM is required for "subtract-model" and optional for "model"; SKY_MASK is optional for "model".
          Finally, option "simple" creates a sky spectrum from the science data, and directly subtracts it, without taking the LSF into account (LSF_PROFILE and input SKY files are ignored). It works on data that was not flux calibrated.
         */
  int skymethod;
  /** @brief   The method used to subtract the sky background (spectrum).
          Option "model" should work in all kinds of science fields: it uses a global sky spectrum model with a local LSF.
          "model" uses fluxes indicated in the SKY_LINES file as starting estimates, but re-fits them on the global sky spectrum created from the science exposure.
          If SKY_CONTINUUM is given, it is directly subtracted, otherwise it is created from the sky region of the science exposure.
          Option "subtract-model" uses the input SKY_LINES and SKY_CONTINUUM, subtracting them directly without re-fitting the fluxes, but still makes use of the local LSF, hence LSF_PROFILE is required.
          The inputs LSF_PROFILE and SKY_LINES are necessary for these two model-based methods; SKY_CONTINUUM is required for "subtract-model" and optional for "model"; SKY_MASK is optional for "model".
          Finally, option "simple" creates a sky spectrum from the science data, and directly subtracts it, without taking the LSF into account (LSF_PROFILE and input SKY files are ignored). It works on data that was not flux calibrated.
         (as string) */
  const char *skymethod_s;

  /** @brief   Cut off the data below this wavelength after loading the pixel table(s). */
  double lambdamin;

  /** @brief   Cut off the data above this wavelength after loading the pixel table(s). */
  double lambdamax;

  /** @brief   Reference wavelength used for correction of differential atmospheric refraction. The R-band (peak wavelength ~7000 Angstrom) that is usually used for guiding, is close to the central wavelength of MUSE, so a value of 7000.0 Angstrom should be used if nothing else is known. A value less than zero switches DAR correction off. */
  double lambdaref;

  /** @brief   Carry out a check of the theoretical DAR correction using source centroiding. If "correct" it will also apply an empirical correction. */
  int darcheck;
  /** @brief   Carry out a check of the theoretical DAR correction using source centroiding. If "correct" it will also apply an empirical correction. (as string) */
  const char *darcheck_s;

  /** @brief   Fraction of the image (without the ignored part) to be considered as sky. If an input sky mask is provided, the fraction is applied to the regions within the mask. If the whole sky mask should be used, set this parameter to 1. */
  double skymodel_fraction;

  /** @brief   Fraction of the image to be ignored. If an input sky mask is provided, the fraction is applied to the regions within the mask. If the whole sky mask should be used, set this parameter to 0. */
  double skymodel_ignore;

  /** @brief   Spectral sampling of the sky spectrum [Angstrom]. */
  double skymodel_sampling;

  /** @brief   Spectral sampling of the continuum spectrum [Angstrom]. */
  double skymodel_csampling;

  /** @brief   Sigma level clipping for cube-based and spectrum-based CR rejection when creating the sky spectrum. This has to be a string of two comma-separated floating-point numbers. The first value gives the sigma-level rejection for cube-based CR rejection (using "median"), the second value the sigma-level for spectrum-based CR cleaning. Both can be switched off, by passing zero or a negative value. */
  const char * sky_crsigma;

  /** @brief   Correct the radial velocity of the telescope with reference to either the barycenter of the Solar System (bary), the center of the Sun (helio), or to the center of the Earth (geo). */
  int rvcorr;
  /** @brief   Correct the radial velocity of the telescope with reference to either the barycenter of the Solar System (bary), the center of the Sun (helio), or to the center of the Earth (geo). (as string) */
  const char *rvcorr_s;

  /** @brief   If false, skip any astrometric calibration, even if one was passed in the input set of files. This causes creation of an output cube with a linear WCS and may result in errors. If you want to use a sensible default, leave this true but do not pass an ASTROMETRY_WCS. */
  int astrometry;

  char __dummy__; /* quieten compiler warning about possibly empty struct */
} muse_scipost_params_t;

#define MUSE_SCIPOST_PARAM_RESAMPLE_NEAREST 1
#define MUSE_SCIPOST_PARAM_RESAMPLE_LINEAR 2
#define MUSE_SCIPOST_PARAM_RESAMPLE_QUADRATIC 3
#define MUSE_SCIPOST_PARAM_RESAMPLE_RENKA 4
#define MUSE_SCIPOST_PARAM_RESAMPLE_DRIZZLE 5
#define MUSE_SCIPOST_PARAM_RESAMPLE_LANCZOS 6
#define MUSE_SCIPOST_PARAM_RESAMPLE_INVALID_VALUE -1
#define MUSE_SCIPOST_PARAM_CRTYPE_IRAF 1
#define MUSE_SCIPOST_PARAM_CRTYPE_MEAN 2
#define MUSE_SCIPOST_PARAM_CRTYPE_MEDIAN 3
#define MUSE_SCIPOST_PARAM_CRTYPE_INVALID_VALUE -1
#define MUSE_SCIPOST_PARAM_FORMAT_CUBE 1
#define MUSE_SCIPOST_PARAM_FORMAT_EURO3D 2
#define MUSE_SCIPOST_PARAM_FORMAT_XCUBE 3
#define MUSE_SCIPOST_PARAM_FORMAT_XEURO3D 4
#define MUSE_SCIPOST_PARAM_FORMAT_SDPCUBE 5
#define MUSE_SCIPOST_PARAM_FORMAT_INVALID_VALUE -1
#define MUSE_SCIPOST_PARAM_WEIGHT_EXPTIME 1
#define MUSE_SCIPOST_PARAM_WEIGHT_FWHM 2
#define MUSE_SCIPOST_PARAM_WEIGHT_NONE 3
#define MUSE_SCIPOST_PARAM_WEIGHT_INVALID_VALUE -1
#define MUSE_SCIPOST_PARAM_AUTOCALIB_NONE 1
#define MUSE_SCIPOST_PARAM_AUTOCALIB_DEEPFIELD 2
#define MUSE_SCIPOST_PARAM_AUTOCALIB_USER 3
#define MUSE_SCIPOST_PARAM_AUTOCALIB_INVALID_VALUE -1
#define MUSE_SCIPOST_PARAM_SKYMETHOD_NONE 1
#define MUSE_SCIPOST_PARAM_SKYMETHOD_SUBTRACT_MODEL 2
#define MUSE_SCIPOST_PARAM_SKYMETHOD_MODEL 3
#define MUSE_SCIPOST_PARAM_SKYMETHOD_SIMPLE 4
#define MUSE_SCIPOST_PARAM_SKYMETHOD_INVALID_VALUE -1
#define MUSE_SCIPOST_PARAM_DARCHECK_NONE 1
#define MUSE_SCIPOST_PARAM_DARCHECK_CHECK 2
#define MUSE_SCIPOST_PARAM_DARCHECK_CORRECT 3
#define MUSE_SCIPOST_PARAM_DARCHECK_INVALID_VALUE -1
#define MUSE_SCIPOST_PARAM_RVCORR_BARY 1
#define MUSE_SCIPOST_PARAM_RVCORR_HELIO 2
#define MUSE_SCIPOST_PARAM_RVCORR_GEO 3
#define MUSE_SCIPOST_PARAM_RVCORR_NONE 4
#define MUSE_SCIPOST_PARAM_RVCORR_INVALID_VALUE -1

/**@}*/

/*----------------------------------------------------------------------------*
 *                           Function prototypes                              *
 *----------------------------------------------------------------------------*/
int muse_scipost_compute(muse_processing *, muse_scipost_params_t *);

#endif /* MUSE_SCIPOST_Z_H */

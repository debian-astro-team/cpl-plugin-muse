/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/* 
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* This file was automatically generated */

#ifndef MUSE_EXP_ALIGN_Z_H
#define MUSE_EXP_ALIGN_Z_H

/*----------------------------------------------------------------------------*
 *                              Includes                                      *
 *----------------------------------------------------------------------------*/
#include <muse.h>
#include <muse_instrument.h>

/*----------------------------------------------------------------------------*
 *                          Special variable types                            *
 *----------------------------------------------------------------------------*/

/** @addtogroup recipe_muse_exp_align */
/**@{*/

/*----------------------------------------------------------------------------*/
/**
  @brief   Structure to hold the parameters of the muse_exp_align recipe.

  This structure contains the parameters for the recipe that may be set on the
  command line, in the configuration, or through the environment.
 */
/*----------------------------------------------------------------------------*/
typedef struct muse_exp_align_params_s {
  /** @brief   
          Search radius (in arcsec) for each iteration of the offset computation.
         */
  const char * rsearch;

  /** @brief   
          Number of bins to use for 2D histogram on the first iteration of the
          offset computation.
         */
  int nbins;

  /** @brief   Use weighting. */
  int weight;

  /** @brief   FWHM in pixels of the convolution filter. */
  double fwhm;

  /** @brief   
          Initial intensity threshold for detecting point sources. If the value
          is negative or zero the threshold is taken as sigma above median
          background MAD. If it is larger than zero the threshold is taken as
          absolute background level.
         */
  double threshold;

  /** @brief   
          Fraction of the image to be ignored.
         */
  double bkgignore;

  /** @brief   
          Fraction of the image (without the ignored part) to be considered as
          background.
         */
  double bkgfraction;

  /** @brief   Increment/decrement of the threshold value in subsequent iterations. */
  double step;

  /** @brief   Maximum number of iterations used for detecting sources. */
  int iterations;

  /** @brief   Minimum number of sources which must be found. */
  int srcmin;

  /** @brief   Maximum number of sources which may be found. */
  int srcmax;

  /** @brief   Lower limit of the allowed point-source roundness. */
  double roundmin;

  /** @brief   Upper limit of the allowed point-source roundness. */
  double roundmax;

  /** @brief   Lower limit of the allowed point-source sharpness. */
  double sharpmin;

  /** @brief   Upper limit of the allowed point-source sharpness. */
  double sharpmax;

  /** @brief   
          Enables the creation of a simple exposure map for the combined
          field-of-view.
         */
  int expmap;

  /** @brief   
          Minimum allowed distance of a source to the closest bad pixel in pixel.
          Detected sources which are closer to a bad pixel are not taken into
          account when computing the field offsets. This option has no effect
          if the source positions are taken from input catalogs.
         */
  double bpixdistance;

  /** @brief   
          Overrides the source detection step. If this is enabled and source
          catalogs are present in the input data set, the source positions used
          to calculate the field offsets are read from the input catalogs. If no
          catalogs are available as input data the source positions are detected
          on the input images.
         */
  int override_detection;

  char __dummy__; /* quieten compiler warning about possibly empty struct */
} muse_exp_align_params_t;


/**@}*/

/*----------------------------------------------------------------------------*
 *                           Function prototypes                              *
 *----------------------------------------------------------------------------*/
int muse_exp_align_compute(muse_processing *, muse_exp_align_params_t *);

#endif /* MUSE_EXP_ALIGN_Z_H */

/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/* 
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* This file was automatically generated */

#ifndef MUSE_SCIPOST_SUBTRACT_SKY_Z_H
#define MUSE_SCIPOST_SUBTRACT_SKY_Z_H

/*----------------------------------------------------------------------------*
 *                              Includes                                      *
 *----------------------------------------------------------------------------*/
#include <muse.h>
#include <muse_instrument.h>

/*----------------------------------------------------------------------------*
 *                          Special variable types                            *
 *----------------------------------------------------------------------------*/

/** @addtogroup recipe_muse_scipost_subtract_sky */
/**@{*/

/*----------------------------------------------------------------------------*/
/**
  @brief   Structure to hold the parameters of the muse_scipost_subtract_sky recipe.

  This structure contains the parameters for the recipe that may be set on the
  command line, in the configuration, or through the environment.
 */
/*----------------------------------------------------------------------------*/
typedef struct muse_scipost_subtract_sky_params_s {
  /** @brief   Cut off the data below this wavelength after loading the pixel table(s). */
  double lambdamin;

  /** @brief   Cut off the data above this wavelength after loading the pixel table(s). */
  double lambdamax;

  /** @brief   If specified, write an additional column containing the
          original data to the pixel table. */
  const char * orig;

  /** @brief   Reference flat field flux, obtained by sky exposure. This parameter is needed to scale the data of each pixel table if more than one pixel table was used to determine the sky. By default, it is taken from the parameter ESO DRS MUSE FLAT FLUX SKY of the first pixel table. */
  double flux_sky;

  /** @brief   Reference flat field flux, obtained by lamp exposure. This parameter is needed to scale the data of each pixel table if more than one pixel table was used to determine the sky. By default, it is taken from the parameter ESO DRS MUSE FLAT FLUX LAMP of the first pixel table. */
  double flux_lamp;

  char __dummy__; /* quieten compiler warning about possibly empty struct */
} muse_scipost_subtract_sky_params_t;


/**@}*/

/*----------------------------------------------------------------------------*
 *                           Function prototypes                              *
 *----------------------------------------------------------------------------*/
int muse_scipost_subtract_sky_compute(muse_processing *, muse_scipost_subtract_sky_params_t *);

#endif /* MUSE_SCIPOST_SUBTRACT_SKY_Z_H */

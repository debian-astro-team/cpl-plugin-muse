/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/* 
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* This file was automatically generated */

#ifndef MUSE_SCIBASIC_Z_H
#define MUSE_SCIBASIC_Z_H

/*----------------------------------------------------------------------------*
 *                              Includes                                      *
 *----------------------------------------------------------------------------*/
#include <muse.h>
#include <muse_instrument.h>

/*----------------------------------------------------------------------------*
 *                          Special variable types                            *
 *----------------------------------------------------------------------------*/

/** @addtogroup recipe_muse_scibasic */
/**@{*/

/*----------------------------------------------------------------------------*/
/**
  @brief   Structure to hold the parameters of the muse_scibasic recipe.

  This structure contains the parameters for the recipe that may be set on the
  command line, in the configuration, or through the environment.
 */
/*----------------------------------------------------------------------------*/
typedef struct muse_scibasic_params_s {
  /** @brief   IFU to handle. If set to 0, all IFUs are processed serially. If set to -1, all IFUs are processed in parallel. */
  int nifu;

  /** @brief   If this is "none", stop when detecting discrepant overscan levels (see ovscsigma), for "offset" it assumes that the mean overscan level represents the real offset in the bias levels of the exposures involved, and adjusts the data accordingly; for "vpoly", a polynomial is fit to the vertical overscan and subtracted from the whole quadrant. */
  const char * overscan;

  /** @brief   This influences how values are rejected when computing overscan statistics. Either no rejection at all ("none"), rejection using the DCR algorithm ("dcr"), or rejection using an iterative constant fit ("fit"). */
  const char * ovscreject;

  /** @brief   If the deviation of mean overscan levels between a raw input image and the reference image is higher than |ovscsigma x stdev|, stop the processing. If overscan="vpoly", this is used as sigma rejection level for the iterative polynomial fit (the level comparison is then done afterwards with |100 x stdev| to guard against incompatible settings). Has no effect for overscan="offset". */
  double ovscsigma;

  /** @brief   The number of pixels of the overscan adjacent to the data section of the CCD that are ignored when computing statistics or fits. */
  int ovscignore;

  /** @brief   Automatically crop the output pixel tables in wavelength depending on the expected useful wavelength range of the active instrument mode (4750-9350 Angstrom for nominal mode and NFM, 4700-9350 Angstrom for nominal AO mode, and 4600-9350 Angstrom for the extended modes). */
  int crop;

  /** @brief   Type of cosmic ray cleaning to use (for quick-look data processing). */
  int cr;
  /** @brief   Type of cosmic ray cleaning to use (for quick-look data processing). (as string) */
  const char *cr_s;

  /** @brief   Search box size in x. Only used if cr=dcr. */
  int xbox;

  /** @brief   Search box size in y. Only used if cr=dcr. */
  int ybox;

  /** @brief   Maximum number of cleaning passes. Only used if cr=dcr. */
  int passes;

  /** @brief   Threshold for detection gap in factors of standard deviation. Only used if cr=dcr. */
  double thres;

  /** @brief   Type of combination to use. Note that in most cases, science exposures cannot easily be combined on the CCD level, so this should usually be kept as "none"! This does not pay attention about the type of input data, and will combine all raw inputs! */
  int combine;
  /** @brief   Type of combination to use. Note that in most cases, science exposures cannot easily be combined on the CCD level, so this should usually be kept as "none"! This does not pay attention about the type of input data, and will combine all raw inputs! (as string) */
  const char *combine_s;

  /** @brief   Number of minimum pixels to reject with minmax */
  int nlow;

  /** @brief   Number of maximum pixels to reject with minmax */
  int nhigh;

  /** @brief   Number of pixels to keep with minmax */
  int nkeep;

  /** @brief   Low sigma for pixel rejection with sigclip */
  double lsigma;

  /** @brief   High sigma for pixel rejection with sigclip */
  double hsigma;

  /** @brief   Scale the individual images to a common exposure time before combining them. */
  int scale;

  /** @brief   Save the pre-processed CCD-based image of each input exposure before it is transformed into a pixel table. */
  int saveimage;

  /** @brief   List of wavelengths of sky emission lines (in Angstrom) to use as reference for wavelength offset correction using a Gaussian fit. It can contain multiple (isolated) lines, as comma-separated list, individual shifts are then combined into one weighted average offset. Set to "none" to deactivate. */
  const char * skylines;

  /** @brief   Half-width of the extraction box (in Angstrom) around each sky emission line. */
  double skyhalfwidth;

  /** @brief   Size of the bins (in Angstrom per pixel) for the intermediate spectrum to do the Gaussian fit to each sky emission line. */
  double skybinsize;

  /** @brief   Sigma clipping parameters for the intermediate spectrum to do the Gaussian fit to each sky emission line. Up to three comma-separated numbers can be given, which are interpreted as high sigma-clipping limit (float), low limit (float), and number of iterations (integer), respectively. */
  const char * skyreject;

  /** @brief   Resample the input science data into 2D spectral images using all supplied calibrations for a visual check. Note that the image produced will show small wiggles even when the input calibrations are good and were applied successfully! */
  int resample;

  /** @brief   Wavelength step (in Angstrom per pixel) to use for resampling. */
  double dlambda;

  /** @brief   Merge output products from different IFUs into a common file. */
  int merge;

  char __dummy__; /* quieten compiler warning about possibly empty struct */
} muse_scibasic_params_t;

#define MUSE_SCIBASIC_PARAM_CR_NONE 1
#define MUSE_SCIBASIC_PARAM_CR_DCR 2
#define MUSE_SCIBASIC_PARAM_CR_INVALID_VALUE -1
#define MUSE_SCIBASIC_PARAM_COMBINE_NONE 1
#define MUSE_SCIBASIC_PARAM_COMBINE_AVERAGE 2
#define MUSE_SCIBASIC_PARAM_COMBINE_MEDIAN 3
#define MUSE_SCIBASIC_PARAM_COMBINE_MINMAX 4
#define MUSE_SCIBASIC_PARAM_COMBINE_SIGCLIP 5
#define MUSE_SCIBASIC_PARAM_COMBINE_INVALID_VALUE -1

/**@}*/

/*----------------------------------------------------------------------------*
 *                           Function prototypes                              *
 *----------------------------------------------------------------------------*/
int muse_scibasic_compute(muse_processing *, muse_scibasic_params_t *);

#endif /* MUSE_SCIBASIC_Z_H */

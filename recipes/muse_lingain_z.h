/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/* 
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* This file was automatically generated */

#ifndef MUSE_LINGAIN_Z_H
#define MUSE_LINGAIN_Z_H

/*----------------------------------------------------------------------------*
 *                              Includes                                      *
 *----------------------------------------------------------------------------*/
#include <muse.h>
#include <muse_instrument.h>

/*----------------------------------------------------------------------------*
 *                          Special variable types                            *
 *----------------------------------------------------------------------------*/

/** @addtogroup recipe_muse_lingain */
/**@{*/

/*----------------------------------------------------------------------------*/
/**
  @brief   Structure to hold the parameters of the muse_lingain recipe.

  This structure contains the parameters for the recipe that may be set on the
  command line, in the configuration, or through the environment.
 */
/*----------------------------------------------------------------------------*/
typedef struct muse_lingain_params_s {
  /** @brief   IFU to handle. If set to 0, all IFUs are processed serially. If set to -1, all IFUs are processed in parallel. */
  int nifu;

  /** @brief   
          Size of windows along the traces of the slices.
         */
  int ybox;

  /** @brief   
          Extra offset from tracing edge.
         */
  int xgap;

  /** @brief   
          Extra offset from the detector edge used for the selection of slices.
         */
  int xborder;

  /** @brief   
          Order of the polynomial used to fit the non-linearity residuals.
         */
  int order;

  /** @brief   
          Exposure time offset in seconds to apply to linearity flat fields.
         */
  double toffset;

  /** @brief   
          Tolerance value for the overall flux consistency check of a pair of flat fields. The value is the maximum relative offset.
         */
  double fluxtol;

  /** @brief   Sigma value used for signal value clipping. */
  double sigma;

  /** @brief   Minimum signal value in log(ADU) used for the gain analysis and the non-linearity polynomial model. */
  double signalmin;

  /** @brief   Maximum signal value in log(ADU) used for the gain analysis and the non-linearity polynomial model. */
  double signalmax;

  /** @brief   Size of a signal bin in log10(ADU) used for the gain analysis and the non-linearity polynomial model. */
  double signalbin;

  /** @brief   Minimum signal value [ADU] used for fitting the gain relation. */
  double gainlimit;

  /** @brief   Sigma value for gain value clipping. */
  double gainsigma;

  /** @brief   Minimum signal value in log(counts) to consider for the non-linearity analysis. */
  double ctsmin;

  /** @brief   Maximum signal value in log(counts) to consider for the non-linearity analysis. */
  double ctsmax;

  /** @brief   Size of a signal bin in log10(counts) used for the non-linearity analysis. */
  double ctsbin;

  /** @brief   Lower limit of desired linear range in log10(counts). */
  double linearmin;

  /** @brief   Upper limit of desired linear range in log10(counts). */
  double linearmax;

  /** @brief   Merge output products from different IFUs into a common file. */
  int merge;

  char __dummy__; /* quieten compiler warning about possibly empty struct */
} muse_lingain_params_t;


/**@}*/

/*----------------------------------------------------------------------------*
 *                           Function prototypes                              *
 *----------------------------------------------------------------------------*/
int muse_lingain_compute(muse_processing *, muse_lingain_params_t *);

#endif /* MUSE_LINGAIN_Z_H */

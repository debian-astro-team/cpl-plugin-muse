/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/* 
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* This file was automatically generated */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*----------------------------------------------------------------------------*
 *                              Includes                                      *
 *----------------------------------------------------------------------------*/
#include <string.h> /* strcmp(), strstr() */
#include <strings.h> /* strcasecmp() */
#include <cpl.h>

#include "muse_create_sky_z.h" /* in turn includes muse.h */

/*----------------------------------------------------------------------------*/
/**
  @defgroup recipe_muse_create_sky         Recipe muse_create_sky: Create night sky model from selected pixels of an exposure of empty sky.
  @author Ole Streicher
  
        This recipe creates the continuum and the atmospheric transition line
        spectra of the night sky from the data in a pixel table(s) belonging to
        one exposure of (mostly) empty sky.
      
 */
/*----------------------------------------------------------------------------*/
/**@{*/

/*----------------------------------------------------------------------------*
 *                         Static variables                                   *
 *----------------------------------------------------------------------------*/
static const char *muse_create_sky_help =
  "This recipe creates the continuum and the atmospheric transition line spectra of the night sky from the data in a pixel table(s) belonging to one exposure of (mostly) empty sky.";

static const char *muse_create_sky_help_esorex =
  "\n\nInput frames for raw frame tag \"PIXTABLE_SKY\":\n"
  "\n Frame tag            Type Req #Fr Description"
  "\n -------------------- ---- --- --- ------------"
  "\n PIXTABLE_SKY         raw   Y      Input pixel table. If the pixel table is not already flux calibrated, the corresponding flux calibration frames should be given as well."
  "\n EXTINCT_TABLE        calib Y    1 Atmospheric extinction table"
  "\n STD_RESPONSE         calib Y    1 Response curve as derived from standard star(s)"
  "\n STD_TELLURIC         calib .    1 Telluric absorption as derived from standard star(s)"
  "\n SKY_LINES            calib Y    1 List of OH transitions and other sky lines"
  "\n SKY_CONTINUUM        calib .    1 Sky continuum to use"
  "\n LSF_PROFILE          calib Y    1 Slice specific LSF parameters cubes"
  "\n SKY_MASK             calib .    1 Sky mask to use"
  "\n\nProduct frames for raw frame tag \"PIXTABLE_SKY\":\n"
  "\n Frame tag            Level    Description"
  "\n -------------------- -------- ------------"
  "\n SKY_MASK             intermed Created sky mask"
  "\n SKY_IMAGE            intermed Whitelight image used to create the sky mask"
  "\n SKY_SPECTRUM         intermed Sky spectrum within the sky mask"
  "\n SKY_LINES            final    Estimated sky line flux table"
  "\n SKY_CONTINUUM        final    Estimated continuum flux spectrum";

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Create the recipe config for this plugin.

  @remark The recipe config is used to check for the tags as well as to create
          the documentation of this plugin.
 */
/*----------------------------------------------------------------------------*/
static cpl_recipeconfig *
muse_create_sky_new_recipeconfig(void)
{
  cpl_recipeconfig *recipeconfig = cpl_recipeconfig_new();
      
  cpl_recipeconfig_set_tag(recipeconfig, "PIXTABLE_SKY", 1, -1);
  cpl_recipeconfig_set_input(recipeconfig, "PIXTABLE_SKY", "EXTINCT_TABLE", 1, 1);
  cpl_recipeconfig_set_input(recipeconfig, "PIXTABLE_SKY", "STD_RESPONSE", 1, 1);
  cpl_recipeconfig_set_input(recipeconfig, "PIXTABLE_SKY", "STD_TELLURIC", -1, 1);
  cpl_recipeconfig_set_input(recipeconfig, "PIXTABLE_SKY", "SKY_LINES", 1, 1);
  cpl_recipeconfig_set_input(recipeconfig, "PIXTABLE_SKY", "SKY_CONTINUUM", 0, 1);
  cpl_recipeconfig_set_input(recipeconfig, "PIXTABLE_SKY", "LSF_PROFILE", 1, 1);
  cpl_recipeconfig_set_input(recipeconfig, "PIXTABLE_SKY", "SKY_MASK", 0, 1);
  cpl_recipeconfig_set_output(recipeconfig, "PIXTABLE_SKY", "SKY_MASK");
  cpl_recipeconfig_set_output(recipeconfig, "PIXTABLE_SKY", "SKY_IMAGE");
  cpl_recipeconfig_set_output(recipeconfig, "PIXTABLE_SKY", "SKY_SPECTRUM");
  cpl_recipeconfig_set_output(recipeconfig, "PIXTABLE_SKY", "SKY_LINES");
  cpl_recipeconfig_set_output(recipeconfig, "PIXTABLE_SKY", "SKY_CONTINUUM");
    
  return recipeconfig;
} /* muse_create_sky_new_recipeconfig() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Return a new header that shall be filled on output.
  @param   aFrametag   tag of the output frame
  @param   aHeader     the prepared FITS header
  @return  CPL_ERROR_NONE on success another cpl_error_code on error

  @remark This function is also used to generate the recipe documentation.
 */
/*----------------------------------------------------------------------------*/
static cpl_error_code
muse_create_sky_prepare_header(const char *aFrametag, cpl_propertylist *aHeader)
{
  cpl_ensure_code(aFrametag, CPL_ERROR_NULL_INPUT);
  cpl_ensure_code(aHeader, CPL_ERROR_NULL_INPUT);
  if (!strcmp(aFrametag, "SKY_MASK")) {
    muse_processing_prepare_property(aHeader, "ESO QC SKY THRESHOLD",
                                     CPL_TYPE_DOUBLE,
                                     "Threshold in the white light considered as sky, used to create this mask");
  } else if (!strcmp(aFrametag, "SKY_IMAGE")) {
  } else if (!strcmp(aFrametag, "SKY_SPECTRUM")) {
  } else if (!strcmp(aFrametag, "SKY_LINES")) {
    muse_processing_prepare_property(aHeader, "ESO QC SKY LINE[0-9]+ NAME",
                                     CPL_TYPE_STRING,
                                     "Name of the strongest line in group k");
    muse_processing_prepare_property(aHeader, "ESO QC SKY LINE[0-9]+ AWAV",
                                     CPL_TYPE_DOUBLE,
                                     "[Angstrom] Wavelength (air) of the strongest line of group l");
    muse_processing_prepare_property(aHeader, "ESO QC SKY LINE[0-9]+ FLUX",
                                     CPL_TYPE_DOUBLE,
                                     "[erg/(s cm2 arcsec2)] Flux of the strongest line of group l");
  } else if (!strcmp(aFrametag, "SKY_CONTINUUM")) {
    muse_processing_prepare_property(aHeader, "ESO QC SKY CONT FLUX",
                                     CPL_TYPE_DOUBLE,
                                     "[erg/(s cm2 arcsec2)] Total flux of the continuum");
    muse_processing_prepare_property(aHeader, "ESO QC SKY CONT MAXDEV",
                                     CPL_TYPE_DOUBLE,
                                     "[erg/(s cm2 arcsec2 Angstrom)] Maximum (absolute value) of the derivative of the continuum spectrum");
  } else {
    cpl_msg_warning(__func__, "Frame tag %s is not defined", aFrametag);
    return CPL_ERROR_ILLEGAL_INPUT;
  }
  return CPL_ERROR_NONE;
} /* muse_create_sky_prepare_header() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Return the level of an output frame.
  @param   aFrametag   tag of the output frame
  @return  the cpl_frame_level or CPL_FRAME_LEVEL_NONE on error

  @remark This function is also used to generate the recipe documentation.
 */
/*----------------------------------------------------------------------------*/
static cpl_frame_level
muse_create_sky_get_frame_level(const char *aFrametag)
{
  if (!aFrametag) {
    return CPL_FRAME_LEVEL_NONE;
  }
  if (!strcmp(aFrametag, "SKY_MASK")) {
    return CPL_FRAME_LEVEL_INTERMEDIATE;
  }
  if (!strcmp(aFrametag, "SKY_IMAGE")) {
    return CPL_FRAME_LEVEL_INTERMEDIATE;
  }
  if (!strcmp(aFrametag, "SKY_SPECTRUM")) {
    return CPL_FRAME_LEVEL_INTERMEDIATE;
  }
  if (!strcmp(aFrametag, "SKY_LINES")) {
    return CPL_FRAME_LEVEL_FINAL;
  }
  if (!strcmp(aFrametag, "SKY_CONTINUUM")) {
    return CPL_FRAME_LEVEL_FINAL;
  }
  return CPL_FRAME_LEVEL_NONE;
} /* muse_create_sky_get_frame_level() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Return the mode of an output frame.
  @param   aFrametag   tag of the output frame
  @return  the muse_frame_mode

  @remark This function is also used to generate the recipe documentation.
 */
/*----------------------------------------------------------------------------*/
static muse_frame_mode
muse_create_sky_get_frame_mode(const char *aFrametag)
{
  if (!aFrametag) {
    return MUSE_FRAME_MODE_ALL;
  }
  if (!strcmp(aFrametag, "SKY_MASK")) {
    return MUSE_FRAME_MODE_MASTER;
  }
  if (!strcmp(aFrametag, "SKY_IMAGE")) {
    return MUSE_FRAME_MODE_MASTER;
  }
  if (!strcmp(aFrametag, "SKY_SPECTRUM")) {
    return MUSE_FRAME_MODE_MASTER;
  }
  if (!strcmp(aFrametag, "SKY_LINES")) {
    return MUSE_FRAME_MODE_MASTER;
  }
  if (!strcmp(aFrametag, "SKY_CONTINUUM")) {
    return MUSE_FRAME_MODE_MASTER;
  }
  return MUSE_FRAME_MODE_ALL;
} /* muse_create_sky_get_frame_mode() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Setup the recipe options.
  @param   aPlugin   the plugin
  @return  0 if everything is ok, -1 if not called as part of a recipe.

  Define the command-line, configuration, and environment parameters for the
  recipe.
 */
/*----------------------------------------------------------------------------*/
static int
muse_create_sky_create(cpl_plugin *aPlugin)
{
  /* Check that the plugin is part of a valid recipe */
  cpl_recipe *recipe;
  if (cpl_plugin_get_type(aPlugin) == CPL_PLUGIN_TYPE_RECIPE) {
    recipe = (cpl_recipe *)aPlugin;
  } else {
    return -1;
  }

  /* register the extended processing information (new FITS header creation, *
   * getting of the frame level for a certain tag)                           */
  muse_processinginfo_register(recipe,
                               muse_create_sky_new_recipeconfig(),
                               muse_create_sky_prepare_header,
                               muse_create_sky_get_frame_level,
                               muse_create_sky_get_frame_mode);

  /* XXX initialize timing in messages                                       *
   *     since at least esorex is too stupid to turn it on, we have to do it */
  if (muse_cplframework() == MUSE_CPLFRAMEWORK_ESOREX) {
    cpl_msg_set_time_on();
  }

  /* Create the parameter list in the cpl_recipe object */
  recipe->parameters = cpl_parameterlist_new();
  /* Fill the parameters list */
  cpl_parameter *p;
      
  /* --fraction: Fraction of the image (without the ignored part) to be considered as sky. If an input sky mask is provided, the fraction is applied to the regions within the mask. If the whole sky mask should be used, set this parameter to 1. */
  p = cpl_parameter_new_value("muse.muse_create_sky.fraction",
                              CPL_TYPE_DOUBLE,
                             "Fraction of the image (without the ignored part) to be considered as sky. If an input sky mask is provided, the fraction is applied to the regions within the mask. If the whole sky mask should be used, set this parameter to 1.",
                              "muse.muse_create_sky",
                              (double)0.75);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "fraction");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "fraction");

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --ignore: Fraction of the image to be ignored. If an input sky mask is provided, the fraction is applied to the regions within the mask. If the whole sky mask should be used, set this parameter to 0. */
  p = cpl_parameter_new_value("muse.muse_create_sky.ignore",
                              CPL_TYPE_DOUBLE,
                             "Fraction of the image to be ignored. If an input sky mask is provided, the fraction is applied to the regions within the mask. If the whole sky mask should be used, set this parameter to 0.",
                              "muse.muse_create_sky",
                              (double)0.05);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "ignore");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "ignore");

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --sampling: Spectral sampling of the sky spectrum [Angstrom]. */
  p = cpl_parameter_new_value("muse.muse_create_sky.sampling",
                              CPL_TYPE_DOUBLE,
                             "Spectral sampling of the sky spectrum [Angstrom].",
                              "muse.muse_create_sky",
                              (double)0.3125);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "sampling");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "sampling");
  if (!getenv("MUSE_EXPERT_USER")) {
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_CLI);
  }

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --csampling: Spectral sampling of the continuum spectrum [Angstrom]. */
  p = cpl_parameter_new_value("muse.muse_create_sky.csampling",
                              CPL_TYPE_DOUBLE,
                             "Spectral sampling of the continuum spectrum [Angstrom].",
                              "muse.muse_create_sky",
                              (double)0.3125);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "csampling");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "csampling");
  if (!getenv("MUSE_EXPERT_USER")) {
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_CLI);
  }

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --crsigma: Sigma level clipping for cube-based and spectrum-based CR rejection. This has to be a string of two comma-separated floating-point numbers. The first value gives the sigma-level rejection for cube-based CR rejection (using "median", see muse_scipost), the second value the sigma-level for spectrum-based CR cleaning. Both can be switched off, by passing zero or a negative value. */
  p = cpl_parameter_new_value("muse.muse_create_sky.crsigma",
                              CPL_TYPE_STRING,
                             "Sigma level clipping for cube-based and spectrum-based CR rejection. This has to be a string of two comma-separated floating-point numbers. The first value gives the sigma-level rejection for cube-based CR rejection (using \"median\", see muse_scipost), the second value the sigma-level for spectrum-based CR cleaning. Both can be switched off, by passing zero or a negative value.",
                              "muse.muse_create_sky",
                              (const char *)"15.,15.");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "crsigma");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "crsigma");
  if (!getenv("MUSE_EXPERT_USER")) {
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_CLI);
  }

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --lambdamin: Cut off the data below this wavelength after loading the pixel table(s). */
  p = cpl_parameter_new_value("muse.muse_create_sky.lambdamin",
                              CPL_TYPE_DOUBLE,
                             "Cut off the data below this wavelength after loading the pixel table(s).",
                              "muse.muse_create_sky",
                              (double)4000.);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "lambdamin");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "lambdamin");

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --lambdamax: Cut off the data above this wavelength after loading the pixel table(s). */
  p = cpl_parameter_new_value("muse.muse_create_sky.lambdamax",
                              CPL_TYPE_DOUBLE,
                             "Cut off the data above this wavelength after loading the pixel table(s).",
                              "muse.muse_create_sky",
                              (double)10000.);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "lambdamax");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "lambdamax");

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --lambdaref: Reference wavelength used for correction of differential atmospheric refraction. The R-band (peak wavelength ~7000 Angstrom) that is usually used for guiding, is close to the central wavelength of MUSE, so a value of 7000.0 Angstrom should be used if nothing else is known. A value less than zero switches DAR correction off. */
  p = cpl_parameter_new_value("muse.muse_create_sky.lambdaref",
                              CPL_TYPE_DOUBLE,
                             "Reference wavelength used for correction of differential atmospheric refraction. The R-band (peak wavelength ~7000 Angstrom) that is usually used for guiding, is close to the central wavelength of MUSE, so a value of 7000.0 Angstrom should be used if nothing else is known. A value less than zero switches DAR correction off.",
                              "muse.muse_create_sky",
                              (double)7000.);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "lambdaref");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "lambdaref");

  cpl_parameterlist_append(recipe->parameters, p);
    
  return 0;
} /* muse_create_sky_create() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Fill the recipe parameters into the parameter structure
  @param   aParams       the recipe-internal structure to fill
  @param   aParameters   the cpl_parameterlist with the parameters
  @return  0 if everything is ok, -1 if something went wrong.

  This is a convienience function that centrally fills all parameters into the
  according fields of the recipe internal structure.
 */
/*----------------------------------------------------------------------------*/
static int
muse_create_sky_params_fill(muse_create_sky_params_t *aParams, cpl_parameterlist *aParameters)
{
  cpl_ensure_code(aParams, CPL_ERROR_NULL_INPUT);
  cpl_ensure_code(aParameters, CPL_ERROR_NULL_INPUT);
  cpl_parameter *p;
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_create_sky.fraction");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->fraction = cpl_parameter_get_double(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_create_sky.ignore");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->ignore = cpl_parameter_get_double(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_create_sky.sampling");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->sampling = cpl_parameter_get_double(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_create_sky.csampling");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->csampling = cpl_parameter_get_double(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_create_sky.crsigma");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->crsigma = cpl_parameter_get_string(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_create_sky.lambdamin");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->lambdamin = cpl_parameter_get_double(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_create_sky.lambdamax");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->lambdamax = cpl_parameter_get_double(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_create_sky.lambdaref");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->lambdaref = cpl_parameter_get_double(p);
    
  return 0;
} /* muse_create_sky_params_fill() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief    Execute the plugin instance given by the interface
  @param    aPlugin   the plugin
  @return   0 if everything is ok, -1 if not called as part of a recipe
 */
/*----------------------------------------------------------------------------*/
static int
muse_create_sky_exec(cpl_plugin *aPlugin)
{
  if (cpl_plugin_get_type(aPlugin) != CPL_PLUGIN_TYPE_RECIPE) {
    return -1;
  }
  muse_processing_recipeinfo(aPlugin);
  cpl_recipe *recipe = (cpl_recipe *)aPlugin;
  cpl_msg_set_threadid_on();

  cpl_frameset *usedframes = cpl_frameset_new(),
               *outframes = cpl_frameset_new();
  muse_create_sky_params_t params;
  muse_create_sky_params_fill(&params, recipe->parameters);

  cpl_errorstate prestate = cpl_errorstate_get();

  muse_processing *proc = muse_processing_new("muse_create_sky",
                                              recipe);
  int rc = muse_create_sky_compute(proc, &params);
  cpl_frameset_join(usedframes, proc->usedframes);
  cpl_frameset_join(outframes, proc->outframes);
  muse_processing_delete(proc);
  
  if (!cpl_errorstate_is_equal(prestate)) {
    /* dump all errors from this recipe in chronological order */
    cpl_errorstate_dump(prestate, CPL_FALSE, muse_cplerrorstate_dump_some);
    /* reset message level to not get the same errors displayed again by esorex */
    cpl_msg_set_level(CPL_MSG_INFO);
  }
  /* clean up duplicates in framesets of used and output frames */
  muse_cplframeset_erase_duplicate(usedframes);
  muse_cplframeset_erase_duplicate(outframes);

  /* to get esorex to see our classification (frame groups etc.), *
   * replace the original frameset with the list of used frames   *
   * before appending product output frames                       */
  /* keep the same pointer, so just erase all frames, not delete the frameset */
  muse_cplframeset_erase_all(recipe->frames);
  cpl_frameset_join(recipe->frames, usedframes);
  cpl_frameset_join(recipe->frames, outframes);
  cpl_frameset_delete(usedframes);
  cpl_frameset_delete(outframes);
  return rc;
} /* muse_create_sky_exec() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief    Destroy what has been created by the 'create' function
  @param    aPlugin   the plugin
  @return   0 if everything is ok, -1 if not called as part of a recipe
 */
/*----------------------------------------------------------------------------*/
static int
muse_create_sky_destroy(cpl_plugin *aPlugin)
{
  /* Get the recipe from the plugin */
  cpl_recipe *recipe;
  if (cpl_plugin_get_type(aPlugin) == CPL_PLUGIN_TYPE_RECIPE) {
    recipe = (cpl_recipe *)aPlugin;
  } else {
    return -1;
  }

  /* Clean up */
  cpl_parameterlist_delete(recipe->parameters);
  muse_processinginfo_delete(recipe);
  return 0;
} /* muse_create_sky_destroy() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief    Add this recipe to the list of available plugins.
  @param    aList   the plugin list
  @return   0 if everything is ok, -1 otherwise (but this cannot happen)

  Create the recipe instance and make it available to the application using the
  interface. This function is exported.
 */
/*----------------------------------------------------------------------------*/
int
cpl_plugin_get_info(cpl_pluginlist *aList)
{
  cpl_recipe *recipe = cpl_calloc(1, sizeof *recipe);
  cpl_plugin *plugin = &recipe->interface;

  char *helptext;
  if (muse_cplframework() == MUSE_CPLFRAMEWORK_ESOREX) {
    helptext = cpl_sprintf("%s%s", muse_create_sky_help,
                           muse_create_sky_help_esorex);
  } else {
    helptext = cpl_sprintf("%s", muse_create_sky_help);
  }

  /* Initialize the CPL plugin stuff for this module */
  cpl_plugin_init(plugin, CPL_PLUGIN_API, MUSE_BINARY_VERSION,
                  CPL_PLUGIN_TYPE_RECIPE,
                  "muse_create_sky",
                  "Create night sky model from selected pixels of an exposure of empty sky.",
                  helptext,
                  "Ole Streicher",
                  "https://support.eso.org",
                  muse_get_license(),
                  muse_create_sky_create,
                  muse_create_sky_exec,
                  muse_create_sky_destroy);
  cpl_pluginlist_append(aList, plugin);
  cpl_free(helptext);

  return 0;
} /* cpl_plugin_get_info() */

/**@}*/
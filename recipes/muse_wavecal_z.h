/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/* 
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* This file was automatically generated */

#ifndef MUSE_WAVECAL_Z_H
#define MUSE_WAVECAL_Z_H

/*----------------------------------------------------------------------------*
 *                              Includes                                      *
 *----------------------------------------------------------------------------*/
#include <muse.h>
#include <muse_instrument.h>

/*----------------------------------------------------------------------------*
 *                          Special variable types                            *
 *----------------------------------------------------------------------------*/

/** @addtogroup recipe_muse_wavecal */
/**@{*/

/*----------------------------------------------------------------------------*/
/**
  @brief   Structure to hold the parameters of the muse_wavecal recipe.

  This structure contains the parameters for the recipe that may be set on the
  command line, in the configuration, or through the environment.
 */
/*----------------------------------------------------------------------------*/
typedef struct muse_wavecal_params_s {
  /** @brief   IFU to handle. If set to 0, all IFUs are processed serially. If set to -1, all IFUs are processed in parallel. */
  int nifu;

  /** @brief   If this is "none", stop when detecting discrepant overscan levels (see ovscsigma), for "offset" it assumes that the mean overscan level represents the real offset in the bias levels of the exposures involved, and adjusts the data accordingly; for "vpoly", a polynomial is fit to the vertical overscan and subtracted from the whole quadrant. */
  const char * overscan;

  /** @brief   This influences how values are rejected when computing overscan statistics. Either no rejection at all ("none"), rejection using the DCR algorithm ("dcr"), or rejection using an iterative constant fit ("fit"). */
  const char * ovscreject;

  /** @brief   If the deviation of mean overscan levels between a raw input image and the reference image is higher than |ovscsigma x stdev|, stop the processing. If overscan="vpoly", this is used as sigma rejection level for the iterative polynomial fit (the level comparison is then done afterwards with |100 x stdev| to guard against incompatible settings). Has no effect for overscan="offset". */
  double ovscsigma;

  /** @brief   The number of pixels of the overscan adjacent to the data section of the CCD that are ignored when computing statistics or fits. */
  int ovscignore;

  /** @brief   Type of lampwise image combination to use. */
  int combine;
  /** @brief   Type of lampwise image combination to use. (as string) */
  const char *combine_s;

  /** @brief   Identify and measure the arc emission lines on images separately for each lamp setup. */
  int lampwise;

  /** @brief   Sigma level used to detect arc emission lines above the median background level in the S/N image of the central column of each slice */
  double sigma;

  /** @brief   The allowed range of resolutions for pattern matching (of detected arc lines to line list) in fractions relative to the expected value */
  double dres;

  /** @brief   Tolerance for pattern matching (of detected arc lines to line list) */
  double tolerance;

  /** @brief   Order of the polynomial for the horizontal curvature within each slice */
  int xorder;

  /** @brief   Order of the polynomial used to fit the dispersion relation */
  int yorder;

  /** @brief   Sigma level for iterative rejection of deviant fits for each arc line within each slice, a negative value means to use the default (2.5). */
  double linesigma;

  /** @brief   Create a table containing residuals of the fits to the data of all arc lines. This is useful to assess the quality of the wavelength solution in detail. */
  int residuals;

  /** @brief   Sigma level for iterative rejection of deviant datapoints during the final polynomial wavelength solution within each slice, a negative value means to use the default (3.0). */
  double fitsigma;

  /** @brief   Type of weighting to use in the final polynomial wavelength solution fit, using centroiding error estimate and/or scatter of each single line as estimates of its accuracy. */
  int fitweighting;
  /** @brief   Type of weighting to use in the final polynomial wavelength solution fit, using centroiding error estimate and/or scatter of each single line as estimates of its accuracy. (as string) */
  const char *fitweighting_s;

  /** @brief   Save  */
  int saveimages;

  /** @brief   Resample the input arc images onto 2D images for a visual check using tracing and wavelength calibration solutions. Note that the image produced will show small wiggles even when the calibration was successful! */
  int resample;

  /** @brief   Create a wavelength map of the input images */
  int wavemap;

  /** @brief   Merge output products from different IFUs into a common file. */
  int merge;

  char __dummy__; /* quieten compiler warning about possibly empty struct */
} muse_wavecal_params_t;

#define MUSE_WAVECAL_PARAM_COMBINE_AVERAGE 1
#define MUSE_WAVECAL_PARAM_COMBINE_MEDIAN 2
#define MUSE_WAVECAL_PARAM_COMBINE_MINMAX 3
#define MUSE_WAVECAL_PARAM_COMBINE_SIGCLIP 4
#define MUSE_WAVECAL_PARAM_COMBINE_INVALID_VALUE -1
#define MUSE_WAVECAL_PARAM_FITWEIGHTING_UNIFORM 1
#define MUSE_WAVECAL_PARAM_FITWEIGHTING_CERR 2
#define MUSE_WAVECAL_PARAM_FITWEIGHTING_SCATTER 3
#define MUSE_WAVECAL_PARAM_FITWEIGHTING_CERRSCATTER 4
#define MUSE_WAVECAL_PARAM_FITWEIGHTING_INVALID_VALUE -1

/**@}*/

/*----------------------------------------------------------------------------*
 *                           Function prototypes                              *
 *----------------------------------------------------------------------------*/
int muse_wavecal_compute(muse_processing *, muse_wavecal_params_t *);

#endif /* MUSE_WAVECAL_Z_H */

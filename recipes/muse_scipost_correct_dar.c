/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/*
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2014 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*---------------------------------------------------------------------------*
 *                             Includes                                      *
 *---------------------------------------------------------------------------*/
#include <string.h>
#include <muse.h>
#include "muse_scipost_correct_dar_z.h"

/*---------------------------------------------------------------------------*
 *                             Functions code                                *
 *---------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
  @brief    Separate DAR correction main routine
  @param    aProcessing  the processing structure
  @param    aParams      the parameters list
  @return   0 if everything is ok, -1 something went wrong

  @see muse_scipost_process_exposure()
 */
/*----------------------------------------------------------------------------*/
int
muse_scipost_correct_dar_compute(muse_processing *aProcessing,
                                 muse_scipost_correct_dar_params_t *aParams)
{
  cpl_frameset *inframes = muse_frameset_find_tags(aProcessing->inframes,
                                                      aProcessing->intags, 0,
                                                      CPL_FALSE);
  cpl_error_code rc = CPL_ERROR_NONE;
  cpl_size iframe, nframes = cpl_frameset_get_size(inframes);
  for (iframe = 0; iframe < nframes; iframe++) {
    cpl_frame *frame = cpl_frameset_get_position(inframes, iframe);
    const char *fn = cpl_frame_get_filename(frame);
    muse_pixtable *pixtable = muse_pixtable_load_restricted_wavelength(fn,
                                                                       aParams->lambdamin,
                                                                       aParams->lambdamax);
    if (pixtable == NULL) {
      cpl_msg_error(__func__, "NULL pixel table for %s", fn);
      rc = CPL_ERROR_NULL_INPUT;
      break;
    }
    muse_processing_append_used(aProcessing, frame, CPL_FRAME_GROUP_RAW, 1);

    /* erase pre-existing QC parameters */
    cpl_propertylist_erase_regexp(pixtable->header, "ESO QC ", 0);
    if (muse_pfits_get_mode(pixtable->header) <= MUSE_MODE_WFM_AO_N) {
      cpl_msg_debug(__func__, "WFM detected: starting DAR correction");
      rc = muse_dar_correct(pixtable, aParams->lambdaref);
      if (rc != CPL_ERROR_NONE) {
        cpl_msg_error(__func__, "while muse_dar_correct(%s)",
                      cpl_frame_get_filename(frame));
        muse_pixtable_delete(pixtable);
        break;
      }
    } /* if WFM */
    /* check and possibly correct the DAR quality, if requested to do so */
    if (aParams->darcheck != MUSE_SCIPOST_CORRECT_DAR_PARAM_DARCHECK_NONE) {
      cpl_boolean dorefine = aParams->darcheck
                           == MUSE_SCIPOST_CORRECT_DAR_PARAM_DARCHECK_CORRECT;
      cpl_msg_info(__func__, "Carrying out DAR %s", dorefine ? "correction" : "check");
      double maxshift = 0;
      rc = muse_dar_check(pixtable, &maxshift, dorefine, NULL);
      if (rc != CPL_ERROR_NONE) {
        cpl_msg_error(__func__, "Maximum detected shift %f\'\' (failure for "
                      "\"%s\"; rc = %d: %s)", maxshift,
                      cpl_frame_get_filename(frame), rc, cpl_error_get_message());
        muse_pixtable_delete(pixtable);
        break;
      } else {
        cpl_msg_info(__func__, "Maximum detected shift %f\'\'", maxshift);
      }
    } /* if DAR checking */
    muse_processing_save_table(aProcessing, 0, pixtable, NULL,
                               MUSE_TAG_PIXTABLE_REDUCED,
                               MUSE_TABLE_TYPE_PIXTABLE);
    muse_pixtable_delete(pixtable);
  } /* for iframe (all input frames) */
  cpl_frameset_delete(inframes);

  return rc;
} /* muse_scipost_correct_dar_compute() */

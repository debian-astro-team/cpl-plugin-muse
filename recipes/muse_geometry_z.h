/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/* 
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* This file was automatically generated */

#ifndef MUSE_GEOMETRY_Z_H
#define MUSE_GEOMETRY_Z_H

/*----------------------------------------------------------------------------*
 *                              Includes                                      *
 *----------------------------------------------------------------------------*/
#include <muse.h>
#include <muse_instrument.h>

/*----------------------------------------------------------------------------*
 *                          Special variable types                            *
 *----------------------------------------------------------------------------*/

/** @addtogroup recipe_muse_geometry */
/**@{*/

/*----------------------------------------------------------------------------*/
/**
  @brief   Structure to hold the parameters of the muse_geometry recipe.

  This structure contains the parameters for the recipe that may be set on the
  command line, in the configuration, or through the environment.
 */
/*----------------------------------------------------------------------------*/
typedef struct muse_geometry_params_s {
  /** @brief   First IFU to analyze. */
  int ifu1;

  /** @brief   Last IFU to analyze. */
  int ifu2;

  /** @brief   Sigma detection level for spot detection, in terms of median deviation above the median. */
  double sigma;

  /** @brief   Type of centroiding and FWHM determination to use for all spot measurements: simple barycenter method or using a Gaussian fit. */
  int centroid;
  /** @brief   Type of centroiding and FWHM determination to use for all spot measurements: simple barycenter method or using a Gaussian fit. (as string) */
  const char *centroid_s;

  /** @brief   Use this sigma-level cut for smoothing of the output table within each slicer stack. Set to non-positive value to deactivate smoothing. */
  double smooth;

  /** @brief   When passing any MASK_CHECK frames in the input, use this lower wavelength cut before reconstructing the image. */
  double lambdamin;

  /** @brief   When passing any MASK_CHECK frames in the input, use this upper wavelength cut before reconstructing the image. */
  double lambdamax;

  char __dummy__; /* quieten compiler warning about possibly empty struct */
} muse_geometry_params_t;

#define MUSE_GEOMETRY_PARAM_CENTROID_BARYCENTER 1
#define MUSE_GEOMETRY_PARAM_CENTROID_GAUSSIAN 2
#define MUSE_GEOMETRY_PARAM_CENTROID_INVALID_VALUE -1

/**@}*/

/*----------------------------------------------------------------------------*
 *                           Function prototypes                              *
 *----------------------------------------------------------------------------*/
int muse_geometry_compute(muse_processing *, muse_geometry_params_t *);

#endif /* MUSE_GEOMETRY_Z_H */

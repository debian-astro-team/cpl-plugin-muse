<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/xsl" href="recipedoc2html.xsl" ?>
<pluginlist instrument="muse">
  <plugin name="muse_create_sky" type="recipe">
    <plugininfo>
      <synopsis>Create night sky model from selected pixels of an exposure of empty sky.</synopsis>
      <description>
        This recipe creates the continuum and the atmospheric transition line
        spectra of the night sky from the data in a pixel table(s) belonging to
        one exposure of (mostly) empty sky.
      </description>
      <author>Ole Streicher</author>
      <email>ole@aip.de</email>
    </plugininfo>
    <parameters>
      <parameter name="fraction" type="double">
        <description>Fraction of the image (without the ignored part) to be considered as sky. If an input sky mask is provided, the fraction is applied to the regions within the mask. If the whole sky mask should be used, set this parameter to 1.</description>
        <default>0.75</default>
      </parameter>
      <parameter name="ignore" type="double">
        <description>Fraction of the image to be ignored. If an input sky mask is provided, the fraction is applied to the regions within the mask. If the whole sky mask should be used, set this parameter to 0.</description>
        <default>0.05</default>
      </parameter>
      <parameter name="sampling" type="double" expertlevel="true">
        <description>Spectral sampling of the sky spectrum [Angstrom].</description>
        <default>0.3125</default>
      </parameter>
      <parameter name="csampling" type="double" expertlevel="true">
        <description>Spectral sampling of the continuum spectrum [Angstrom].</description>
        <default>0.3125</default>
      </parameter>
      <parameter name="crsigma" type="string" expertlevel="true">
        <description>Sigma level clipping for cube-based and spectrum-based CR rejection. This has to be a string of two comma-separated floating-point numbers. The first value gives the sigma-level rejection for cube-based CR rejection (using "median", see muse_scipost), the second value the sigma-level for spectrum-based CR cleaning. Both can be switched off, by passing zero or a negative value.</description>
        <default>15.,15.</default>
      </parameter>
      <parameter name="lambdamin" type="double">
        <description>Cut off the data below this wavelength after loading the pixel table(s).</description>
        <default>4000.</default>
      </parameter>
      <parameter name="lambdamax" type="double">
        <description>Cut off the data above this wavelength after loading the pixel table(s).</description>
        <default>10000.</default>
      </parameter>
      <parameter name="lambdaref" type="double">
        <description>Reference wavelength used for correction of differential atmospheric refraction. The R-band (peak wavelength ~7000 Angstrom) that is usually used for guiding, is close to the central wavelength of MUSE, so a value of 7000.0 Angstrom should be used if nothing else is known. A value less than zero switches DAR correction off.</description>
        <default>7000.</default>
      </parameter>
    </parameters>
    <frames>
      <frameset>
        <frame tag="PIXTABLE_SKY" group="raw" min="1">
          <description>Input pixel table. If the pixel table is not already flux calibrated, the corresponding flux calibration frames should be given as well.</description>
        </frame>
        <frame tag="EXTINCT_TABLE" group="calib" min="1" max="1">
          <description>Atmospheric extinction table</description>
          <select origin="extern"/>
        </frame>
        <frame tag="STD_RESPONSE" group="calib" min="1" max="1">
          <description>Response curve as derived from standard star(s)</description>
          <select origin="generated">
            <restrict attribute="ESO_INS_MODE"/>
          </select>
        </frame>
        <frame tag="STD_TELLURIC" group="calib" max="1">
          <description>Telluric absorption as derived from standard star(s)</description>
          <select origin="generated">
            <restrict attribute="ESO_INS_MODE"/>
          </select>
        </frame>
        <frame tag="SKY_LINES" group="calib" min="1" max="1">
          <description>List of OH transitions and other sky lines</description>
          <select origin="extern"/>
        </frame>
        <frame tag="SKY_CONTINUUM" group="calib" min="0" max="1">
          <description>Sky continuum to use</description>
          <select origin="generated">
            <restrict attribute="observation_block"/>
          </select>
        </frame>
        <frame tag="LSF_PROFILE" group="calib" min="1" max="1">
          <description>Slice specific LSF parameters cubes</description>
          <select origin="generated">
            <restrict attribute="ESO_INS_MODE"/>
            <restrict attribute="ESO_DET_READ_CURID"/>
          </select>
        </frame>
        <frame tag="SKY_MASK" group="calib" min="0" max="1" format="SKY_MASK">
          <description>Sky mask to use</description>
          <select origin="generated">
            <restrict attribute="observation_block"/>
          </select>
        </frame>
        <frame tag="SKY_MASK" group="product" level="intermediate" mode="master" format="SKY_MASK">
          <description>Created sky mask</description>
          <fitsheader>
            <property type="double">
              <name>ESO QC SKY THRESHOLD</name>
              <description>Threshold in the white light considered as sky, used to create this mask</description>
            </property>
          </fitsheader>
        </frame>
        <frame tag="SKY_IMAGE" group="product" level="intermediate" mode="master" format="MUSE_IMAGE">
          <description>Whitelight image used to create the sky mask</description>
        </frame>
        <frame tag="SKY_SPECTRUM" group="product" level="intermediate" mode="master" format="FLUX_TABLE">
          <description>Sky spectrum within the sky mask</description>
        </frame>
        <frame tag="SKY_LINES" group="product" level="final" mode="master" format="SKY_LINES">
          <description>Estimated sky line flux table</description>
          <fitsheader>
            <parameter id="l" name="line group" regexp="[0-9]+" min="1"/>
            <property type="string">
              <name>ESO QC SKY LINEl NAME</name>
              <description>Name of the strongest line in group k</description>
            </property>
            <property type="double">
              <name>ESO QC SKY LINEl AWAV</name>
              <unit>Angstrom</unit>
              <description>Wavelength (air) of the strongest line of group l</description>
            </property>
            <property type="double">
              <name>ESO QC SKY LINEl FLUX</name>
              <unit>erg/(s cm2 arcsec2)</unit>
              <description>Flux of the strongest line of group l</description>
            </property>
          </fitsheader>
        </frame>
        <frame tag="SKY_CONTINUUM" group="product" level="final" mode="master" format="FLUX_TABLE">
          <description>Estimated continuum flux spectrum</description>
          <fitsheader>
            <property type="double">
              <name>ESO QC SKY CONT FLUX</name>
              <unit>erg/(s cm2 arcsec2)</unit>
              <description>Total flux of the continuum</description>
            </property>
            <property type="double">
              <name>ESO QC SKY CONT MAXDEV</name>
              <unit>erg/(s cm2 arcsec2 Angstrom)</unit>
              <description>Maximum (absolute value) of the derivative of the continuum spectrum</description>
            </property>
          </fitsheader>
        </frame>
      </frameset>
    </frames>
  </plugin>
</pluginlist>

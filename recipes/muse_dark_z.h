/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/* 
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* This file was automatically generated */

#ifndef MUSE_DARK_Z_H
#define MUSE_DARK_Z_H

/*----------------------------------------------------------------------------*
 *                              Includes                                      *
 *----------------------------------------------------------------------------*/
#include <muse.h>
#include <muse_instrument.h>

/*----------------------------------------------------------------------------*
 *                          Special variable types                            *
 *----------------------------------------------------------------------------*/

/** @addtogroup recipe_muse_dark */
/**@{*/

/*----------------------------------------------------------------------------*/
/**
  @brief   Structure to hold the parameters of the muse_dark recipe.

  This structure contains the parameters for the recipe that may be set on the
  command line, in the configuration, or through the environment.
 */
/*----------------------------------------------------------------------------*/
typedef struct muse_dark_params_s {
  /** @brief   IFU to handle. If set to 0, all IFUs are processed serially. If set to -1, all IFUs are processed in parallel. */
  int nifu;

  /** @brief   If this is "none", stop when detecting discrepant overscan levels (see ovscsigma), for "offset" it assumes that the mean overscan level represents the real offset in the bias levels of the exposures involved, and adjusts the data accordingly; for "vpoly", a polynomial is fit to the vertical overscan and subtracted from the whole quadrant. */
  const char * overscan;

  /** @brief   This influences how values are rejected when computing overscan statistics. Either no rejection at all ("none"), rejection using the DCR algorithm ("dcr"), or rejection using an iterative constant fit ("fit"). */
  const char * ovscreject;

  /** @brief   If the deviation of mean overscan levels between a raw input image and the reference image is higher than |ovscsigma x stdev|, stop the processing. If overscan="vpoly", this is used as sigma rejection level for the iterative polynomial fit (the level comparison is then done afterwards with |100 x stdev| to guard against incompatible settings). Has no effect for overscan="offset". */
  double ovscsigma;

  /** @brief   The number of pixels of the overscan adjacent to the data section of the CCD that are ignored when computing statistics or fits. */
  int ovscignore;

  /** @brief   Type of image combination to use. */
  int combine;
  /** @brief   Type of image combination to use. (as string) */
  const char *combine_s;

  /** @brief   Number of minimum pixels to reject with minmax. */
  int nlow;

  /** @brief   Number of maximum pixels to reject with minmax. */
  int nhigh;

  /** @brief   Number of pixels to keep with minmax. */
  int nkeep;

  /** @brief   Low sigma for pixel rejection with sigclip. */
  double lsigma;

  /** @brief   High sigma for pixel rejection with sigclip. */
  double hsigma;

  /** @brief   Scale the individual images to a common exposure time before combining them. */
  int scale;

  /** @brief   Normalize the master dark to this exposure time (in seconds). To disable normalization, set this to a negative value. */
  double normalize;

  /** @brief   Sigma level, in terms of median deviation above the median dark level, above which a pixel is detected and marked as 'hot'. */
  double hotsigma;

  /** @brief   Model the master dark using a set of polynomials. */
  int model;

  /** @brief   Merge output products from different IFUs into a common file. */
  int merge;

  char __dummy__; /* quieten compiler warning about possibly empty struct */
} muse_dark_params_t;

#define MUSE_DARK_PARAM_COMBINE_AVERAGE 1
#define MUSE_DARK_PARAM_COMBINE_MEDIAN 2
#define MUSE_DARK_PARAM_COMBINE_MINMAX 3
#define MUSE_DARK_PARAM_COMBINE_SIGCLIP 4
#define MUSE_DARK_PARAM_COMBINE_INVALID_VALUE -1

/**@}*/

/*----------------------------------------------------------------------------*
 *                           Function prototypes                              *
 *----------------------------------------------------------------------------*/
int muse_dark_compute(muse_processing *, muse_dark_params_t *);

#endif /* MUSE_DARK_Z_H */

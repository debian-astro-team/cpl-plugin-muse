<?xml-stylesheet type="text/xsl" href="recipedoc2html.xsl" ?>
<pluginlist instrument="muse">
  <plugin name="muse_lingain" type="recipe">
    <plugininfo>
      <synopsis>Compute the gain and a model of the residual non-linearity for each detector quadrant</synopsis>
      <description>
        The recipe uses the bias and flat field images of a detector monitoring
        exposure sequence to determine the detector gain in counts/ADU and to
        model the residual non-linearity for each of the four detector quadrants
        of all IFUs.

        All measurements done by the recipe are done on the illuminated parts
        of the detector, i.e. on the slices. The location of the slices is
        taken from the given trace table, which is a mandatory input. Using the
        traces of the slices on the detector a set of measurement windows is
        placed along these traces. The data used for the determination of the
        gain and the residual non-linearity is the taken from these windows.

        Bad pixels indicated by an, optionally, provided bad pixel table, or
        flagged during the preprocessing (bias subtraction) of the input data
        are excluded from the measurements.

        Local measurements of the read-out-noise, the signal and the gain are
        calculated for each of the measurement windows. Using these measurements
        the gain for each detector quadrant is computed as the zero-order
        coefficient of a 1st order polynomial fitted to the binned gain
        measurements as a function of the signal level.

        The residual non-linearity is modelled by a (high) order polynomial
        which is fitted to the fractional percentage deviation of the count
        rate from an expected constant count rate (the linear case) as function
        of the signal level. (Not yet implemented!)
      </description>
      <author>Ralf Palsa</author>
      <email>rpalsa@eso.org</email>
    </plugininfo>
    <parameters>
      <parameter name="nifu" type="int">
        <description>IFU to handle. If set to 0, all IFUs are processed serially. If set to -1, all IFUs are processed in parallel.</description>
        <range min="-1" max="24"/>
        <default>0</default>
      </parameter>
      <parameter name="ybox" type="int">
        <description>
          Size of windows along the traces of the slices.
        </description>
        <default>50</default>
      </parameter>
      <parameter name="xgap" type="int">
        <description>
          Extra offset from tracing edge.
        </description>
        <default>3</default>
      </parameter>
      <parameter name="xborder" type="int">
        <description>
          Extra offset from the detector edge used for the selection of slices.
        </description>
        <default>10</default>
      </parameter>
      <parameter name="order" type="int">
        <description>
          Order of the polynomial used to fit the non-linearity residuals.
        </description>
        <default>12</default>
      </parameter>
      <parameter name="toffset" type="double">
        <description>
          Exposure time offset in seconds to apply to linearity flat fields.
        </description>
        <default>0.018</default>
      </parameter>
      <parameter name="fluxtol" type="double">
        <description>
          Tolerance value for the overall flux consistency check of a pair of flat fields. The value is the maximum relative offset.
        </description>
        <default>0.01</default>
      </parameter>
      <parameter name="sigma" type="double">
        <description>Sigma value used for signal value clipping.</description>
        <default>3.</default>
      </parameter>
      <parameter name="signalmin" type="double">
        <description>Minimum signal value in log(ADU) used for the gain analysis and the non-linearity polynomial model.</description>
        <default>0.</default>
      </parameter>
      <parameter name="signalmax" type="double">
        <description>Maximum signal value in log(ADU) used for the gain analysis and the non-linearity polynomial model.</description>
        <default>4.9</default>
      </parameter>
      <parameter name="signalbin" type="double">
        <description>Size of a signal bin in log10(ADU) used for the gain analysis and the non-linearity polynomial model.</description>
        <default>0.1</default>
      </parameter>
      <parameter name="gainlimit" type="double">
        <description>Minimum signal value [ADU] used for fitting the gain relation.</description>
        <default>100.</default>
      </parameter>
      <parameter name="gainsigma" type="double">
        <description>Sigma value for gain value clipping.</description>
        <default>3.</default>
      </parameter>
      <parameter name="ctsmin" type="double">
        <description>Minimum signal value in log(counts) to consider for the non-linearity analysis.</description>
        <default>3.</default>
      </parameter>
      <parameter name="ctsmax" type="double">
        <description>Maximum signal value in log(counts) to consider for the non-linearity analysis.</description>
        <default>4.9</default>
      </parameter>
      <parameter name="ctsbin" type="double">
        <description>Size of a signal bin in log10(counts) used for the non-linearity analysis.</description>
        <default>0.1</default>
      </parameter>
      <parameter name="linearmin" type="double">
        <description>Lower limit of desired linear range in log10(counts).</description>
        <default>2.5</default>
      </parameter>
      <parameter name="linearmax" type="double">
        <description>Upper limit of desired linear range in log10(counts).</description>
        <default>3.</default>
      </parameter>
      <parameter name="merge" type="boolean">
        <description>Merge output products from different IFUs into a common file.</description>
        <default>false</default>
      </parameter>
    </parameters>
    <frames>
      <frameset processing="standard">
        <frame tag="LINGAIN_LAMP_OFF" group="raw" min="2">
          <description>Detector monitoring bias images</description>
        </frame>
        <frame tag="LINGAIN_LAMP_ON" group="raw" min="2">
          <description>Detector monitoring flat field images</description>
        </frame>
        <frame tag="MASTER_BIAS" group="calib" min="1" max="1">
          <description>Master bias</description>
          <select origin="generated">
            <restrict attribute="ESO_DET_READ_CURID"/>
          </select>
        </frame>
        <frame tag="TRACE_TABLE" group="calib" min="1" max="1">
          <description>Trace table</description>
          <select origin="generated">
            <restrict attribute="ESO_INS_MODE"/>
            <restrict attribute="ESO_DET_READ_CURID"/>
          </select>
        </frame>
        <frame tag="BADPIX_TABLE" group="calib" default="false">
          <description>Known bad pixels</description>
          <select origin="extern"/>
        </frame>
        <frame tag="NONLINEARITY_GAIN" group="product" level="final" mode="master" format="HEADER">
          <description>List of non-linearity and gain parameters for each detector readout port.</description>
          <fitsheader>
            <parameter id="n" name="quadrant" regexp="[1234]" min="1" max="4"/>
            <property type="double">
              <name>ESO QC LINGAIN OUTn COUNTS MIN</name>
              <unit>ADU</unit>
              <description>Minimum signal level in measurement region</description>
            </property>
            <property type="double">
              <name>ESO QC LINGAIN OUTn COUNTS MAX</name>
              <unit>ADU</unit>
              <description>Maximum signal level in measurement region</description>
            </property>
            <property type="double">
              <name>ESO QC LINGAIN OUTn RON</name>
              <unit>count</unit>
              <description>Read-out noise measured per quadrant as weighted mean.</description>
            </property>
            <property type="double">
              <name>ESO QC LINGAIN OUTn RONERR</name>
              <unit>count</unit>
              <description>Read-out noise error estimate</description>
            </property>
            <property type="double">
              <name>ESO QC LINGAIN OUTn RON MEDIAN</name>
              <unit>count</unit>
              <description>Median read-out noise</description>
            </property>
            <property type="double">
              <name>ESO QC LINGAIN OUTn RON MAD</name>
              <unit>count</unit>
              <description>MAD of the read-out noise measurements</description>
            </property>
            <property type="double">
              <name>ESO QC LINGAIN OUTn CONAD</name>
              <unit>ADU/count</unit>
              <description>Conversion factor calculated as the inverse of the measured gain</description>
            </property>
            <property type="double">
              <name>ESO QC LINGAIN OUTn GAIN</name>
              <unit>count/ADU</unit>
              <description>Gain value as determined from a first order fit</description>
            </property>
            <property type="double">
              <name>ESO QC LINGAIN OUTn GAINERR</name>
              <unit>count/ADU</unit>
              <description>RMS of the first order polynomial fit used to determine the gain</description>
            </property>
            <!--
            <property type="double">
              <name>ESO QC LINGAIN NLFITn RMS</name>
              <description>RMS of the residual non-linearity fit</description>
            </property>
             -->
          </fitsheader>
        </frame>
      </frameset>
    </frames>
  </plugin>
</pluginlist>

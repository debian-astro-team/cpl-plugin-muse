/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/* 
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* This file was automatically generated */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*----------------------------------------------------------------------------*
 *                              Includes                                      *
 *----------------------------------------------------------------------------*/
#include <string.h> /* strcmp(), strstr() */
#include <strings.h> /* strcasecmp() */
#include <cpl.h>

#include "muse_dark_z.h" /* in turn includes muse.h */

/*----------------------------------------------------------------------------*/
/**
  @defgroup recipe_muse_dark         Recipe muse_dark: Combine several separate dark images into one master dark file and locate hot pixels.
  @author Peter Weilbacher
  
        This recipe combines several separate dark images into one master dark
        file.
        The master dark contains the combined pixel values of the raw dark
        exposures, with respect to the image combination method used and
        normalization time specified.

        Processing trims the raw data and records the overscan statistics,
        subtracts the bias (taking account of the overscan, if --overscan is not
        "none") from each raw input image, converts them from adu to count,
        scales them according to their exposure time, and combines them using
        input parameters. Hot pixels are then identified using image statistics
        and marked in the data quality extension. The combined image is
        normalized to 1 hour exposure time. QC statistics are computed on
        the output master dark.

        If --model=true, a smooth polynomial model of the combined master dark
        is computed, created from several individual 2D polynomials to describe
        different features visible in MUSE dark frames. It is only advisable to
        use this, if the master dark is the result of at least 50 individual
        long dark exposures.
      
 */
/*----------------------------------------------------------------------------*/
/**@{*/

/*----------------------------------------------------------------------------*
 *                         Static variables                                   *
 *----------------------------------------------------------------------------*/
static const char *muse_dark_help =
  "This recipe combines several separate dark images into one master dark file. The master dark contains the combined pixel values of the raw dark exposures, with respect to the image combination method used and normalization time specified. Processing trims the raw data and records the overscan statistics, subtracts the bias (taking account of the overscan, if --overscan is not \"none\") from each raw input image, converts them from adu to count, scales them according to their exposure time, and combines them using input parameters. Hot pixels are then identified using image statistics and marked in the data quality extension. The combined image is normalized to 1 hour exposure time. QC statistics are computed on the output master dark. If --model=true, a smooth polynomial model of the combined master dark is computed, created from several individual 2D polynomials to describe different features visible in MUSE dark frames. It is only advisable to use this, if the master dark is the result of at least 50 individual long dark exposures.";

static const char *muse_dark_help_esorex =
  "\n\nInput frames for raw frame tag \"DARK\":\n"
  "\n Frame tag            Type Req #Fr Description"
  "\n -------------------- ---- --- --- ------------"
  "\n DARK                 raw   Y  >=3 Raw dark"
  "\n MASTER_BIAS          calib Y    1 Master bias"
  "\n BADPIX_TABLE         calib .      Known bad pixels"
  "\n\nProduct frames for raw frame tag \"DARK\":\n"
  "\n Frame tag            Level    Description"
  "\n -------------------- -------- ------------"
  "\n MASTER_DARK          final    Master dark"
  "\n MODEL_DARK           final    Model of the master dark (if --model=true).";

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Create the recipe config for this plugin.

  @remark The recipe config is used to check for the tags as well as to create
          the documentation of this plugin.
 */
/*----------------------------------------------------------------------------*/
static cpl_recipeconfig *
muse_dark_new_recipeconfig(void)
{
  cpl_recipeconfig *recipeconfig = cpl_recipeconfig_new();
      
  cpl_recipeconfig_set_tag(recipeconfig, "DARK", 3, -1);
  cpl_recipeconfig_set_input(recipeconfig, "DARK", "MASTER_BIAS", 1, 1);
  cpl_recipeconfig_set_input(recipeconfig, "DARK", "BADPIX_TABLE", -1, -1);
  cpl_recipeconfig_set_output(recipeconfig, "DARK", "MASTER_DARK");
  cpl_recipeconfig_set_output(recipeconfig, "DARK", "MODEL_DARK");
    
  return recipeconfig;
} /* muse_dark_new_recipeconfig() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Return a new header that shall be filled on output.
  @param   aFrametag   tag of the output frame
  @param   aHeader     the prepared FITS header
  @return  CPL_ERROR_NONE on success another cpl_error_code on error

  @remark This function is also used to generate the recipe documentation.
 */
/*----------------------------------------------------------------------------*/
static cpl_error_code
muse_dark_prepare_header(const char *aFrametag, cpl_propertylist *aHeader)
{
  cpl_ensure_code(aFrametag, CPL_ERROR_NULL_INPUT);
  cpl_ensure_code(aHeader, CPL_ERROR_NULL_INPUT);
  if (!strcmp(aFrametag, "MASTER_DARK")) {
    muse_processing_prepare_property(aHeader, "ESO QC DARK INPUT[0-9]+ NSATURATED",
                                     CPL_TYPE_INT,
                                     "Number of saturated pixels in raw dark i in input list");
    muse_processing_prepare_property(aHeader, "ESO QC DARK MASTER NBADPIX",
                                     CPL_TYPE_INT,
                                     "Number of bad pixels determined from master dark");
    muse_processing_prepare_property(aHeader, "ESO QC DARK MASTER MEDIAN",
                                     CPL_TYPE_FLOAT,
                                     "Median value of the master dark");
    muse_processing_prepare_property(aHeader, "ESO QC DARK MASTER MEAN",
                                     CPL_TYPE_FLOAT,
                                     "Mean value of the master dark");
    muse_processing_prepare_property(aHeader, "ESO QC DARK MASTER STDEV",
                                     CPL_TYPE_FLOAT,
                                     "Standard deviation of the master dark");
    muse_processing_prepare_property(aHeader, "ESO QC DARK MASTER MIN",
                                     CPL_TYPE_FLOAT,
                                     "Minimum value of the master dark");
    muse_processing_prepare_property(aHeader, "ESO QC DARK MASTER MAX",
                                     CPL_TYPE_FLOAT,
                                     "Maximum value of the master dark");
    muse_processing_prepare_property(aHeader, "ESO QC DARK MASTER DC",
                                     CPL_TYPE_FLOAT,
                                     "[count/pix/h] Dark current measured on master dark in randomly placed windows");
    muse_processing_prepare_property(aHeader, "ESO QC DARK MASTER DCERR",
                                     CPL_TYPE_FLOAT,
                                     "[count/pix/h] Dark current error measured on master dark in randomly placed windows");
    muse_processing_prepare_property(aHeader, "ESO QC DARK MASTER NSATURATED",
                                     CPL_TYPE_INT,
                                     "Number of saturated pixels in output data");
  } else if (!strcmp(aFrametag, "MODEL_DARK")) {
  } else {
    cpl_msg_warning(__func__, "Frame tag %s is not defined", aFrametag);
    return CPL_ERROR_ILLEGAL_INPUT;
  }
  return CPL_ERROR_NONE;
} /* muse_dark_prepare_header() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Return the level of an output frame.
  @param   aFrametag   tag of the output frame
  @return  the cpl_frame_level or CPL_FRAME_LEVEL_NONE on error

  @remark This function is also used to generate the recipe documentation.
 */
/*----------------------------------------------------------------------------*/
static cpl_frame_level
muse_dark_get_frame_level(const char *aFrametag)
{
  if (!aFrametag) {
    return CPL_FRAME_LEVEL_NONE;
  }
  if (!strcmp(aFrametag, "MASTER_DARK")) {
    return CPL_FRAME_LEVEL_FINAL;
  }
  if (!strcmp(aFrametag, "MODEL_DARK")) {
    return CPL_FRAME_LEVEL_FINAL;
  }
  return CPL_FRAME_LEVEL_NONE;
} /* muse_dark_get_frame_level() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Return the mode of an output frame.
  @param   aFrametag   tag of the output frame
  @return  the muse_frame_mode

  @remark This function is also used to generate the recipe documentation.
 */
/*----------------------------------------------------------------------------*/
static muse_frame_mode
muse_dark_get_frame_mode(const char *aFrametag)
{
  if (!aFrametag) {
    return MUSE_FRAME_MODE_ALL;
  }
  if (!strcmp(aFrametag, "MASTER_DARK")) {
    return MUSE_FRAME_MODE_MASTER;
  }
  if (!strcmp(aFrametag, "MODEL_DARK")) {
    return MUSE_FRAME_MODE_MASTER;
  }
  return MUSE_FRAME_MODE_ALL;
} /* muse_dark_get_frame_mode() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Setup the recipe options.
  @param   aPlugin   the plugin
  @return  0 if everything is ok, -1 if not called as part of a recipe.

  Define the command-line, configuration, and environment parameters for the
  recipe.
 */
/*----------------------------------------------------------------------------*/
static int
muse_dark_create(cpl_plugin *aPlugin)
{
  /* Check that the plugin is part of a valid recipe */
  cpl_recipe *recipe;
  if (cpl_plugin_get_type(aPlugin) == CPL_PLUGIN_TYPE_RECIPE) {
    recipe = (cpl_recipe *)aPlugin;
  } else {
    return -1;
  }

  /* register the extended processing information (new FITS header creation, *
   * getting of the frame level for a certain tag)                           */
  muse_processinginfo_register(recipe,
                               muse_dark_new_recipeconfig(),
                               muse_dark_prepare_header,
                               muse_dark_get_frame_level,
                               muse_dark_get_frame_mode);

  /* XXX initialize timing in messages                                       *
   *     since at least esorex is too stupid to turn it on, we have to do it */
  if (muse_cplframework() == MUSE_CPLFRAMEWORK_ESOREX) {
    cpl_msg_set_time_on();
  }

  /* Create the parameter list in the cpl_recipe object */
  recipe->parameters = cpl_parameterlist_new();
  /* Fill the parameters list */
  cpl_parameter *p;
      
  /* --nifu: IFU to handle. If set to 0, all IFUs are processed serially. If set to -1, all IFUs are processed in parallel. */
  p = cpl_parameter_new_range("muse.muse_dark.nifu",
                              CPL_TYPE_INT,
                             "IFU to handle. If set to 0, all IFUs are processed serially. If set to -1, all IFUs are processed in parallel.",
                              "muse.muse_dark",
                              (int)0,
                              (int)-1,
                              (int)24);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "nifu");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "nifu");

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --overscan: If this is "none", stop when detecting discrepant overscan levels (see ovscsigma), for "offset" it assumes that the mean overscan level represents the real offset in the bias levels of the exposures involved, and adjusts the data accordingly; for "vpoly", a polynomial is fit to the vertical overscan and subtracted from the whole quadrant. */
  p = cpl_parameter_new_value("muse.muse_dark.overscan",
                              CPL_TYPE_STRING,
                             "If this is \"none\", stop when detecting discrepant overscan levels (see ovscsigma), for \"offset\" it assumes that the mean overscan level represents the real offset in the bias levels of the exposures involved, and adjusts the data accordingly; for \"vpoly\", a polynomial is fit to the vertical overscan and subtracted from the whole quadrant.",
                              "muse.muse_dark",
                              (const char *)"vpoly");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "overscan");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "overscan");

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --ovscreject: This influences how values are rejected when computing overscan statistics. Either no rejection at all ("none"), rejection using the DCR algorithm ("dcr"), or rejection using an iterative constant fit ("fit"). */
  p = cpl_parameter_new_value("muse.muse_dark.ovscreject",
                              CPL_TYPE_STRING,
                             "This influences how values are rejected when computing overscan statistics. Either no rejection at all (\"none\"), rejection using the DCR algorithm (\"dcr\"), or rejection using an iterative constant fit (\"fit\").",
                              "muse.muse_dark",
                              (const char *)"dcr");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "ovscreject");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "ovscreject");

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --ovscsigma: If the deviation of mean overscan levels between a raw input image and the reference image is higher than |ovscsigma x stdev|, stop the processing. If overscan="vpoly", this is used as sigma rejection level for the iterative polynomial fit (the level comparison is then done afterwards with |100 x stdev| to guard against incompatible settings). Has no effect for overscan="offset". */
  p = cpl_parameter_new_value("muse.muse_dark.ovscsigma",
                              CPL_TYPE_DOUBLE,
                             "If the deviation of mean overscan levels between a raw input image and the reference image is higher than |ovscsigma x stdev|, stop the processing. If overscan=\"vpoly\", this is used as sigma rejection level for the iterative polynomial fit (the level comparison is then done afterwards with |100 x stdev| to guard against incompatible settings). Has no effect for overscan=\"offset\".",
                              "muse.muse_dark",
                              (double)30.);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "ovscsigma");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "ovscsigma");

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --ovscignore: The number of pixels of the overscan adjacent to the data section of the CCD that are ignored when computing statistics or fits. */
  p = cpl_parameter_new_value("muse.muse_dark.ovscignore",
                              CPL_TYPE_INT,
                             "The number of pixels of the overscan adjacent to the data section of the CCD that are ignored when computing statistics or fits.",
                              "muse.muse_dark",
                              (int)3);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "ovscignore");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "ovscignore");

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --combine: Type of image combination to use. */
  p = cpl_parameter_new_enum("muse.muse_dark.combine",
                             CPL_TYPE_STRING,
                             "Type of image combination to use.",
                             "muse.muse_dark",
                             (const char *)"sigclip",
                             4,
                             (const char *)"average",
                             (const char *)"median",
                             (const char *)"minmax",
                             (const char *)"sigclip");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "combine");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "combine");

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --nlow: Number of minimum pixels to reject with minmax. */
  p = cpl_parameter_new_value("muse.muse_dark.nlow",
                              CPL_TYPE_INT,
                             "Number of minimum pixels to reject with minmax.",
                              "muse.muse_dark",
                              (int)1);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "nlow");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "nlow");

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --nhigh: Number of maximum pixels to reject with minmax. */
  p = cpl_parameter_new_value("muse.muse_dark.nhigh",
                              CPL_TYPE_INT,
                             "Number of maximum pixels to reject with minmax.",
                              "muse.muse_dark",
                              (int)1);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "nhigh");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "nhigh");

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --nkeep: Number of pixels to keep with minmax. */
  p = cpl_parameter_new_value("muse.muse_dark.nkeep",
                              CPL_TYPE_INT,
                             "Number of pixels to keep with minmax.",
                              "muse.muse_dark",
                              (int)1);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "nkeep");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "nkeep");

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --lsigma: Low sigma for pixel rejection with sigclip. */
  p = cpl_parameter_new_value("muse.muse_dark.lsigma",
                              CPL_TYPE_DOUBLE,
                             "Low sigma for pixel rejection with sigclip.",
                              "muse.muse_dark",
                              (double)3);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "lsigma");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "lsigma");

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --hsigma: High sigma for pixel rejection with sigclip. */
  p = cpl_parameter_new_value("muse.muse_dark.hsigma",
                              CPL_TYPE_DOUBLE,
                             "High sigma for pixel rejection with sigclip.",
                              "muse.muse_dark",
                              (double)3);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "hsigma");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "hsigma");

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --scale: Scale the individual images to a common exposure time before combining them. */
  p = cpl_parameter_new_value("muse.muse_dark.scale",
                              CPL_TYPE_BOOL,
                             "Scale the individual images to a common exposure time before combining them.",
                              "muse.muse_dark",
                              (int)TRUE);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "scale");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "scale");
  if (!getenv("MUSE_EXPERT_USER")) {
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_CLI);
  }

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --normalize: Normalize the master dark to this exposure time (in seconds). To disable normalization, set this to a negative value. */
  p = cpl_parameter_new_value("muse.muse_dark.normalize",
                              CPL_TYPE_DOUBLE,
                             "Normalize the master dark to this exposure time (in seconds). To disable normalization, set this to a negative value.",
                              "muse.muse_dark",
                              (double)3600.);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "normalize");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "normalize");
  if (!getenv("MUSE_EXPERT_USER")) {
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_CLI);
  }

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --hotsigma: Sigma level, in terms of median deviation above the median dark level, above which a pixel is detected and marked as 'hot'. */
  p = cpl_parameter_new_value("muse.muse_dark.hotsigma",
                              CPL_TYPE_DOUBLE,
                             "Sigma level, in terms of median deviation above the median dark level, above which a pixel is detected and marked as 'hot'.",
                              "muse.muse_dark",
                              (double)5);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "hotsigma");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "hotsigma");

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --model: Model the master dark using a set of polynomials. */
  p = cpl_parameter_new_value("muse.muse_dark.model",
                              CPL_TYPE_BOOL,
                             "Model the master dark using a set of polynomials.",
                              "muse.muse_dark",
                              (int)FALSE);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "model");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "model");

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --merge: Merge output products from different IFUs into a common file. */
  p = cpl_parameter_new_value("muse.muse_dark.merge",
                              CPL_TYPE_BOOL,
                             "Merge output products from different IFUs into a common file.",
                              "muse.muse_dark",
                              (int)FALSE);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "merge");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "merge");

  cpl_parameterlist_append(recipe->parameters, p);
    
  return 0;
} /* muse_dark_create() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Fill the recipe parameters into the parameter structure
  @param   aParams       the recipe-internal structure to fill
  @param   aParameters   the cpl_parameterlist with the parameters
  @return  0 if everything is ok, -1 if something went wrong.

  This is a convienience function that centrally fills all parameters into the
  according fields of the recipe internal structure.
 */
/*----------------------------------------------------------------------------*/
static int
muse_dark_params_fill(muse_dark_params_t *aParams, cpl_parameterlist *aParameters)
{
  cpl_ensure_code(aParams, CPL_ERROR_NULL_INPUT);
  cpl_ensure_code(aParameters, CPL_ERROR_NULL_INPUT);
  cpl_parameter *p;
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_dark.nifu");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->nifu = cpl_parameter_get_int(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_dark.overscan");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->overscan = cpl_parameter_get_string(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_dark.ovscreject");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->ovscreject = cpl_parameter_get_string(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_dark.ovscsigma");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->ovscsigma = cpl_parameter_get_double(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_dark.ovscignore");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->ovscignore = cpl_parameter_get_int(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_dark.combine");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->combine_s = cpl_parameter_get_string(p);
  aParams->combine =
    (!strcasecmp(aParams->combine_s, "average")) ? MUSE_DARK_PARAM_COMBINE_AVERAGE :
    (!strcasecmp(aParams->combine_s, "median")) ? MUSE_DARK_PARAM_COMBINE_MEDIAN :
    (!strcasecmp(aParams->combine_s, "minmax")) ? MUSE_DARK_PARAM_COMBINE_MINMAX :
    (!strcasecmp(aParams->combine_s, "sigclip")) ? MUSE_DARK_PARAM_COMBINE_SIGCLIP :
      MUSE_DARK_PARAM_COMBINE_INVALID_VALUE;
  cpl_ensure_code(aParams->combine != MUSE_DARK_PARAM_COMBINE_INVALID_VALUE,
                  CPL_ERROR_ILLEGAL_INPUT);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_dark.nlow");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->nlow = cpl_parameter_get_int(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_dark.nhigh");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->nhigh = cpl_parameter_get_int(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_dark.nkeep");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->nkeep = cpl_parameter_get_int(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_dark.lsigma");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->lsigma = cpl_parameter_get_double(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_dark.hsigma");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->hsigma = cpl_parameter_get_double(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_dark.scale");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->scale = cpl_parameter_get_bool(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_dark.normalize");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->normalize = cpl_parameter_get_double(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_dark.hotsigma");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->hotsigma = cpl_parameter_get_double(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_dark.model");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->model = cpl_parameter_get_bool(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_dark.merge");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->merge = cpl_parameter_get_bool(p);
    
  return 0;
} /* muse_dark_params_fill() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief    Execute the plugin instance given by the interface
  @param    aPlugin   the plugin
  @return   0 if everything is ok, -1 if not called as part of a recipe
 */
/*----------------------------------------------------------------------------*/
static int
muse_dark_exec(cpl_plugin *aPlugin)
{
  if (cpl_plugin_get_type(aPlugin) != CPL_PLUGIN_TYPE_RECIPE) {
    return -1;
  }
  muse_processing_recipeinfo(aPlugin);
  cpl_recipe *recipe = (cpl_recipe *)aPlugin;
  cpl_msg_set_threadid_on();

  cpl_frameset *usedframes = cpl_frameset_new(),
               *outframes = cpl_frameset_new();
  muse_dark_params_t params;
  muse_dark_params_fill(&params, recipe->parameters);

  cpl_errorstate prestate = cpl_errorstate_get();

  if (params.nifu < -1 || params.nifu > kMuseNumIFUs) {
    cpl_msg_error(__func__, "Please specify a valid IFU number (between 1 and "
                  "%d), 0 (to process all IFUs consecutively), or -1 (to "
                  "process all IFUs in parallel) using --nifu.", kMuseNumIFUs);
    return -1;
  } /* if invalid params.nifu */

  cpl_boolean donotmerge = CPL_FALSE; /* depending on nifu we may not merge */
  int rc = 0;
  if (params.nifu > 0) {
    muse_processing *proc = muse_processing_new("muse_dark",
                                                recipe);
    rc = muse_dark_compute(proc, &params);
    cpl_frameset_join(usedframes, proc->usedframes);
    cpl_frameset_join(outframes, proc->outframes);
    muse_processing_delete(proc);
    donotmerge = CPL_TRUE; /* after processing one IFU, merging cannot work */
  } else if (params.nifu < 0) { /* parallel processing */
    int *rcs = cpl_calloc(kMuseNumIFUs, sizeof(int));
    int nifu;
    // FIXME: Removed default(none) clause here, to make the code work with
    //        gcc9 while keeping older versions of gcc working too without
    //        resorting to conditional compilation. Having default(none) here
    //        causes gcc9 to abort the build because of __func__ not being
    //        specified in a data sharing clause. Adding a data sharing clause
    //        makes older gcc versions choke.
    #pragma omp parallel for                                                   \
            shared(outframes, params, rcs, recipe, usedframes)
    for (nifu = 1; nifu <= kMuseNumIFUs; nifu++) {
      muse_processing *proc = muse_processing_new("muse_dark",
                                                  recipe);
      muse_dark_params_t *pars = cpl_malloc(sizeof(muse_dark_params_t));
      memcpy(pars, &params, sizeof(muse_dark_params_t));
      pars->nifu = nifu;
      int *rci = rcs + (nifu - 1);
      *rci = muse_dark_compute(proc, pars);
      if (rci && (int)cpl_error_get_code() == MUSE_ERROR_CHIP_NOT_LIVE) {
        *rci = 0;
      }
      cpl_free(pars);
      #pragma omp critical(muse_processing_used_frames)
      cpl_frameset_join(usedframes, proc->usedframes);
      #pragma omp critical(muse_processing_output_frames)
      cpl_frameset_join(outframes, proc->outframes);
      muse_processing_delete(proc);
    } /* for nifu */
    /* non-parallel loop to propagate the "worst" return code;       *
     * since we only ever return -1, any non-zero code is propagated */
    for (nifu = 1; nifu <= kMuseNumIFUs; nifu++) {
      if (rcs[nifu-1] != 0) {
        rc = rcs[nifu-1];
      } /* if */
    } /* for nifu */
    cpl_free(rcs);
  } else { /* serial processing */
    for (params.nifu = 1; params.nifu <= kMuseNumIFUs && !rc; params.nifu++) {
      muse_processing *proc = muse_processing_new("muse_dark",
                                                  recipe);
      rc = muse_dark_compute(proc, &params);
      if (rc && (int)cpl_error_get_code() == MUSE_ERROR_CHIP_NOT_LIVE) {
        rc = 0;
      }
      cpl_frameset_join(usedframes, proc->usedframes);
      cpl_frameset_join(outframes, proc->outframes);
      muse_processing_delete(proc);
    } /* for nifu */
  } /* else */
  UNUSED_ARGUMENT(donotmerge); /* maybe this is not going to be used below */

  if (!cpl_errorstate_is_equal(prestate)) {
    /* dump all errors from this recipe in chronological order */
    cpl_errorstate_dump(prestate, CPL_FALSE, muse_cplerrorstate_dump_some);
    /* reset message level to not get the same errors displayed again by esorex */
    cpl_msg_set_level(CPL_MSG_INFO);
  }
  /* clean up duplicates in framesets of used and output frames */
  muse_cplframeset_erase_duplicate(usedframes);
  muse_cplframeset_erase_duplicate(outframes);

  /* merge output products from the up to 24 IFUs */
  if (params.merge && !donotmerge) {
    muse_utils_frameset_merge_frames(outframes, CPL_TRUE);
  }

  /* to get esorex to see our classification (frame groups etc.), *
   * replace the original frameset with the list of used frames   *
   * before appending product output frames                       */
  /* keep the same pointer, so just erase all frames, not delete the frameset */
  muse_cplframeset_erase_all(recipe->frames);
  cpl_frameset_join(recipe->frames, usedframes);
  cpl_frameset_join(recipe->frames, outframes);
  cpl_frameset_delete(usedframes);
  cpl_frameset_delete(outframes);
  return rc;
} /* muse_dark_exec() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief    Destroy what has been created by the 'create' function
  @param    aPlugin   the plugin
  @return   0 if everything is ok, -1 if not called as part of a recipe
 */
/*----------------------------------------------------------------------------*/
static int
muse_dark_destroy(cpl_plugin *aPlugin)
{
  /* Get the recipe from the plugin */
  cpl_recipe *recipe;
  if (cpl_plugin_get_type(aPlugin) == CPL_PLUGIN_TYPE_RECIPE) {
    recipe = (cpl_recipe *)aPlugin;
  } else {
    return -1;
  }

  /* Clean up */
  cpl_parameterlist_delete(recipe->parameters);
  muse_processinginfo_delete(recipe);
  return 0;
} /* muse_dark_destroy() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief    Add this recipe to the list of available plugins.
  @param    aList   the plugin list
  @return   0 if everything is ok, -1 otherwise (but this cannot happen)

  Create the recipe instance and make it available to the application using the
  interface. This function is exported.
 */
/*----------------------------------------------------------------------------*/
int
cpl_plugin_get_info(cpl_pluginlist *aList)
{
  cpl_recipe *recipe = cpl_calloc(1, sizeof *recipe);
  cpl_plugin *plugin = &recipe->interface;

  char *helptext;
  if (muse_cplframework() == MUSE_CPLFRAMEWORK_ESOREX) {
    helptext = cpl_sprintf("%s%s", muse_dark_help,
                           muse_dark_help_esorex);
  } else {
    helptext = cpl_sprintf("%s", muse_dark_help);
  }

  /* Initialize the CPL plugin stuff for this module */
  cpl_plugin_init(plugin, CPL_PLUGIN_API, MUSE_BINARY_VERSION,
                  CPL_PLUGIN_TYPE_RECIPE,
                  "muse_dark",
                  "Combine several separate dark images into one master dark file and locate hot pixels.",
                  helptext,
                  "Peter Weilbacher",
                  "https://support.eso.org",
                  muse_get_license(),
                  muse_dark_create,
                  muse_dark_exec,
                  muse_dark_destroy);
  cpl_pluginlist_append(aList, plugin);
  cpl_free(helptext);

  return 0;
} /* cpl_plugin_get_info() */

/**@}*/
/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/* 
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* This file was automatically generated */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*----------------------------------------------------------------------------*
 *                              Includes                                      *
 *----------------------------------------------------------------------------*/
#include <string.h> /* strcmp(), strstr() */
#include <strings.h> /* strcasecmp() */
#include <cpl.h>

#include "muse_scipost_raman_z.h" /* in turn includes muse.h */

/*----------------------------------------------------------------------------*/
/**
  @defgroup recipe_muse_scipost_raman         Recipe muse_scipost_raman: Remove of 4GLSF Raman emission.
  @author Ole Streicher
  
        This recipe removes the Raman scattered light of the 4GLSF system from
        the exposure. It must be before the normal sky subtraction.
      
 */
/*----------------------------------------------------------------------------*/
/**@{*/

/*----------------------------------------------------------------------------*
 *                         Static variables                                   *
 *----------------------------------------------------------------------------*/
static const char *muse_scipost_raman_help =
  "This recipe removes the Raman scattered light of the 4GLSF system from the exposure. It must be before the normal sky subtraction.";

static const char *muse_scipost_raman_help_esorex =
  "\n\nInput frames for raw frame tag \"PIXTABLE_OBJECT\":\n"
  "\n Frame tag            Type Req #Fr Description"
  "\n -------------------- ---- --- --- ------------"
  "\n PIXTABLE_OBJECT      raw   Y    1 Input pixel table"
  "\n RAMAN_LINES          calib Y    1 List of Raman emission lines"
  "\n LSF_PROFILE          calib Y      Slice specific LSF parameters cubes"
  "\n SKY_MASK             calib Y    1 Sky mask to use"
  "\n\nProduct frames for raw frame tag \"PIXTABLE_OBJECT\":\n"
  "\n Frame tag            Level    Description"
  "\n -------------------- -------- ------------"
  "\n RAMAN_IMAGES         intermed Images for Raman correction diagnostics (if an input RAMAN_LINES was given, the instrument used AO and --save contains \"raman\"). Extensions are: DATA: model of the Raman light distribution in the field of view (arbitrary units), RAMAN_IMAGE_O2: reconstructed image in the O2 band, SKY_MASK_O2: sky mask used for the O2 band, RAMAN_IMAGE_N2: reconstructed image in the N2 band, SKY_MASK_N2: sky mask used for the N2 band, RAMAN_FIT_O2: model of Raman flux distribution in the O2 band, RAMAN_FIT_N2: model of Raman flux distribution in the N2 band."
  "\n PIXTABLE_REDUCED     final    Output pixel table for raman subtraction.";

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Create the recipe config for this plugin.

  @remark The recipe config is used to check for the tags as well as to create
          the documentation of this plugin.
 */
/*----------------------------------------------------------------------------*/
static cpl_recipeconfig *
muse_scipost_raman_new_recipeconfig(void)
{
  cpl_recipeconfig *recipeconfig = cpl_recipeconfig_new();
      
  cpl_recipeconfig_set_tag(recipeconfig, "PIXTABLE_OBJECT", 1, 1);
  cpl_recipeconfig_set_input(recipeconfig, "PIXTABLE_OBJECT", "RAMAN_LINES", 1, 1);
  cpl_recipeconfig_set_input(recipeconfig, "PIXTABLE_OBJECT", "LSF_PROFILE", 1, -1);
  cpl_recipeconfig_set_input(recipeconfig, "PIXTABLE_OBJECT", "SKY_MASK", 1, 1);
  cpl_recipeconfig_set_output(recipeconfig, "PIXTABLE_OBJECT", "RAMAN_IMAGES");
  cpl_recipeconfig_set_output(recipeconfig, "PIXTABLE_OBJECT", "PIXTABLE_REDUCED");
    
  return recipeconfig;
} /* muse_scipost_raman_new_recipeconfig() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Return a new header that shall be filled on output.
  @param   aFrametag   tag of the output frame
  @param   aHeader     the prepared FITS header
  @return  CPL_ERROR_NONE on success another cpl_error_code on error

  @remark This function is also used to generate the recipe documentation.
 */
/*----------------------------------------------------------------------------*/
static cpl_error_code
muse_scipost_raman_prepare_header(const char *aFrametag, cpl_propertylist *aHeader)
{
  cpl_ensure_code(aFrametag, CPL_ERROR_NULL_INPUT);
  cpl_ensure_code(aHeader, CPL_ERROR_NULL_INPUT);
  if (!strcmp(aFrametag, "RAMAN_IMAGES")) {
    muse_processing_prepare_property(aHeader, "ESO QC SCIPOST RAMAN SPATIAL XX",
                                     CPL_TYPE_DOUBLE,
                                     "2D Polynomial x^2 coefficient");
    muse_processing_prepare_property(aHeader, "ESO QC SCIPOST RAMAN SPATIAL XY",
                                     CPL_TYPE_DOUBLE,
                                     "2D Polynomial xy coefficient");
    muse_processing_prepare_property(aHeader, "ESO QC SCIPOST RAMAN SPATIAL YY",
                                     CPL_TYPE_DOUBLE,
                                     "2D Polynomial y^2 coefficient");
    muse_processing_prepare_property(aHeader, "ESO QC SCIPOST RAMAN SPATIAL X",
                                     CPL_TYPE_DOUBLE,
                                     "2D Polynomial x coefficient");
    muse_processing_prepare_property(aHeader, "ESO QC SCIPOST RAMAN SPATIAL Y",
                                     CPL_TYPE_DOUBLE,
                                     "2D Polynomial y coefficient");
    muse_processing_prepare_property(aHeader, "ESO QC SCIPOST RAMAN FLUX O2",
                                     CPL_TYPE_FLOAT,
                                     "[erg/(s cm2 arcsec2)] Computed average Raman scattered flux in the O2 band (around 6484 Angstrom)");
    muse_processing_prepare_property(aHeader, "ESO QC SCIPOST RAMAN FLUX N2",
                                     CPL_TYPE_FLOAT,
                                     "[erg/(s cm2 arcsec2)] Computed average Raman scattered flux in the N2 band (around 6827 Angstrom)");
  } else if (!strcmp(aFrametag, "PIXTABLE_REDUCED")) {
  } else {
    cpl_msg_warning(__func__, "Frame tag %s is not defined", aFrametag);
    return CPL_ERROR_ILLEGAL_INPUT;
  }
  return CPL_ERROR_NONE;
} /* muse_scipost_raman_prepare_header() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Return the level of an output frame.
  @param   aFrametag   tag of the output frame
  @return  the cpl_frame_level or CPL_FRAME_LEVEL_NONE on error

  @remark This function is also used to generate the recipe documentation.
 */
/*----------------------------------------------------------------------------*/
static cpl_frame_level
muse_scipost_raman_get_frame_level(const char *aFrametag)
{
  if (!aFrametag) {
    return CPL_FRAME_LEVEL_NONE;
  }
  if (!strcmp(aFrametag, "RAMAN_IMAGES")) {
    return CPL_FRAME_LEVEL_INTERMEDIATE;
  }
  if (!strcmp(aFrametag, "PIXTABLE_REDUCED")) {
    return CPL_FRAME_LEVEL_FINAL;
  }
  return CPL_FRAME_LEVEL_NONE;
} /* muse_scipost_raman_get_frame_level() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Return the mode of an output frame.
  @param   aFrametag   tag of the output frame
  @return  the muse_frame_mode

  @remark This function is also used to generate the recipe documentation.
 */
/*----------------------------------------------------------------------------*/
static muse_frame_mode
muse_scipost_raman_get_frame_mode(const char *aFrametag)
{
  if (!aFrametag) {
    return MUSE_FRAME_MODE_ALL;
  }
  if (!strcmp(aFrametag, "RAMAN_IMAGES")) {
    return MUSE_FRAME_MODE_DATEOBS;
  }
  if (!strcmp(aFrametag, "PIXTABLE_REDUCED")) {
    return MUSE_FRAME_MODE_MASTER;
  }
  return MUSE_FRAME_MODE_ALL;
} /* muse_scipost_raman_get_frame_mode() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Setup the recipe options.
  @param   aPlugin   the plugin
  @return  0 if everything is ok, -1 if not called as part of a recipe.

  Define the command-line, configuration, and environment parameters for the
  recipe.
 */
/*----------------------------------------------------------------------------*/
static int
muse_scipost_raman_create(cpl_plugin *aPlugin)
{
  /* Check that the plugin is part of a valid recipe */
  cpl_recipe *recipe;
  if (cpl_plugin_get_type(aPlugin) == CPL_PLUGIN_TYPE_RECIPE) {
    recipe = (cpl_recipe *)aPlugin;
  } else {
    return -1;
  }

  /* register the extended processing information (new FITS header creation, *
   * getting of the frame level for a certain tag)                           */
  muse_processinginfo_register(recipe,
                               muse_scipost_raman_new_recipeconfig(),
                               muse_scipost_raman_prepare_header,
                               muse_scipost_raman_get_frame_level,
                               muse_scipost_raman_get_frame_mode);

  /* XXX initialize timing in messages                                       *
   *     since at least esorex is too stupid to turn it on, we have to do it */
  if (muse_cplframework() == MUSE_CPLFRAMEWORK_ESOREX) {
    cpl_msg_set_time_on();
  }

  /* Create the parameter list in the cpl_recipe object */
  recipe->parameters = cpl_parameterlist_new();
  /* Fill the parameters list */
  cpl_parameter *p;
      
  /* --lambdamin: Cut off the data below this wavelength after loading the pixel table(s). */
  p = cpl_parameter_new_value("muse.muse_scipost_raman.lambdamin",
                              CPL_TYPE_DOUBLE,
                             "Cut off the data below this wavelength after loading the pixel table(s).",
                              "muse.muse_scipost_raman",
                              (double)4000.);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "lambdamin");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "lambdamin");

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --lambdamax: Cut off the data above this wavelength after loading the pixel table(s). */
  p = cpl_parameter_new_value("muse.muse_scipost_raman.lambdamax",
                              CPL_TYPE_DOUBLE,
                             "Cut off the data above this wavelength after loading the pixel table(s).",
                              "muse.muse_scipost_raman",
                              (double)10000.);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "lambdamax");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "lambdamax");

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --lambdaref: Reference wavelength used for correction of differential atmospheric refraction. The R-band (peak wavelength ~7000 Angstrom) that is usually used for guiding, is close to the central wavelength of MUSE, so a value of 7000.0 Angstrom should be used if nothing else is known. A value less than zero switches DAR correction off. */
  p = cpl_parameter_new_value("muse.muse_scipost_raman.lambdaref",
                              CPL_TYPE_DOUBLE,
                             "Reference wavelength used for correction of differential atmospheric refraction. The R-band (peak wavelength ~7000 Angstrom) that is usually used for guiding, is close to the central wavelength of MUSE, so a value of 7000.0 Angstrom should be used if nothing else is known. A value less than zero switches DAR correction off.",
                              "muse.muse_scipost_raman",
                              (double)7000.);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "lambdaref");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "lambdaref");

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --width: Wavelength range around Raman lines [Angstrom]. */
  p = cpl_parameter_new_value("muse.muse_scipost_raman.width",
                              CPL_TYPE_DOUBLE,
                             "Wavelength range around Raman lines [Angstrom].",
                              "muse.muse_scipost_raman",
                              (double)20.);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "width");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "width");
  if (!getenv("MUSE_EXPERT_USER")) {
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_CLI);
  }

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --crsigma: Sigma level clipping for cube-based CR rejection (using "median", see muse_scipost). It can be switched off, by passing zero or a negative value. */
  p = cpl_parameter_new_value("muse.muse_scipost_raman.crsigma",
                              CPL_TYPE_DOUBLE,
                             "Sigma level clipping for cube-based CR rejection (using \"median\", see muse_scipost). It can be switched off, by passing zero or a negative value.",
                              "muse.muse_scipost_raman",
                              (double)15.);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "crsigma");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "crsigma");
  if (!getenv("MUSE_EXPERT_USER")) {
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_CLI);
  }

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --fraction: Fraction of the image (without the ignored part) to be considered as sky. If an input sky mask is provided, the fraction is applied to the regions within the mask. If the whole sky mask should be used, set this parameter to 1. */
  p = cpl_parameter_new_value("muse.muse_scipost_raman.fraction",
                              CPL_TYPE_DOUBLE,
                             "Fraction of the image (without the ignored part) to be considered as sky. If an input sky mask is provided, the fraction is applied to the regions within the mask. If the whole sky mask should be used, set this parameter to 1.",
                              "muse.muse_scipost_raman",
                              (double)0.75);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "fraction");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "fraction");

  cpl_parameterlist_append(recipe->parameters, p);
      
  /* --ignore: Lowest fraction of the image to be ignored. If an input sky mask is provided, the fraction is applied to the regions within the mask. If the whole sky mask should be used, set this parameter to 0. */
  p = cpl_parameter_new_value("muse.muse_scipost_raman.ignore",
                              CPL_TYPE_DOUBLE,
                             "Lowest fraction of the image to be ignored. If an input sky mask is provided, the fraction is applied to the regions within the mask. If the whole sky mask should be used, set this parameter to 0.",
                              "muse.muse_scipost_raman",
                              (double)0.05);
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CFG, "ignore");
  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "ignore");

  cpl_parameterlist_append(recipe->parameters, p);
    
  return 0;
} /* muse_scipost_raman_create() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief   Fill the recipe parameters into the parameter structure
  @param   aParams       the recipe-internal structure to fill
  @param   aParameters   the cpl_parameterlist with the parameters
  @return  0 if everything is ok, -1 if something went wrong.

  This is a convienience function that centrally fills all parameters into the
  according fields of the recipe internal structure.
 */
/*----------------------------------------------------------------------------*/
static int
muse_scipost_raman_params_fill(muse_scipost_raman_params_t *aParams, cpl_parameterlist *aParameters)
{
  cpl_ensure_code(aParams, CPL_ERROR_NULL_INPUT);
  cpl_ensure_code(aParameters, CPL_ERROR_NULL_INPUT);
  cpl_parameter *p;
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_scipost_raman.lambdamin");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->lambdamin = cpl_parameter_get_double(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_scipost_raman.lambdamax");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->lambdamax = cpl_parameter_get_double(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_scipost_raman.lambdaref");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->lambdaref = cpl_parameter_get_double(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_scipost_raman.width");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->width = cpl_parameter_get_double(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_scipost_raman.crsigma");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->crsigma = cpl_parameter_get_double(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_scipost_raman.fraction");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->fraction = cpl_parameter_get_double(p);
      
  p = cpl_parameterlist_find(aParameters, "muse.muse_scipost_raman.ignore");
  cpl_ensure_code(p, CPL_ERROR_DATA_NOT_FOUND);
  aParams->ignore = cpl_parameter_get_double(p);
    
  return 0;
} /* muse_scipost_raman_params_fill() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief    Execute the plugin instance given by the interface
  @param    aPlugin   the plugin
  @return   0 if everything is ok, -1 if not called as part of a recipe
 */
/*----------------------------------------------------------------------------*/
static int
muse_scipost_raman_exec(cpl_plugin *aPlugin)
{
  if (cpl_plugin_get_type(aPlugin) != CPL_PLUGIN_TYPE_RECIPE) {
    return -1;
  }
  muse_processing_recipeinfo(aPlugin);
  cpl_recipe *recipe = (cpl_recipe *)aPlugin;
  cpl_msg_set_threadid_on();

  cpl_frameset *usedframes = cpl_frameset_new(),
               *outframes = cpl_frameset_new();
  muse_scipost_raman_params_t params;
  muse_scipost_raman_params_fill(&params, recipe->parameters);

  cpl_errorstate prestate = cpl_errorstate_get();

  muse_processing *proc = muse_processing_new("muse_scipost_raman",
                                              recipe);
  int rc = muse_scipost_raman_compute(proc, &params);
  cpl_frameset_join(usedframes, proc->usedframes);
  cpl_frameset_join(outframes, proc->outframes);
  muse_processing_delete(proc);
  
  if (!cpl_errorstate_is_equal(prestate)) {
    /* dump all errors from this recipe in chronological order */
    cpl_errorstate_dump(prestate, CPL_FALSE, muse_cplerrorstate_dump_some);
    /* reset message level to not get the same errors displayed again by esorex */
    cpl_msg_set_level(CPL_MSG_INFO);
  }
  /* clean up duplicates in framesets of used and output frames */
  muse_cplframeset_erase_duplicate(usedframes);
  muse_cplframeset_erase_duplicate(outframes);

  /* to get esorex to see our classification (frame groups etc.), *
   * replace the original frameset with the list of used frames   *
   * before appending product output frames                       */
  /* keep the same pointer, so just erase all frames, not delete the frameset */
  muse_cplframeset_erase_all(recipe->frames);
  cpl_frameset_join(recipe->frames, usedframes);
  cpl_frameset_join(recipe->frames, outframes);
  cpl_frameset_delete(usedframes);
  cpl_frameset_delete(outframes);
  return rc;
} /* muse_scipost_raman_exec() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief    Destroy what has been created by the 'create' function
  @param    aPlugin   the plugin
  @return   0 if everything is ok, -1 if not called as part of a recipe
 */
/*----------------------------------------------------------------------------*/
static int
muse_scipost_raman_destroy(cpl_plugin *aPlugin)
{
  /* Get the recipe from the plugin */
  cpl_recipe *recipe;
  if (cpl_plugin_get_type(aPlugin) == CPL_PLUGIN_TYPE_RECIPE) {
    recipe = (cpl_recipe *)aPlugin;
  } else {
    return -1;
  }

  /* Clean up */
  cpl_parameterlist_delete(recipe->parameters);
  muse_processinginfo_delete(recipe);
  return 0;
} /* muse_scipost_raman_destroy() */

/*----------------------------------------------------------------------------*/
/**
  @private
  @brief    Add this recipe to the list of available plugins.
  @param    aList   the plugin list
  @return   0 if everything is ok, -1 otherwise (but this cannot happen)

  Create the recipe instance and make it available to the application using the
  interface. This function is exported.
 */
/*----------------------------------------------------------------------------*/
int
cpl_plugin_get_info(cpl_pluginlist *aList)
{
  cpl_recipe *recipe = cpl_calloc(1, sizeof *recipe);
  cpl_plugin *plugin = &recipe->interface;

  char *helptext;
  if (muse_cplframework() == MUSE_CPLFRAMEWORK_ESOREX) {
    helptext = cpl_sprintf("%s%s", muse_scipost_raman_help,
                           muse_scipost_raman_help_esorex);
  } else {
    helptext = cpl_sprintf("%s", muse_scipost_raman_help);
  }

  /* Initialize the CPL plugin stuff for this module */
  cpl_plugin_init(plugin, CPL_PLUGIN_API, MUSE_BINARY_VERSION,
                  CPL_PLUGIN_TYPE_RECIPE,
                  "muse_scipost_raman",
                  "Remove of 4GLSF Raman emission.",
                  helptext,
                  "Ole Streicher",
                  "https://support.eso.org",
                  muse_get_license(),
                  muse_scipost_raman_create,
                  muse_scipost_raman_exec,
                  muse_scipost_raman_destroy);
  cpl_pluginlist_append(aList, plugin);
  cpl_free(helptext);

  return 0;
} /* cpl_plugin_get_info() */

/**@}*/
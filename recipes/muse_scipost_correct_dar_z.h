/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/* 
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* This file was automatically generated */

#ifndef MUSE_SCIPOST_CORRECT_DAR_Z_H
#define MUSE_SCIPOST_CORRECT_DAR_Z_H

/*----------------------------------------------------------------------------*
 *                              Includes                                      *
 *----------------------------------------------------------------------------*/
#include <muse.h>
#include <muse_instrument.h>

/*----------------------------------------------------------------------------*
 *                          Special variable types                            *
 *----------------------------------------------------------------------------*/

/** @addtogroup recipe_muse_scipost_correct_dar */
/**@{*/

/*----------------------------------------------------------------------------*/
/**
  @brief   Structure to hold the parameters of the muse_scipost_correct_dar recipe.

  This structure contains the parameters for the recipe that may be set on the
  command line, in the configuration, or through the environment.
 */
/*----------------------------------------------------------------------------*/
typedef struct muse_scipost_correct_dar_params_s {
  /** @brief   Cut off the data below this wavelength after loading the pixel table(s). */
  double lambdamin;

  /** @brief   Cut off the data above this wavelength after loading the pixel table(s). */
  double lambdamax;

  /** @brief   Reference wavelength used for correction of differential atmospheric refraction. The R-band (peak wavelength ~7000 Angstrom) that is usually used for guiding, is close to the central wavelength of MUSE, so a value of 7000.0 Angstrom should be used if nothing else is known. A value less than zero switches DAR correction off. */
  double lambdaref;

  /** @brief   Carry out a check of the theoretical DAR correction using source centroiding. If "correct" it will also apply an empirical correction. */
  int darcheck;
  /** @brief   Carry out a check of the theoretical DAR correction using source centroiding. If "correct" it will also apply an empirical correction. (as string) */
  const char *darcheck_s;

  char __dummy__; /* quieten compiler warning about possibly empty struct */
} muse_scipost_correct_dar_params_t;

#define MUSE_SCIPOST_CORRECT_DAR_PARAM_DARCHECK_NONE 1
#define MUSE_SCIPOST_CORRECT_DAR_PARAM_DARCHECK_CHECK 2
#define MUSE_SCIPOST_CORRECT_DAR_PARAM_DARCHECK_CORRECT 3
#define MUSE_SCIPOST_CORRECT_DAR_PARAM_DARCHECK_INVALID_VALUE -1

/**@}*/

/*----------------------------------------------------------------------------*
 *                           Function prototypes                              *
 *----------------------------------------------------------------------------*/
int muse_scipost_correct_dar_compute(muse_processing *, muse_scipost_correct_dar_params_t *);

#endif /* MUSE_SCIPOST_CORRECT_DAR_Z_H */

/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/*
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2014 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*---------------------------------------------------------------------------*
 *                             Includes                                      *
 *---------------------------------------------------------------------------*/
#include <muse.h>
#include "muse_scipost_combine_pixtables_z.h"

/*----------------------------------------------------------------------------*/
/**
  @brief    Combine several pixel tables and exposures
  @param    aProcessing  the processing structure
  @param    aParams      the parameters list
  @return   0 if everything is ok, -1 something went wrong

  @see muse_scipost_compute()
 */
/*----------------------------------------------------------------------------*/
int
muse_scipost_combine_pixtables_compute(muse_processing *aProcessing,
                                       muse_scipost_combine_pixtables_params_t *aParams)
{
  /* setup and check weighting scheme */
  muse_xcombine_types weight = muse_postproc_get_weight_type(aParams->weight_s);

  /* sort input pixel tables into different exposures */
  cpl_table *exposures = muse_processing_sort_exposures(aProcessing);
  if (!exposures) {
    cpl_msg_error(__func__, "no science exposures found in input");
    return -1;
  }
  int nexposures = cpl_table_get_nrow(exposures);
  /* load the optional offsets table */
  cpl_table *offsets = muse_processing_load_ctable(aProcessing, MUSE_TAG_OFFSET_LIST, 0);
  if (offsets && muse_cpltable_check(offsets, muse_offset_list_def) != CPL_ERROR_NONE) {
    cpl_msg_warning(__func__, "Input %s has unexpected format, proceeding "
                    "without offset and flux scales!", MUSE_TAG_OFFSET_LIST);
    cpl_table_delete(offsets);
    offsets = NULL;
  } /* if offsets have wrong format */

  /* now process all the pixel tables, do it separately for each exposure */
  /* allocate one additional element for NULL termination */
  muse_pixtable **pixtables = cpl_calloc(nexposures + 1, sizeof(muse_pixtable *));
  int i;
  for (i = 0; i < nexposures; i++) {
    cpl_table *thisexp = cpl_table_extract(exposures, i, 1);
    pixtables[i] = muse_pixtable_load_merge_channels(thisexp,
                                                     aParams->lambdamin,
                                                     aParams->lambdamax);
    cpl_table_delete(thisexp);
    if (pixtables[i]) {
      /* erase pre-existing QC parameters */
      cpl_propertylist_erase_regexp(pixtables[i]->header, "ESO QC ", 0);
    } /* if */
  }
  cpl_table_delete(exposures);

  do { /* loop to break out of in case of error */
    /* now combine the possibly more than one exposures */
    muse_pixtable *bigpixtable = NULL;
    if (nexposures > 1) {
      cpl_error_code rc = muse_xcombine_weights(pixtables, weight);
      if (rc != CPL_ERROR_NONE) {
        cpl_msg_error(__func__, "weighting the pixel tables didn't work: %s",
                      cpl_error_get_message());
        break;
      }
      /* combine individual pixel tables and delete them */
      bigpixtable = muse_xcombine_tables(pixtables, offsets);
      if (!bigpixtable) {
        cpl_msg_error(__func__, "combining the pixel tables didn't work: %s",
                      cpl_error_get_message());
        break;
      }
    } else {
      bigpixtable = pixtables[0];
    }
    cpl_free(pixtables);

    muse_processing_save_table(aProcessing, -1, bigpixtable, NULL,
                               MUSE_TAG_PIXTABLE_COMBINED,
                               MUSE_TABLE_TYPE_PIXTABLE);
    muse_pixtable_delete(bigpixtable);
    cpl_table_delete(offsets);
    return 0;
  } while (0); /* end of "error-free" loop, error handling follows */
  for (i = 0; i < nexposures; i++) {
    muse_pixtable_delete(pixtables[i]);
  } /* for i (exposures) */
  cpl_free(pixtables);
  cpl_table_delete(offsets);
  return -1;
} /* muse_scipost_combine_pixtables_compute() */

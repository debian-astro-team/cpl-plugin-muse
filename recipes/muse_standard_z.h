/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/* 
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* This file was automatically generated */

#ifndef MUSE_STANDARD_Z_H
#define MUSE_STANDARD_Z_H

/*----------------------------------------------------------------------------*
 *                              Includes                                      *
 *----------------------------------------------------------------------------*/
#include <muse.h>
#include <muse_instrument.h>

/*----------------------------------------------------------------------------*
 *                          Special variable types                            *
 *----------------------------------------------------------------------------*/

/** @addtogroup recipe_muse_standard */
/**@{*/

/*----------------------------------------------------------------------------*/
/**
  @brief   Structure to hold the parameters of the muse_standard recipe.

  This structure contains the parameters for the recipe that may be set on the
  command line, in the configuration, or through the environment.
 */
/*----------------------------------------------------------------------------*/
typedef struct muse_standard_params_s {
  /** @brief   Type of flux integration to use. "gaussian", "moffat", and "smoffat" use 2D profile fitting, "circle" and "square" are non-optimal aperture flux integrators. "smoffat" uses smoothing of the Moffat parameters from an initial fit, to derive physically meaningful wavelength-dependent behavior.  "auto" selects the smoothed Moffat profile for WFM data and circular flux integration for NFM. */
  int profile;
  /** @brief   Type of flux integration to use. "gaussian", "moffat", and "smoffat" use 2D profile fitting, "circle" and "square" are non-optimal aperture flux integrators. "smoffat" uses smoothing of the Moffat parameters from an initial fit, to derive physically meaningful wavelength-dependent behavior.  "auto" selects the smoothed Moffat profile for WFM data and circular flux integration for NFM. (as string) */
  const char *profile_s;

  /** @brief   How to select the star for flux integration, "flux" uses the brightest star in the field, "distance" uses the detection nearest to the approximate coordinates of the reference source. */
  int select;
  /** @brief   How to select the star for flux integration, "flux" uses the brightest star in the field, "distance" uses the detection nearest to the approximate coordinates of the reference source. (as string) */
  const char *select_s;

  /** @brief   How to smooth the response curve before writing it to disk.  "none" does not do any kind of smoothing (such a response curve is only useful, if smoothed externally; "median" does a median-filter of 15 Angstrom half-width; "ppoly" fits piecewise cubic polynomials (each one across 2x150 Angstrom width) postprocessed by a sliding average filter of 15 Angstrom half-width. */
  int smooth;
  /** @brief   How to smooth the response curve before writing it to disk.  "none" does not do any kind of smoothing (such a response curve is only useful, if smoothed externally; "median" does a median-filter of 15 Angstrom half-width; "ppoly" fits piecewise cubic polynomials (each one across 2x150 Angstrom width) postprocessed by a sliding average filter of 15 Angstrom half-width. (as string) */
  const char *smooth_s;

  /** @brief   Cut off the data below this wavelength after loading the pixel table(s). */
  double lambdamin;

  /** @brief   Cut off the data above this wavelength after loading the pixel table(s). */
  double lambdamax;

  /** @brief   Reference wavelength used for correction of differential atmospheric refraction. The R-band (peak wavelength ~7000 Angstrom) that is usually used for guiding, is close to the central wavelength of MUSE, so a value of 7000.0 Angstrom should be used if nothing else is known. A value less than zero switches DAR correction off. */
  double lambdaref;

  /** @brief   Carry out a check of the theoretical DAR correction using source centroiding. If "correct" it will also apply an empirical correction. */
  int darcheck;
  /** @brief   Carry out a check of the theoretical DAR correction using source centroiding. If "correct" it will also apply an empirical correction. (as string) */
  const char *darcheck_s;

  /** @brief   The filter name(s) to be used for the output field-of-view image. Each name has to correspond to an EXTNAME in an extension of the FILTER_LIST file. If an unsupported filter name is given, creation of the respective image is omitted. If multiple filter names are given, they have to be comma separated. If the zeropoint QC parameters are wanted, make sure to add "Johnson_V,Cousins_R,Cousins_I". */
  const char * filter;

  char __dummy__; /* quieten compiler warning about possibly empty struct */
} muse_standard_params_t;

#define MUSE_STANDARD_PARAM_PROFILE_GAUSSIAN 1
#define MUSE_STANDARD_PARAM_PROFILE_MOFFAT 2
#define MUSE_STANDARD_PARAM_PROFILE_SMOFFAT 3
#define MUSE_STANDARD_PARAM_PROFILE_CIRCLE 4
#define MUSE_STANDARD_PARAM_PROFILE_SQUARE 5
#define MUSE_STANDARD_PARAM_PROFILE_AUTO 6
#define MUSE_STANDARD_PARAM_PROFILE_INVALID_VALUE -1
#define MUSE_STANDARD_PARAM_SELECT_FLUX 1
#define MUSE_STANDARD_PARAM_SELECT_DISTANCE 2
#define MUSE_STANDARD_PARAM_SELECT_INVALID_VALUE -1
#define MUSE_STANDARD_PARAM_SMOOTH_NONE 1
#define MUSE_STANDARD_PARAM_SMOOTH_MEDIAN 2
#define MUSE_STANDARD_PARAM_SMOOTH_PPOLY 3
#define MUSE_STANDARD_PARAM_SMOOTH_INVALID_VALUE -1
#define MUSE_STANDARD_PARAM_DARCHECK_NONE 1
#define MUSE_STANDARD_PARAM_DARCHECK_CHECK 2
#define MUSE_STANDARD_PARAM_DARCHECK_CORRECT 3
#define MUSE_STANDARD_PARAM_DARCHECK_INVALID_VALUE -1

/**@}*/

/*----------------------------------------------------------------------------*
 *                           Function prototypes                              *
 *----------------------------------------------------------------------------*/
int muse_standard_compute(muse_processing *, muse_standard_params_t *);

#endif /* MUSE_STANDARD_Z_H */

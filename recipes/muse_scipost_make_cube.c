/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/*
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2014 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*---------------------------------------------------------------------------*
 *                             Includes                                      *
 *---------------------------------------------------------------------------*/
#include <muse.h>
#include "muse_scipost_make_cube_z.h"

/*---------------------------------------------------------------------------*
 *                             Functions code                                *
 *---------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
  @brief    Separate cube creation, main function
  @param    aProcessing  the processing structure
  @param    aParams      the parameters list
  @return   0 if everything is ok, -1 something went wrong
 */
/*----------------------------------------------------------------------------*/
int
muse_scipost_make_cube_compute(muse_processing *aProcessing,
                               muse_scipost_make_cube_params_t *aParams)
{
  cpl_frameset *inframes = muse_frameset_find_tags(aProcessing->inframes,
                                                      aProcessing->intags, 0,
                                                      CPL_FALSE);
  cpl_frame *frame = cpl_frameset_get_position(inframes, 0);
  char *fn = cpl_strdup(cpl_frame_get_filename(frame));
  muse_pixtable *pixtable = muse_pixtable_load_restricted_wavelength(fn,
                                                                     aParams->lambdamin,
                                                                     aParams->lambdamax);
  muse_processing_append_used(aProcessing, frame, CPL_FRAME_GROUP_RAW, 1);
  cpl_frameset_delete(inframes);
  if (!pixtable) {
    cpl_msg_error(__func__, "NULL pixel table for %s", fn);
    cpl_free(fn);
    return -1;
  }
  cpl_free(fn);
  /* erase pre-existing QC parameters */
  cpl_propertylist_erase_regexp(pixtable->header, "ESO QC ", 0);
  muse_pixtable_wcs wcstype = muse_pixtable_wcs_check(pixtable);
  if (wcstype == MUSE_PIXTABLE_WCS_NATSPH) {
    muse_wcs_position_celestial(pixtable,
                                muse_pfits_get_ra(pixtable->header),
                                muse_pfits_get_dec(pixtable->header));
  }
  muse_resampling_type resample
    = muse_postproc_get_resampling_type(aParams->resample_s);
  muse_resampling_params *rp = muse_resampling_params_new(resample);
  rp->dx = aParams->dx;
  rp->dy = aParams->dy;
  rp->dlambda = aParams->dlambda;
  rp->crtype = muse_postproc_get_cr_type(aParams->crtype_s);
  rp->crsigma = aParams->crsigma;
  rp->ld = aParams->ld;
  rp->rc = aParams->rc;
  muse_resampling_params_set_pixfrac(rp, aParams->pixfrac);
  cpl_propertylist *outwcs = muse_postproc_cube_load_output_wcs(aProcessing);
  muse_resampling_params_set_wcs(rp, outwcs);
  cpl_propertylist_delete(outwcs);
  muse_cube_type format = muse_postproc_get_cube_format(aParams->format_s);
  cpl_error_code rc = muse_postproc_cube_resample_and_collapse(aProcessing,
                                                               pixtable,
                                                               format, rp,
                                                               aParams->filter);
  muse_resampling_params_delete(rp);
  if (aParams->stacked) {
    cpl_msg_debug(__func__, "additional output as column-stacked image");
    muse_image *img = muse_resampling_image(pixtable,
                                            MUSE_RESAMPLE_WEIGHTED_RENKA,
                                            aParams->dx, aParams->dlambda);
    muse_processing_save_image(aProcessing, -1, img, MUSE_TAG_OBJECT_RESAMPLED);
    muse_image_delete(img);
  }
  muse_pixtable_delete(pixtable);
  return rc == CPL_ERROR_NONE ? 0 : -1;
} /* muse_scipost_make_cube_compute() */

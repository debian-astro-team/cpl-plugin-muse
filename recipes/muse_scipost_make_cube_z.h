/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/* 
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* This file was automatically generated */

#ifndef MUSE_SCIPOST_MAKE_CUBE_Z_H
#define MUSE_SCIPOST_MAKE_CUBE_Z_H

/*----------------------------------------------------------------------------*
 *                              Includes                                      *
 *----------------------------------------------------------------------------*/
#include <muse.h>
#include <muse_instrument.h>

/*----------------------------------------------------------------------------*
 *                          Special variable types                            *
 *----------------------------------------------------------------------------*/

/** @addtogroup recipe_muse_scipost_make_cube */
/**@{*/

/*----------------------------------------------------------------------------*/
/**
  @brief   Structure to hold the parameters of the muse_scipost_make_cube recipe.

  This structure contains the parameters for the recipe that may be set on the
  command line, in the configuration, or through the environment.
 */
/*----------------------------------------------------------------------------*/
typedef struct muse_scipost_make_cube_params_s {
  /** @brief   Cut off the data below this wavelength after loading the pixel table(s). */
  double lambdamin;

  /** @brief   Cut off the data above this wavelength after loading the pixel table(s). */
  double lambdamax;

  /** @brief   The resampling technique to use for the final output cube. */
  int resample;
  /** @brief   The resampling technique to use for the final output cube. (as string) */
  const char *resample_s;

  /** @brief   Horizontal step size for resampling (in arcsec or pixel). The following defaults are taken when this value is set to 0.0: 0.2'' for WFM, 0.025'' for NFM, 1.0 if data is in pixel units. */
  double dx;

  /** @brief   Vertical step size for resampling (in arcsec or pixel). The following defaults are taken when this value is set to 0.0: 0.2'' for WFM, 0.025'' for NFM, 1.0 if data is in pixel units. */
  double dy;

  /** @brief   Wavelength step size (in Angstrom). Natural instrument sampling is used, if this is 0.0 */
  double dlambda;

  /** @brief   Type of statistics used for detection of cosmic rays during final resampling. "iraf" uses the variance information, "mean" uses standard (mean/stdev) statistics, "median" uses median and the median median of the absolute median deviation. */
  int crtype;
  /** @brief   Type of statistics used for detection of cosmic rays during final resampling. "iraf" uses the variance information, "mean" uses standard (mean/stdev) statistics, "median" uses median and the median median of the absolute median deviation. (as string) */
  const char *crtype_s;

  /** @brief   Sigma rejection factor to use for cosmic ray rejection during final resampling. A zero or negative value switches cosmic ray rejection off. */
  double crsigma;

  /** @brief   Critical radius for the "renka" resampling method. */
  double rc;

  /** @brief   Pixel down-scaling factor for the "drizzle" resampling method. Up to three, comma-separated, floating-point values can be given. If only one value is given, it applies to all dimensions, two values are interpreted as spatial and spectral direction, respectively, while three are taken as horizontal, vertical, and spectral. */
  const char * pixfrac;

  /** @brief   Number of adjacent pixels to take into account during resampling in all three directions (loop distance); this affects all resampling methods except "nearest". */
  int ld;

  /** @brief   Type of output file format, "Cube" is a standard FITS cube with NAXIS=3 and multiple extensions (for data and variance). The extended "x" formats include the reconstructed image(s) in FITS image extensions within the same file. */
  int format;
  /** @brief   Type of output file format, "Cube" is a standard FITS cube with NAXIS=3 and multiple extensions (for data and variance). The extended "x" formats include the reconstructed image(s) in FITS image extensions within the same file. (as string) */
  const char *format_s;

  /** @brief   If true, write an additional output file in form of a 2D stacked image (x direction is pseudo-spatial, y direction is wavelength). */
  int stacked;

  /** @brief   The filter name(s) to be used for the output field-of-view image. Each name has to correspond to an EXTNAME in an extension of the FILTER_LIST file. If an unsupported filter name is given, creation of the respective image is omitted. If multiple filter names are given, they have to be comma separated. */
  const char * filter;

  char __dummy__; /* quieten compiler warning about possibly empty struct */
} muse_scipost_make_cube_params_t;

#define MUSE_SCIPOST_MAKE_CUBE_PARAM_RESAMPLE_NEAREST 1
#define MUSE_SCIPOST_MAKE_CUBE_PARAM_RESAMPLE_LINEAR 2
#define MUSE_SCIPOST_MAKE_CUBE_PARAM_RESAMPLE_QUADRATIC 3
#define MUSE_SCIPOST_MAKE_CUBE_PARAM_RESAMPLE_RENKA 4
#define MUSE_SCIPOST_MAKE_CUBE_PARAM_RESAMPLE_DRIZZLE 5
#define MUSE_SCIPOST_MAKE_CUBE_PARAM_RESAMPLE_LANCZOS 6
#define MUSE_SCIPOST_MAKE_CUBE_PARAM_RESAMPLE_INVALID_VALUE -1
#define MUSE_SCIPOST_MAKE_CUBE_PARAM_CRTYPE_IRAF 1
#define MUSE_SCIPOST_MAKE_CUBE_PARAM_CRTYPE_MEAN 2
#define MUSE_SCIPOST_MAKE_CUBE_PARAM_CRTYPE_MEDIAN 3
#define MUSE_SCIPOST_MAKE_CUBE_PARAM_CRTYPE_INVALID_VALUE -1
#define MUSE_SCIPOST_MAKE_CUBE_PARAM_FORMAT_CUBE 1
#define MUSE_SCIPOST_MAKE_CUBE_PARAM_FORMAT_EURO3D 2
#define MUSE_SCIPOST_MAKE_CUBE_PARAM_FORMAT_XCUBE 3
#define MUSE_SCIPOST_MAKE_CUBE_PARAM_FORMAT_XEURO3D 4
#define MUSE_SCIPOST_MAKE_CUBE_PARAM_FORMAT_INVALID_VALUE -1

/**@}*/

/*----------------------------------------------------------------------------*
 *                           Function prototypes                              *
 *----------------------------------------------------------------------------*/
int muse_scipost_make_cube_compute(muse_processing *, muse_scipost_make_cube_params_t *);

#endif /* MUSE_SCIPOST_MAKE_CUBE_Z_H */

/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/* 
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* This file was automatically generated */

#ifndef MUSE_SCIPOST_CORRECT_RV_Z_H
#define MUSE_SCIPOST_CORRECT_RV_Z_H

/*----------------------------------------------------------------------------*
 *                              Includes                                      *
 *----------------------------------------------------------------------------*/
#include <muse.h>
#include <muse_instrument.h>

/*----------------------------------------------------------------------------*
 *                          Special variable types                            *
 *----------------------------------------------------------------------------*/

/** @addtogroup recipe_muse_scipost_correct_rv */
/**@{*/

/*----------------------------------------------------------------------------*/
/**
  @brief   Structure to hold the parameters of the muse_scipost_correct_rv recipe.

  This structure contains the parameters for the recipe that may be set on the
  command line, in the configuration, or through the environment.
 */
/*----------------------------------------------------------------------------*/
typedef struct muse_scipost_correct_rv_params_s {
  /** @brief   Cut off the data below this wavelength after loading the pixel table(s). */
  double lambdamin;

  /** @brief   Cut off the data above this wavelength after loading the pixel table(s). */
  double lambdamax;

  /** @brief   Correct the radial velocity of the telescope with reference to either the barycenter of the Solar System (bary), the center of the Sun (helio), or to the center of the Earth (geo). */
  int rvcorr;
  /** @brief   Correct the radial velocity of the telescope with reference to either the barycenter of the Solar System (bary), the center of the Sun (helio), or to the center of the Earth (geo). (as string) */
  const char *rvcorr_s;

  char __dummy__; /* quieten compiler warning about possibly empty struct */
} muse_scipost_correct_rv_params_t;

#define MUSE_SCIPOST_CORRECT_RV_PARAM_RVCORR_BARY 1
#define MUSE_SCIPOST_CORRECT_RV_PARAM_RVCORR_HELIO 2
#define MUSE_SCIPOST_CORRECT_RV_PARAM_RVCORR_GEO 3
#define MUSE_SCIPOST_CORRECT_RV_PARAM_RVCORR_NONE 4
#define MUSE_SCIPOST_CORRECT_RV_PARAM_RVCORR_INVALID_VALUE -1

/**@}*/

/*----------------------------------------------------------------------------*
 *                           Function prototypes                              *
 *----------------------------------------------------------------------------*/
int muse_scipost_correct_rv_compute(muse_processing *, muse_scipost_correct_rv_params_t *);

#endif /* MUSE_SCIPOST_CORRECT_RV_Z_H */

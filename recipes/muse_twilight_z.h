/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/* 
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2005-2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* This file was automatically generated */

#ifndef MUSE_TWILIGHT_Z_H
#define MUSE_TWILIGHT_Z_H

/*----------------------------------------------------------------------------*
 *                              Includes                                      *
 *----------------------------------------------------------------------------*/
#include <muse.h>
#include <muse_instrument.h>

/*----------------------------------------------------------------------------*
 *                          Special variable types                            *
 *----------------------------------------------------------------------------*/

/** @addtogroup recipe_muse_twilight */
/**@{*/

/*----------------------------------------------------------------------------*/
/**
  @brief   Structure to hold the parameters of the muse_twilight recipe.

  This structure contains the parameters for the recipe that may be set on the
  command line, in the configuration, or through the environment.
 */
/*----------------------------------------------------------------------------*/
typedef struct muse_twilight_params_s {
  /** @brief   If this is "none", stop when detecting discrepant overscan levels (see ovscsigma), for "offset" it assumes that the mean overscan level represents the real offset in the bias levels of the exposures involved, and adjusts the data accordingly; for "vpoly", a polynomial is fit to the vertical overscan and subtracted from the whole quadrant. */
  const char * overscan;

  /** @brief   This influences how values are rejected when computing overscan statistics. Either no rejection at all ("none"), rejection using the DCR algorithm ("dcr"), or rejection using an iterative constant fit ("fit"). */
  const char * ovscreject;

  /** @brief   If the deviation of mean overscan levels between a raw input image and the reference image is higher than |ovscsigma x stdev|, stop the processing. If overscan="vpoly", this is used as sigma rejection level for the iterative polynomial fit (the level comparison is then done afterwards with |100 x stdev| to guard against incompatible settings). Has no effect for overscan="offset". */
  double ovscsigma;

  /** @brief   The number of pixels of the overscan adjacent to the data section of the CCD that are ignored when computing statistics or fits. */
  int ovscignore;

  /** @brief   Type of combination to use */
  int combine;
  /** @brief   Type of combination to use (as string) */
  const char *combine_s;

  /** @brief   Number of minimum pixels to reject with minmax */
  int nlow;

  /** @brief   Number of maximum pixels to reject with minmax */
  int nhigh;

  /** @brief   Number of pixels to keep with minmax */
  int nkeep;

  /** @brief   Low sigma for pixel rejection with sigclip */
  double lsigma;

  /** @brief   High sigma for pixel rejection with sigclip */
  double hsigma;

  /** @brief   Scale the individual images to a common exposure time before combining them. */
  int scale;

  /** @brief   The resampling technique to use for the final output cube. */
  int resample;
  /** @brief   The resampling technique to use for the final output cube. (as string) */
  const char *resample_s;

  /** @brief   Type of statistics used for detection of cosmic rays during final resampling. "iraf" uses the variance information, "mean" uses standard (mean/stdev) statistics, "median" uses median and the median median of the absolute median deviation. */
  int crtype;
  /** @brief   Type of statistics used for detection of cosmic rays during final resampling. "iraf" uses the variance information, "mean" uses standard (mean/stdev) statistics, "median" uses median and the median median of the absolute median deviation. (as string) */
  const char *crtype_s;

  /** @brief   Sigma rejection factor to use for cosmic ray rejection during final resampling. A zero or negative value switches cosmic ray rejection off. */
  double crsigma;

  /** @brief   Minimum wavelength for twilight reconstruction. */
  double lambdamin;

  /** @brief   Maximum wavelength for twilight reconstruction. */
  double lambdamax;

  /** @brief   Sampling for twilight reconstruction, this should result in planes of equal wavelength coverage. */
  double dlambda;

  /** @brief   Polynomial order to use in x direction to fit the full field of view. */
  int xorder;

  /** @brief   Polynomial order to use in y direction to fit the full field of view. */
  int yorder;

  /** @brief   Pixels on edges stronger than this fraction in the normalized image are excluded from the fit to the vignetted area. Set to non-positive number to include them in the fit. This has no effect for NFM skyflats. */
  double vignmaskedges;

  /** @brief   Type of smoothing to use for the vignetted region given by the VIGNETTING_MASK (for WFM, or the internal mask, for NFM); gaussian uses (vignxpar + vignypar)/2 as FWHM. */
  int vignsmooth;
  /** @brief   Type of smoothing to use for the vignetted region given by the VIGNETTING_MASK (for WFM, or the internal mask, for NFM); gaussian uses (vignxpar + vignypar)/2 as FWHM. (as string) */
  const char *vignsmooth_s;

  /** @brief   Parameter used by the vignetting smoothing: x order for polyfit (default, recommended 4), parameter that influences the FWHM for the gaussian (recommended: 10), or x dimension of median filter (recommended 5). If a negative value is found, the default is taken. */
  int vignxpar;

  /** @brief   Parameter used by the vignetting smoothing: y order for polyfit (default, recommended 4), parameter that influences the FWHM for the gaussian (recommended: 10), or y dimension of median filter (recommended 5). If a negative value is found, the default is taken. */
  int vignypar;

  /** @brief   The height of the vignetted region at the top of the MUSE field in NFM. This is the region modeled separately (the final vignetting model might be smaller). */
  int vignnfmmask;

  char __dummy__; /* quieten compiler warning about possibly empty struct */
} muse_twilight_params_t;

#define MUSE_TWILIGHT_PARAM_COMBINE_AVERAGE 1
#define MUSE_TWILIGHT_PARAM_COMBINE_MEDIAN 2
#define MUSE_TWILIGHT_PARAM_COMBINE_MINMAX 3
#define MUSE_TWILIGHT_PARAM_COMBINE_SIGCLIP 4
#define MUSE_TWILIGHT_PARAM_COMBINE_INVALID_VALUE -1
#define MUSE_TWILIGHT_PARAM_RESAMPLE_NEAREST 1
#define MUSE_TWILIGHT_PARAM_RESAMPLE_LINEAR 2
#define MUSE_TWILIGHT_PARAM_RESAMPLE_QUADRATIC 3
#define MUSE_TWILIGHT_PARAM_RESAMPLE_RENKA 4
#define MUSE_TWILIGHT_PARAM_RESAMPLE_DRIZZLE 5
#define MUSE_TWILIGHT_PARAM_RESAMPLE_LANCZOS 6
#define MUSE_TWILIGHT_PARAM_RESAMPLE_INVALID_VALUE -1
#define MUSE_TWILIGHT_PARAM_CRTYPE_IRAF 1
#define MUSE_TWILIGHT_PARAM_CRTYPE_MEAN 2
#define MUSE_TWILIGHT_PARAM_CRTYPE_MEDIAN 3
#define MUSE_TWILIGHT_PARAM_CRTYPE_INVALID_VALUE -1
#define MUSE_TWILIGHT_PARAM_VIGNSMOOTH_POLYFIT 1
#define MUSE_TWILIGHT_PARAM_VIGNSMOOTH_GAUSSIAN 2
#define MUSE_TWILIGHT_PARAM_VIGNSMOOTH_MEDIAN 3
#define MUSE_TWILIGHT_PARAM_VIGNSMOOTH_INVALID_VALUE -1

/**@}*/

/*----------------------------------------------------------------------------*
 *                           Function prototypes                              *
 *----------------------------------------------------------------------------*/
int muse_twilight_compute(muse_processing *, muse_twilight_params_t *);

#endif /* MUSE_TWILIGHT_Z_H */

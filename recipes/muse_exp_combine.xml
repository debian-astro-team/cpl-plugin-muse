<?xml-stylesheet type="text/xsl" href="recipedoc2html.xsl" ?>
<pluginlist instrument="muse">
  <plugin name="muse_exp_combine" type="recipe">
    <plugininfo>
      <synopsis>Combine several exposures into one datacube.</synopsis>
      <description>
        Sort reduced pixel tables, one per exposure, by exposure and combine
        them with applied weights into one final datacube.
      </description>
      <author>Peter Weilbacher</author>
      <email>pweilbacher@aip.de</email>
    </plugininfo>
    <parameters>
      <parameter name="save" type="string">
        <description>Select output product(s) to save. Can contain one or more of "cube" (output cube and associated images; if this is not given, no resampling is done at all) or "combined" (fully reduced and combined pixel table for the full set of exposures; this is useful, if the final resampling step is to be done again separately). If several options are given, they have to be comma-separated.</description>
        <default>cube</default>
      </parameter>
      <parameter name="resample" type="string">
        <description>The resampling technique to use for the final output cube.</description>
        <enumeration>
          <value>nearest</value>
          <value>linear</value>
          <value>quadratic</value>
          <value>renka</value>
          <value>drizzle</value>
          <value>lanczos</value>
        </enumeration>
        <default>drizzle</default>
      </parameter>
      <parameter name="dx" type="double">
        <description>Horizontal step size for resampling (in arcsec or pixel). The following defaults are taken when this value is set to 0.0: 0.2'' for WFM, 0.025'' for NFM, 1.0 if data is in pixel units.</description>
        <default>0.0</default>
      </parameter>
      <parameter name="dy" type="double">
        <description>Vertical step size for resampling (in arcsec or pixel). The following defaults are taken when this value is set to 0.0: 0.2'' for WFM, 0.025'' for NFM, 1.0 if data is in pixel units.</description>
        <default>0.0</default>
      </parameter>
      <parameter name="dlambda" type="double">
        <description>Wavelength step size (in Angstrom). Natural instrument sampling is used, if this is 0.0</description>
        <default>0.0</default>
      </parameter>
      <parameter name="crtype" type="string">
        <description>Type of statistics used for detection of cosmic rays during final resampling. "iraf" uses the variance information, "mean" uses standard (mean/stdev) statistics, "median" uses median and the median median of the absolute median deviation.</description>
        <enumeration>
          <value>iraf</value>
          <value>mean</value>
          <value>median</value>
        </enumeration>
        <default>median</default>
      </parameter>
      <parameter name="crsigma" type="double">
        <description>Sigma rejection factor to use for cosmic ray rejection during final resampling. A zero or negative value switches cosmic ray rejection off.</description>
        <default>10.</default>
      </parameter>
      <parameter name="rc" type="double">
        <description>Critical radius for the "renka" resampling method.</description>
        <default>1.25</default>
      </parameter>
      <parameter name="pixfrac" type="string">
        <description>Pixel down-scaling factor for the "drizzle" resampling method. Up to three, comma-separated, floating-point values can be given. If only one value is given, it applies to all dimensions, two values are interpreted as spatial and spectral direction, respectively, while three are taken as horizontal, vertical, and spectral.</description>
        <default>0.6,0.6</default>
      </parameter>
      <parameter name="ld" type="int">
        <description>Number of adjacent pixels to take into account during resampling in all three directions (loop distance); this affects all resampling methods except "nearest".</description>
        <default>1</default>
      </parameter>
      <parameter name="format" type="string">
        <description>Type of output file format, "Cube" is a standard FITS cube with NAXIS=3 and multiple extensions (for data and variance). The extended "x" formats include the reconstructed image(s) in FITS image extensions within the same file. "sdpCube" does some extra calculations to create FITS keywords for the ESO Science Data Products.</description>
        <enumeration>
          <value>Cube</value>
          <value>Euro3D</value>
          <value>xCube</value>
          <value>xEuro3D</value>
          <value>sdpCube</value>
        </enumeration>
        <default>Cube</default>
      </parameter>
      <parameter name="weight" type="string">
        <description>Type of weighting scheme to use when combining multiple exposures. "exptime" just uses the exposure time to weight the exposures, "fwhm" uses the best available seeing information from the headers as well, "header" queries ESO.DRS.MUSE.WEIGHT of each input file instead of the FWHM, and "none" preserves an existing weight column in the input pixel tables without changes.</description>
        <enumeration>
          <value>exptime</value>
          <value>fwhm</value>
          <value>header</value>
          <value>none</value>
        </enumeration>
        <default>exptime</default>
      </parameter>
      <parameter name="filter" type="string">
        <description>The filter name(s) to be used for the output field-of-view image. Each name has to correspond to an EXTNAME in an extension of the FILTER_LIST file. If an unsupported filter name is given, creation of the respective image is omitted. If multiple filter names are given, they have to be comma separated.</description>
        <default>white</default>
      </parameter>
      <parameter name="lambdamin" type="double">
        <description>Cut off the data below this wavelength after loading the pixel table(s).</description>
        <default>4000.</default>
      </parameter>
      <parameter name="lambdamax" type="double">
        <description>Cut off the data above this wavelength after loading the pixel table(s).</description>
        <default>10000.</default>
      </parameter>
    </parameters>
    <frames>
      <frameset>
        <frame tag="PIXTABLE_REDUCED" group="raw" min="2">
          <description>Input pixel tables</description>
        </frame>
        <frame tag="OFFSET_LIST" group="calib" max="1" format="OFFSET_LIST">
          <description>List of coordinate offsets (and optional flux scale factors)</description>
          <select origin="generated"/>
        </frame>
        <frame tag="FILTER_LIST" group="calib" max="1">
          <description>File to be used to create field-of-view images.</description>
          <select origin="extern"/>
        </frame>
        <frame tag="OUTPUT_WCS" group="calib" max="1" format="OUTPUT_WCS">
          <description>WCS to override output cube location / dimensions (see data format chapter for details)</description>
          <select origin="manual"/>
        </frame>
        <frame tag="DATACUBE_FINAL" group="product" level="final" mode="master" format="DATACUBE|EURO3DCUBE">
          <description>Output datacube (if --save contains "cube")</description>
          <fitsheader>
            <parameter id="k" name="source" regexp="[0-9]+" min="1"/>
            <property type="int">
              <name>ESO QC EXPCOMB NDET</name>
              <description>Number of detected sources in combined cube.</description>
            </property>
            <property type="float">
              <name>ESO QC EXPCOMB LAMBDA</name>
              <unit>Angstrom</unit>
              <description>Wavelength of plane in combined cube that was used for object detection.</description>
            </property>
            <property type="float">
              <name>ESO QC EXPCOMB POSk X</name>
              <unit>pix</unit>
              <description>Position of source k in x-direction in combined cube. If the FWHM measurement fails, this value will be -1.</description>
            </property>
            <property type="float">
              <name>ESO QC EXPCOMB POSk Y</name>
              <unit>pix</unit>
              <description>Position of source k in y-direction in combined cube. If the FWHM measurement fails, this value will be -1.</description>
            </property>
            <property type="float">
              <name>ESO QC EXPCOMB FWHMk X</name>
              <unit>arcsec</unit>
              <description>FWHM of source k in x-direction in combined cube. If the FWHM measurement fails, this value will be -1.</description>
            </property>
            <property type="float">
              <name>ESO QC EXPCOMB FWHMk Y</name>
              <unit>arcsec</unit>
              <description>FWHM of source k in y-direction in combined cube. If the FWHM measurement fails, this value will be -1.</description>
            </property>
            <property type="int">
              <name>ESO QC EXPCOMB FWHM NVALID</name>
              <description>Number of detected sources with valid FWHM in combined cube.</description>
            </property>
            <property type="float">
              <name>ESO QC EXPCOMB FWHM MEDIAN</name>
              <unit>arcsec</unit>
              <description>Median FWHM of all sources with valid FWHM measurement (in x- and y-direction) in combined cube. If less than three sources with valid FWHM are detected, this value is zero.</description>
            </property>
            <property type="float">
              <name>ESO QC EXPCOMB FWHM MAD</name>
              <unit>arcsec</unit>
              <description>Median absolute deviation of the FWHM of all sources with valid FWHM measurement (in x- and y-direction) in combined cube. If less than three sources with valid FWHM are detected, this value is zero.</description>
            </property>
          </fitsheader>
        </frame>
        <frame tag="IMAGE_FOV" group="product" level="final" mode="sequence" format="MUSE_IMAGE">
          <description>Field-of-view images corresponding to the "filter" parameter (if --save contains "cube").</description>
        </frame>
        <frame tag="PIXTABLE_COMBINED" group="product" level="intermediate" mode="master" format="PIXEL_TABLE">
          <description>Combined pixel table (if --save contains "combined")</description>
        </frame>
      </frameset>
    </frames>
  </plugin>
</pluginlist>

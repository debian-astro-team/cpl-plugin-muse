/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/*
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2007-2014 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <muse.h>
#include <string.h>

/*----------------------------------------------------------------------------*/
/**
 * @defgroup tools_musepixtabledump         Tool muse_pixtable_dump
 *
 * <b>muse_pixtable_dump</b>: Display a MUSE pixel table on the screen.
 *
 * This displays (dumps) the contents of a MUSE pixel table on the screen.
 * Instead of just displaying the contents of the table as other FITS table
 * viewers could do, this tool knows to interpret the "origin" column of the
 * pixel table, and displays the original coordinates of each pixel in the
 * table.
 *
 * <b>Command line arguments:</b>
 *   - <tt>-s startidx</tt> (optional, default: 0)\n
 *     index (row number, starting at zero) in the table where to
 *     start the display
 *   - <tt>-c count</tt> (optional, default: all)\n
 *     number of rows to display
 *   - <tt><b>PIXTABLE</b></tt>\n
 *     the filename of the pixel table to display
 *
 * <b>Return values:</b>
 *   - <tt> 0</tt>\n   Success
 *   - <tt> 1</tt>\n   no filename given
 *   - <tt> 2</tt>\n   argument <tt>-s</tt> without any number
 *   - <tt> 3</tt>\n   argument <tt>-c</tt> without any number
 *   - <tt> 9</tt>\n   unknown option given
 *   - <tt>10</tt>\n   pixel table could not be loaded from file
 *   - <tt>11</tt>\n   File does not seem to contain a MUSE pixel table
 *   - <tt>12</tt>\n   Illegal data range given
 *   - <tt>50</tt>\n   unknown error
 */
/*----------------------------------------------------------------------------*/

/**@{*/

#define PRINT_USAGE(rc)                                                        \
  fprintf(stderr, "Usage: %s [ -s startidx ] [ -c count ] PIXTABLE\n",         \
          argv[0]);                                                            \
  cpl_end(); return (rc);

int main(int argc, char **argv)
{
  cpl_init(CPL_INIT_DEFAULT);
  muse_processing_recipeinfo(NULL);

  if (argc <= 1) {
    /* filename is needed at least */
    PRINT_USAGE(1);
  }

  char *tname = NULL;
  cpl_size start = 0, count = CPL_SIZE_MAX;
  int i;

  /* argument processing */
  for (i = 1; i < argc; i++) {
    if (strncmp(argv[i], "-s", 3) == 0) {
      /* skip to next arg to get start value */
      i++;
      if (i < argc) {
        start = atol(argv[i]);
      } else {
        PRINT_USAGE(2);
      }
    } else if (strncmp(argv[i], "-c", 3) == 0) {
      /* skip to next arg to get count value */
      i++;
      if (i < argc) {
        count = atol(argv[i]);
      } else {
        PRINT_USAGE(3);
      }
    } else if (strncmp(argv[i], "-", 1) == 0) { /* unallowed options */
      PRINT_USAGE(9);
    } else {
      tname = argv[i];
      break; /* we have the required name, skip the rest */
    }
  }

  cpl_msg_set_level(CPL_MSG_WARNING); /* swallow INFO output */
  muse_pixtable *table = muse_pixtable_load_window(tname, start, count);
  if (!table) {
    PRINT_USAGE(10);
  }

  if (count == CPL_SIZE_MAX) {
    count = muse_pixtable_get_nrow(table);
  }
  /* find out original table length from the table FITS header */
  cpl_propertylist *theader = cpl_propertylist_load(tname, 1);
  cpl_size nrow = cpl_propertylist_get_long_long(theader, "NAXIS2");
  cpl_propertylist_delete(theader);

  printf("# MUSE pixel table \"%s\", showing %"CPL_SIZE_FORMAT" rows starting "
         "at index %"CPL_SIZE_FORMAT" of %"CPL_SIZE_FORMAT"\n", tname, count,
         start, nrow);
  cpl_error_code rc = muse_pixtable_dump(table, 0, count, 1);
  switch (rc) {
  case CPL_ERROR_NONE:
    rc = 0;
    break;
  case CPL_ERROR_BAD_FILE_FORMAT:
    fprintf(stderr, "%s: \"%s\" does not seem to contain a MUSE pixel table!\n",
            argv[0], tname);
    rc = 11;
    break;
  case CPL_ERROR_ILLEGAL_INPUT:
    fprintf(stderr, "%s: Illegal data range given (start index=%"CPL_SIZE_FORMAT
            " count=%"CPL_SIZE_FORMAT") for table with %"CPL_SIZE_FORMAT
            " rows!\n", argv[0], start, count, muse_pixtable_get_nrow(table));
    rc = 12;
    break;
  default:
    rc = 50;
  } /* switch */

  muse_pixtable_delete(table);
  cpl_end();
  return rc;
}

/**@}*/

/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 sts=2 et cin: */
/*
 * This file is part of the MUSE Instrument Pipeline
 * Copyright (C) 2014 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <muse.h>
#include <string.h>

/*----------------------------------------------------------------------------*/
/**
 * @defgroup tools_musepixtableeraseslice   Tool muse_pixtable_erase_slice
 *
 * <b>muse_pixtable_erase_slice</b>: Erase a slice of one IFU from a MUSE pixel
 *                                   table.
 *
 * <b>Command line arguments:</b>
 *   - <tt><b>PIXTABLE_IN</b></tt>\n
 *     the filename of the pixel table to erase a slice in
 *   - <tt><b>IFU</b></tt>\n
 *     IFU number of the slice to erase
 *   - <tt><b>SLICE</b></tt>\n
 *     SLICE number (CCD numbering) of the slice (belonging to IFU) to erase
 *   - <tt><b>PIXTABLE_OUT</b></tt>\n
 *     the filename of the output pixel table to write; can be the same as
 *     PIXTABLE_IN which causes the input table to be overwritten
 *
 * <b>Return values:</b>
 *   - <tt> 0</tt>\n   Success
 *   - <tt> 1</tt>\n   not four arguments given
 *   - <tt> 2</tt>\n   illegal IFU number given
 *   - <tt> 3</tt>\n   illegal slice number given
 *   - <tt>10</tt>\n   pixel table could not be loaded from file
 *   - <tt>50</tt>\n   unknown error
 */
/*----------------------------------------------------------------------------*/

/**@{*/

#define PRINT_USAGE(rc)                                                        \
  fprintf(stderr, "Usage: %s PIXTABLE_IN IFU SLICE PIXTABLE_OUT\n", argv[0]);  \
  cpl_end(); return (rc);

int main(int argc, char **argv)
{
  cpl_init(CPL_INIT_DEFAULT);
  muse_processing_recipeinfo(NULL);
  cpl_errorstate prestate = cpl_errorstate_get();

  if (argc != 5) {
    /* four arguments are needed */
    PRINT_USAGE(1);
  }

  char *tiname = NULL, /* input table */
       *toname = NULL; /* output table */
  unsigned char ifu = 0;
  unsigned short slice = 0;
  int i;

  /* argument processing */
  for (i = 1; i < argc; i++) {
    if (tiname && toname) {
      break; /* we have the required name, skip the rest */
    }
    if (!tiname) {
      tiname = argv[i]; /* set the name for the input table */
    } else if (!ifu) {
      ifu = atoi(argv[i]);
    } else if (!slice) {
      slice = atoi(argv[i]);
    } else {
      toname = argv[i]; /* set the name for the output table */
    }
  } /* for i (all arguments) */
  if (ifu == 0 || ifu > kMuseNumIFUs) {
    printf("Illegal IFU number %hhu given\n", ifu);
    PRINT_USAGE(2);
  }
  if (slice == 0 || slice > kMuseSlicesPerCCD) {
    printf("Illegal slice number %hu given\n", slice);
    PRINT_USAGE(3);
  }

  muse_pixtable *table = muse_pixtable_load(tiname);
  if (!table) {
    PRINT_USAGE(10);
  }
  cpl_size nrow = muse_pixtable_get_nrow(table);
  printf("Loaded pixel table \"%s\" with %"CPL_SIZE_FORMAT" rows\n",
         tiname, nrow);

  cpl_error_code rc = muse_pixtable_erase_ifu_slice(table, ifu, slice);
  cpl_size nrow2 = muse_pixtable_get_nrow(table);
  if (rc == CPL_ERROR_NONE) {
    printf("Erased slice %hu of IFU %hhu (%"CPL_SIZE_FORMAT" pixels removed)\n",
           slice, ifu, nrow - nrow2);
  } else {
    printf("Error while erasing slice %hu of IFU %hhu: %s\n", slice, ifu,
           cpl_error_get_message());
  }

  muse_pixtable_save(table, toname);
  printf("MUSE pixel table \"%s\" (%"CPL_SIZE_FORMAT" rows) saved\n", toname,
         nrow2);
  muse_pixtable_delete(table);
  rc = cpl_errorstate_is_equal(prestate) ? 0 : 50;
  if (rc) {
    cpl_errorstate_dump(prestate, CPL_FALSE, muse_cplerrorstate_dump_some);
  }
  cpl_end();
  return rc;
}

/**@}*/
